import type * as monaco from 'monaco-editor-core'
import { WorkerManager } from './webworker/worker-manager'
import { RestClientWorker } from './webworker/rest-client-worker'
import { HoverAdapter } from './features/hover/hover-adapter'
import { CompletionAdapter } from './features/completion/completion-adapter'
import { createTokenizationSupport } from './features/tokenization/tokenization'
import { FoldingRangeAdapter } from './features/folding/folding-range-adapter'
import { DiagnosticsAdapter } from './features/validation/diagnostic-adapter'
import { CodeLensAdapter } from './features/codelens/code-lens-adapter'
import { FormatAdapter } from './features/format/format-adapter'
import { HttpQuery } from './models/http-query'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { restClientLanguageConfiguration } from './configuration/language-configuration'
import { optionDefaults, RestClientOptions } from './configuration/rest-client-options'
import { LanguageServiceOptions } from './configuration/language-service-options'
import { buildRequest } from './utils/build-request'
import { parse } from './parser/parse'
import { TypeFormattingEditAdapter } from './features/type-formatting-edit/type-formatting-edit-adapter'
import { JSONScanner } from './parser/scanner'
import { SyntaxKind } from './models/enums/syntax-kind'
export { SyntaxKind as TokenType }

export const languageId = 'rest-client-lang'
export const runQueryCommand = 'run_query'

export interface CustomCompletionItem {
  label: string;
  description?: string;
  kind: number;
  type?: string;
}

export type CustomBodyCompletionResolver = (kind: 'key' | 'value', schema: any, ctx: CustomBodyCompletionContext, options: any) => Promise<CustomCompletionItem[]>
export type CustomPathCompletionResolver = (paramName: string, paramSchema: any, options: any) => Promise<CustomCompletionItem[]>
export type CustomQueryCompletionResolver = (kind: 'key' | 'value', paramName: string, paramSchema: any, options: any) => Promise<CustomCompletionItem[]>

export interface CustomBodyCompletionContext {
  method: string
  pathParams: { [key: string]: string },
  queryParams: { [key: string]: string },
  url: string
}

export type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'HEAD' | 'PATCH' | 'OPTIONS' | 'TRACE'

export type SchemaDef = SchemaDefPath | SchemaDefRef | SchemaDefSchema

export interface SchemaDefPath {
  type: 'path';
  method: HttpMethod;
  path: string;
}

export interface SchemaDefRef {
  type: 'ref';
  $ref: string;
}

export interface SchemaDefSchema {
  type: 'schema';
  schema: any;
}

export type QueryRunner = (httpQueries: HttpQuery[]) => void

export function setupRestClient(_monaco: typeof monaco, options: RestClientOptions = {}) {
  const languageServiceOptions = new LanguageServiceOptions(
    _monaco,
    languageId,
    { ...optionDefaults, ...options, features: { ...optionDefaults.features, ...options.features } },
  )

  const client = new WorkerManager(_monaco, languageServiceOptions)
  const worker = (...uris: monaco.Uri[]): Promise<RestClientWorker> => {
    return client.getLanguageServiceWorker(...uris)
  }
  const restClient = new RestClientLanguage(_monaco, languageServiceOptions)

  _monaco.languages.register({ id: languageId })
  _monaco.languages.onLanguage(languageId, () => {
    const disposables: monaco.IDisposable[] = []
    const providers: monaco.IDisposable[] = []

    disposables.push(client)

    const { languageId, options } = languageServiceOptions
    disposeAll(providers)

    providers.push(_monaco.languages.registerOnTypeFormattingEditProvider(languageId, new TypeFormattingEditAdapter(_monaco, worker)))

    if (options.features.completionItems) {
      providers.push(_monaco.languages.registerCompletionItemProvider(languageId, new CompletionAdapter(_monaco, worker)))
    }
    if (options.features.hovers) {
      providers.push(_monaco.languages.registerHoverProvider(languageId, new HoverAdapter(_monaco, worker)))
    }
    if (options.features.foldingRanges) {
      providers.push(_monaco.languages.registerFoldingRangeProvider(languageId, new FoldingRangeAdapter(_monaco, worker)))
    }
    if (options.features.rangeFormatting) {
      providers.push(_monaco.languages.registerDocumentRangeFormattingEditProvider(languageId, new FormatAdapter(_monaco, worker)))
    }
    if (options.features.diagnostics) {
      providers.push(new DiagnosticsAdapter(_monaco, worker, languageServiceOptions))
    }
    if (options.features.tokens) {
      _monaco.languages.setTokensProvider(languageId, createTokenizationSupport())
    }
    if (options.features.codeLens) {
      _monaco.editor.registerCommand(runQueryCommand, (accessor, modelId: string, httpQuery: HttpQuery) => {
        const queryRunner = restClient.getQueryRunner(modelId)
        if (queryRunner !== undefined) {
          queryRunner([httpQuery])
        }
      })
      providers.push(_monaco.languages.registerCodeLensProvider(languageId, new CodeLensAdapter(worker)))
    }

    disposables.push(_monaco.languages.setLanguageConfiguration(languageId, restClientLanguageConfiguration))
    disposables.push(asDisposable(providers))

    return asDisposable(disposables)
  })

  return restClient
}

export interface CreateEditorOptions {
  forceEmptyLastLine?: boolean;
  formatOnSuggest?: boolean;
  expandSuggestionDocs?: boolean;
  queryRunner?: QueryRunner
}

export class RestClientLanguage {
  private _queryRunners = new Map<string, QueryRunner>()

  private defaultRunKeyBindings = [
    this._monaco.KeyMod.CtrlCmd | this._monaco.KeyCode.Enter,
  ]
  private defaultRunAllKeyBindings = [
    this._monaco.KeyMod.CtrlCmd | this._monaco.KeyMod.Shift | this._monaco.KeyCode.Enter,
  ]
  constructor(private readonly _monaco: typeof monaco, private _options: LanguageServiceOptions) { }
  set options(options: RestClientOptions) {
    this._options.setOptions({
      ...this._options.options, ...options,
    })
  }
  createEditor(domElement: HTMLElement, options?: monaco.editor.IStandaloneEditorConstructionOptions, createOptions?: CreateEditorOptions) {
    let overrideServices: any = undefined
    if (createOptions?.expandSuggestionDocs ?? true) {
      overrideServices = {
        // This is a hack to always show the documentation
        storageService: {
          get() {},
          remove() {},
          // To disable F1 behavior (command list), remove the following line
          getNumber(key: string) { return 0 },
          getBoolean(key: string) { return key === 'expandSuggestionDocs'},
          store() {},
          onWillSaveState() {},
          onDidChangeStorage() {},
        },
      }
    }
    const editor = this._monaco.editor.create(domElement, options, overrideServices)

    if (createOptions?.forceEmptyLastLine ?? true) {
      this.enableForceEmptyLastLine(editor)
    }
    if (createOptions?.queryRunner !== undefined) {
      this.registerRunQueryAction(editor, createOptions?.queryRunner)
    }
    if (createOptions?.formatOnSuggest ?? true) {
      this.enableFormatOnSuggest(editor)
    }
    return editor
  }
  getQueryRunner(modelId: string) {
    return this._queryRunners.get(modelId)
  }
  registerQueryRunner(modelId: string, fn: QueryRunner) {
    this._queryRunners.set(modelId, fn)
  }
  unregisterQueryRunner(modelId: string) {
    this._queryRunners.delete(modelId)
  }
  setModelOptions(modelUri: string, options: RestClientOptions) {
    this._options.setModelOptions(modelUri, options)
  }

  getParsedRequests(editor: monaco.editor.IStandaloneCodeEditor) {
    if (!editor) {
      return undefined
    }
    const model = editor.getModel()
    const textDoc = TextDocument.create(model.uri.toString(), languageId, model.getVersionId(), model.getValue())
    const requestsDoc = parse(textDoc, this._options.options)
    return (requestsDoc.requests ?? []).map(request => buildRequest(request, textDoc))
  }

  getTokens(content: string) {
    const scanner = new JSONScanner(content)
    let tokens = []
    let token = scanner.scanNext(false)
    while(token !== SyntaxKind.EOF) {
      String.fromCharCode(scanner.mustacheStart0, scanner.mustacheStart1)
      const mustacheTemplateStart = `${String.fromCharCode(scanner.mustacheStart0)}${scanner.mustacheStart1 === undefined ? '' : String.fromCharCode(scanner.mustacheStart1)}`
      const mustacheTemplateEnd = `${String.fromCharCode(scanner.mustacheEnd0)}${scanner.mustacheEnd1 === undefined ? '' : String.fromCharCode(scanner.mustacheEnd1)}`
      tokens.push({
        kind: token,
        offset: scanner.getTokenOffset(),
        position: scanner.getPosition(),
        value: scanner.getTokenValue(),
        mustacheTemplateStart,
        mustacheTemplateEnd
      })
      token = scanner.scanNext(false)
    }
    return tokens
  }

  parseContent(editor: monaco.editor.IStandaloneCodeEditor, content: string) {
    const model = editor.getModel()
    const textDoc = TextDocument.create(model.uri.toString(), languageId, model.getVersionId(), content)
    return parse(textDoc, this._options.options)
  }

  registerRunAllQueryAction(editor: monaco.editor.IStandaloneCodeEditor, queryRunner?: QueryRunner, keybindings?: number[]) {
    if (queryRunner !== undefined) {
      this.registerQueryRunner(editor.getModel().uri.toString(), queryRunner)
    }
    const runAction: monaco.editor.IActionDescriptor = {
      id: 'editor.action.runAllRequest',
      label: 'Run all request',
      keybindings: keybindings ?? this.defaultRunAllKeyBindings,
      precondition: null,
      keybindingContext: null,

      run: (editor: monaco.editor.IStandaloneCodeEditor) => {
        const model = editor.getModel()
        const queryRunner = this.getQueryRunner(model.uri.toString())
        if (queryRunner === undefined) {
          return
        }
        const position = editor.getPosition()
        if (!model || !position) { return }
        const textDoc = TextDocument.create(model.uri.toString(), languageId, model.getVersionId(), model.getValue())
        const requestsDoc = parse(textDoc, this._options.options);

        const requests = (requestsDoc?.requests ?? [])
          .map(request => buildRequest(request, textDoc))
          .filter(request => !!request)
        queryRunner(requests)
      },
    }
    editor.addAction(runAction)
  }
  private enableForceEmptyLastLine(editor: monaco.editor.IStandaloneCodeEditor) {
    editor.onDidChangeModelContent((model) => {
      if (editor.getModel().getLineContent(editor.getModel().getLineCount()) !== '') {
        editor.executeEdits(null, [{
          text: '\n',
          range: {
            startLineNumber: editor.getModel().getLineCount() + 1,
            endLineNumber: editor.getModel().getLineCount() + 1,
            startColumn: 0,
            endColumn: 0,
          },
          forceMoveMarkers: false,
        }])
      }
    })
  }
  private enableFormatOnSuggest(editor: monaco.editor.IStandaloneCodeEditor) {
    const suggest = editor.getContribution<any>('editor.contrib.suggestController')
    suggest._alertCompletionItemOld = suggest._alertCompletionItem
    suggest._alertCompletionItem = (completion: any) => {
      if (!!editor) {
        editor.getAction('editor.action.formatDocument').run()
      }
      suggest._alertCompletionItemOld(completion)
    }
  }
  private registerRunQueryAction(editor: monaco.editor.IStandaloneCodeEditor, queryRunner?: QueryRunner, keybindings?: number[]) {
    if (queryRunner !== undefined) {
      this.registerQueryRunner(editor.getModel().uri.toString(), queryRunner)
    }
    const runAction: monaco.editor.IActionDescriptor = {
      id: 'editor.action.runRequest',
      label: 'Run request',
      keybindings: keybindings ?? this.defaultRunKeyBindings,
      precondition: null,
      keybindingContext: null,

      run: (editor: monaco.editor.IStandaloneCodeEditor) => {
        const model = editor.getModel()
        const queryRunner = this.getQueryRunner(model.uri.toString())
        if (queryRunner === undefined) {
          return
        }
        const position = editor.getPosition()
        if (!model || !position) { return }
        const textDoc = TextDocument.create(model.uri.toString(), languageId, model.getVersionId(), model.getValue())
        const requestsDoc = parse(textDoc, this._options.options)
        const offset = textDoc.offsetAt({ line: position.lineNumber - 1, character: position.column - 1 })
        const request = requestsDoc.getRequestAt(offset)
        if (!!request) {
          queryRunner([buildRequest(request, textDoc)])
        }
      },
    }
    editor.addAction(runAction)
  }
}

export interface WorkerAccessor {
  (first: monaco.Uri, ...more: monaco.Uri[]): Promise<RestClientWorker>;
}

function asDisposable(disposables: monaco.IDisposable[]): monaco.IDisposable {
  return { dispose: () => disposeAll(disposables) }
}

function disposeAll(disposables: monaco.IDisposable[]) {
  while (disposables.length) {
    disposables.pop().dispose()
  }
}
