import { languageId, WorkerAccessor } from '../../monaco-rest-client'
import { ProtocolToMonacoConverter } from '../../utils/monaco-converter'
import type * as monaco from 'monaco-editor-core'
import { LanguageServiceOptions } from '../../configuration/language-service-options'

export class DiagnosticsAdapter {
  p2m: ProtocolToMonacoConverter
  private _disposables: monaco.IDisposable[] = []
  private _listener: { [uri: string]: monaco.IDisposable } = Object.create(null)
  constructor(
    private readonly _monaco: typeof monaco,
    private _worker: WorkerAccessor,
    defaults: LanguageServiceOptions,
  ) {
    this.p2m = new ProtocolToMonacoConverter(_monaco)
    const onModelAdd = (model: monaco.editor.IModel): void => {
      let modeId = model.getModeId()
      if (modeId !== languageId) {
        return
      }

      let handle: number
      this._listener[model.uri.toString()] = model.onDidChangeContent(() => {
        window.clearTimeout(handle)
        handle = window.setTimeout(() => this._doValidate(model.uri, modeId), 500)
      })

      this._doValidate(model.uri, modeId)
    }

    const onModelRemoved = (model: monaco.editor.IModel): void => {
      this._monaco.editor.setModelMarkers(model, languageId, [])

      let uriStr = model.uri.toString()
      let listener = this._listener[uriStr]
      if (listener) {
        listener.dispose()
        delete this._listener[uriStr]
      }
    }

    this._disposables.push(this._monaco.editor.onDidCreateModel(onModelAdd))
    this._disposables.push(this._monaco.editor.onWillDisposeModel(onModelRemoved))
    this._disposables.push(
      this._monaco.editor.onDidChangeModelLanguage((event) => {
        onModelRemoved(event.model)
        onModelAdd(event.model)
      }),
    )

    defaults.onDidChange((_) => {
      this._monaco.editor.getModels().forEach((model) => {
        if (model.getModeId() === languageId) {
          onModelRemoved(model)
          onModelAdd(model)
        }
      })
    })

    this._disposables.push({
      dispose: () => {
        for (let key in this._listener) {
          this._listener[key].dispose()
        }
      },
    })

    this._monaco.editor.getModels().forEach(onModelAdd)
  }

  public dispose(): void {
    this._disposables.forEach((d) => d && d.dispose())
    this._disposables = []
  }

  private _doValidate(resource: monaco.Uri, languageId: string): void {
    this._worker(resource)
      .then((worker) => {
        return worker.doValidation(resource.toString())
      })
      .then((diagnostics) => {
        const markers = this.p2m.asDiagnostics(diagnostics)
        let model = this._monaco.editor.getModel(resource)
        if (model && model.getModeId() === languageId) {
          this._monaco.editor.setModelMarkers(model, languageId, markers)
        }
      })
      .then(undefined, (err) => {
        console.error(err)
      })
  }
}
