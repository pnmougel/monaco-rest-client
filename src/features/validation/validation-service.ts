import { TextDocument } from 'vscode-languageserver-textdocument'
import { RequestsDocument } from '../../models/requests-document'
import { Diagnostic } from 'vscode-languageserver-protocol'
import { DiagnosticSeverity, Range } from 'vscode-languageserver-types'
import { ErrorCode } from '../../models/enums/error-codes'
import { OpenApiService } from '../../services/open-api-service'
import { ValidationResult } from './validation-result'
import { NoOpSchemaCollector } from '../schema/schema-collector'
import { validate } from './validate'
import { CharacterCodes } from '../../models/enums/character-codes'
import { RestClientOptions } from '../../configuration/rest-client-options'

export class ValidationService {
  public constructor(private readonly openApiService: OpenApiService) {}

  public doValidation(textDocument: TextDocument, requestsDocument: RequestsDocument, options: RestClientOptions): Diagnostic[] {
    if (!(options.validate ?? true)) {
      return []
    }
    const diagnostics: Diagnostic[] = []

    const added: { [signature: string]: boolean } = {}
    const addProblem = (problem: Diagnostic) => {
      // remove duplicated messages
      const signature = problem.range.start.line + ' ' + problem.range.start.character + ' ' + problem.message
      if (!added[signature]) {
        added[signature] = true
        diagnostics.push(problem)
      }
    }

    const validationResult = new ValidationResult()
    if (this.openApiService.spec !== undefined) {
      requestsDocument.requests.forEach(req => {
        if (options.schemaDef === undefined) {
          const pathDefinition = this.openApiService.getPathDefinition(req, options.schemaDef)
          if (!pathDefinition) {
            let node = !!req.url ? req.url : (!!req.method ? req.method : req)

            const range = Range.create(textDocument.positionAt(node.offset), textDocument.positionAt(node.offset + node.length))
            addProblem(Diagnostic.create(range, 'Invalid path', DiagnosticSeverity.Warning, ErrorCode.InvalidPath))
          } else if (!!req.url) {
            req.url.queryParams.forEach(queryParam => {
              if (!!queryParam.keyNode?.value) {
                const key = queryParam.keyNode.value.toLowerCase()
                const value = (queryParam.valueNode?.value ?? '').toLowerCase()

                const matchingParam = pathDefinition.getQueryParam(key)
                if (!matchingParam) {
                  const range = Range.create(textDocument.positionAt(queryParam.keyNode.offset), textDocument.positionAt(queryParam.keyNode.offset + queryParam.keyNode.length))
                  addProblem(Diagnostic.create(range, 'Invalid query key', DiagnosticSeverity.Warning, ErrorCode.InvalidQueryParamKey))
                } else if (!!matchingParam.schema && !!value) {
                  const range = Range.create(textDocument.positionAt(queryParam.valueNode.offset), textDocument.positionAt(queryParam.valueNode.offset + queryParam.valueNode.length))
                  if (matchingParam.schema.type === 'boolean' && value !== 'true' && value !== 'false') {
                    addProblem(Diagnostic.create(range, 'Expected true or false', DiagnosticSeverity.Warning, ErrorCode.InvalidQueryParamValue))
                  } else if (matchingParam.schema.type === 'string' && Array.isArray(matchingParam.schema.enum)) {
                    const options = matchingParam.schema.enum ?? []
                    let found = false
                    for (let i = 0; i !== options.length && !found; i++) {
                      found = options[i].toLowerCase() === value.toLowerCase()
                    }
                    if (!found) {
                      addProblem(Diagnostic.create(range, `Expected one of ${options.join(', ')}.`, DiagnosticSeverity.Warning, ErrorCode.InvalidQueryParamValue))
                    }
                  } else if (matchingParam.schema.type === 'number') {
                    let isNumber = true
                    for (let i = 0; i !== value.length && isNumber; i++) {
                      const code = value.charCodeAt(i)
                      isNumber = code >= CharacterCodes._0 && code <= CharacterCodes._9
                    }
                    if (!isNumber) {
                      addProblem(Diagnostic.create(range, `Numeric value expected`, DiagnosticSeverity.Warning, ErrorCode.InvalidQueryParamValue))
                    }
                  }
                }
              }
            })
          }
        }

        const schema = this.openApiService.getSchemaForRequest(req, options.schemaDef)
        if (!!req.json) {
          if (schema === undefined) {
            const range = Range.create(textDocument.positionAt(req.json.offset), textDocument.positionAt(req.json.offset + req.json.length))
            addProblem(Diagnostic.create(range, `Unexpected body`, DiagnosticSeverity.Warning, ErrorCode.UnexpectedBody))
          } else {
            validate(this.openApiService, req.json, schema, validationResult, NoOpSchemaCollector.instance)
          }
        }
      })
    }

    const semanticErrors = validationResult.problems.map(p => {
      const range = Range.create(textDocument.positionAt(p.location.offset), textDocument.positionAt(p.location.offset + p.location.length))
      return Diagnostic.create(range, p.message, p.severity ?? DiagnosticSeverity.Warning, p.code)
    })
    if (semanticErrors) {
      semanticErrors.forEach(addProblem)
    }

    for (const p of requestsDocument.syntaxErrors) {
      if (p.code === ErrorCode.TrailingComma) {
        if (options.allowTrailingComma ?? true) {
          continue
        }
        p.severity = DiagnosticSeverity.Error
      }
      addProblem(p)
    }
    return diagnostics
  }
}
