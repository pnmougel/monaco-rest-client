import { ASTNode } from '../../models/ast/ast-node'
import { PropertyASTNode } from '../../models/ast/json/property-node'
import { NumberASTNode } from '../../models/ast/json/number-node'
import { StringASTNode } from '../../models/ast/json/string-node'
import { ArrayASTNode } from '../../models/ast/json/array-node'
import { ObjectASTNode } from '../../models/ast/json/object-node'
import { localize } from '../../utils/localize'
import { getNodeValue } from '../../utils/node-utils'
import { asSchema } from '../../utils/as-schema'
import { ErrorCode } from '../../models/enums/error-codes'
import { equals, isBoolean, isDefined, isNumber, isString } from '../../utils/objects'
import { DiagnosticSeverity } from 'vscode-languageserver-types'
import { extendedRegExp } from '../../utils/strings'
import { formats } from '../../models/enums/formats'
import { ValidationResult } from './validation-result'
import { JSONSchema, JSONSchemaRef } from '../schema/json-schema'
import { ISchemaCollector, NoOpSchemaCollector } from '../schema/schema-collector'
import { OpenApiService } from '../../services/open-api-service'

export function validate(openApiService: OpenApiService, n: ASTNode | undefined, schema: JSONSchema, validationResult: ValidationResult, matchingSchemas: ISchemaCollector): void {
  if (!n || !matchingSchemas.include(n)) {
    return
  }

  schema = openApiService.resolveEntry(schema)

  const node = n
  switch (node.type) {
    case 'object':
      _validateObjectNode(openApiService, node as ObjectASTNode, schema, validationResult, matchingSchemas)
      break
    case 'array':
      _validateArrayNode(openApiService, node as ArrayASTNode, schema, validationResult, matchingSchemas)
      break
    case 'string':
      _validateStringNode(openApiService, node as StringASTNode, schema, validationResult, matchingSchemas)
      break
    case 'number':
      _validateNumberNode(openApiService, node as NumberASTNode, schema, validationResult, matchingSchemas)
      break
    case 'property':
      return validate(openApiService, (node as PropertyASTNode).valueNode, schema, validationResult, matchingSchemas)
  }
  _validateNode()

  matchingSchemas.add({ node: node, schema: schema })

  function _validateNode() {
    function matchesType(type: string) {
      return node.type === type || node.type === 'template' || (type === 'integer' && node.type === 'number' && (node as NumberASTNode).isInteger)
    }

    if (Array.isArray(schema.type)) {
      if (!schema.type.some(matchesType)) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: schema.errorMessage || localize('typeArrayMismatchWarning', 'Incorrect type. Expected one of {0}.', (<string[]>schema.type).join(', ')),
        })
      }
    } else if (schema.type) {
      if (!matchesType(schema.type)) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: schema.errorMessage || localize('typeMismatchWarning', 'Incorrect type. Expected "{0}".', schema.type),
        })
      }
    }
    if (Array.isArray(schema.allOf)) {
      for (const subSchemaRef of schema.allOf) {
        validate(openApiService, node, asSchema(subSchemaRef), validationResult, matchingSchemas)
      }
    }
    const notSchema = asSchema(schema.not)
    if (notSchema) {
      const subValidationResult = new ValidationResult()
      const subMatchingSchemas = matchingSchemas.newSub()
      validate(openApiService, node, notSchema, subValidationResult, subMatchingSchemas)
      if (!subValidationResult.hasProblems()) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: localize('notSchemaWarning', 'Matches a schema that is not allowed.'),
        })
      }
      for (const ms of subMatchingSchemas.schemas) {
        ms.inverted = !ms.inverted
        matchingSchemas.add(ms)
      }
    }

    const testAlternatives = (alternatives: JSONSchemaRef[], maxOneMatch: boolean) => {
      const matches = []

      // remember the best match that is used for error messages
      let bestMatch: { schema: JSONSchema; validationResult: ValidationResult; matchingSchemas: ISchemaCollector; } | undefined = undefined
      for (const subSchemaRef of alternatives) {
        const subSchema = asSchema(subSchemaRef)
        const subValidationResult = new ValidationResult()
        const subMatchingSchemas = matchingSchemas.newSub()
        validate(openApiService, node, subSchema, subValidationResult, subMatchingSchemas)
        if (!subValidationResult.hasProblems()) {
          matches.push(subSchema)
        }
        if (!bestMatch) {
          bestMatch = { schema: subSchema, validationResult: subValidationResult, matchingSchemas: subMatchingSchemas }
        } else {
          if (!maxOneMatch && !subValidationResult.hasProblems() && !bestMatch.validationResult.hasProblems()) {
            // no errors, both are equally good matches
            bestMatch.matchingSchemas.merge(subMatchingSchemas)
            bestMatch.validationResult.propertiesMatches += subValidationResult.propertiesMatches
            bestMatch.validationResult.propertiesValueMatches += subValidationResult.propertiesValueMatches
          } else {
            const compareResult = subValidationResult.compare(bestMatch.validationResult)
            if (compareResult > 0) {
              // our node is the best matching so far
              bestMatch = {
                schema: subSchema,
                validationResult: subValidationResult,
                matchingSchemas: subMatchingSchemas,
              }
            } else if (compareResult === 0) {
              // there's already a best matching but we are as good
              bestMatch.matchingSchemas.merge(subMatchingSchemas)
              bestMatch.validationResult.mergeEnumValues(subValidationResult)
            }
          }
        }
      }

      if (matches.length > 1 && maxOneMatch) {
        validationResult.problems.push({
          location: { offset: node.offset, length: 1 },
          message: localize('oneOfWarning', 'Matches multiple schemas when only one must validate.'),
        })
      }
      if (bestMatch) {
        validationResult.merge(bestMatch.validationResult)
        validationResult.propertiesMatches += bestMatch.validationResult.propertiesMatches
        validationResult.propertiesValueMatches += bestMatch.validationResult.propertiesValueMatches
        matchingSchemas.merge(bestMatch.matchingSchemas)
      }
      return matches.length
    }
    if (Array.isArray(schema.anyOf)) {
      testAlternatives(schema.anyOf, false)
    }
    if (Array.isArray(schema.oneOf)) {
      testAlternatives(schema.oneOf, true)
    }

    const testBranch = (schema: JSONSchemaRef) => {
      const subValidationResult = new ValidationResult()
      const subMatchingSchemas = matchingSchemas.newSub()

      validate(openApiService, node, asSchema(schema), subValidationResult, subMatchingSchemas)

      validationResult.merge(subValidationResult)
      validationResult.propertiesMatches += subValidationResult.propertiesMatches
      validationResult.propertiesValueMatches += subValidationResult.propertiesValueMatches
      matchingSchemas.merge(subMatchingSchemas)
    }

    const testCondition = (ifSchema: JSONSchemaRef, thenSchema?: JSONSchemaRef, elseSchema?: JSONSchemaRef) => {
      const subSchema = asSchema(ifSchema)
      const subValidationResult = new ValidationResult()
      const subMatchingSchemas = matchingSchemas.newSub()

      validate(openApiService, node, subSchema, subValidationResult, subMatchingSchemas)
      matchingSchemas.merge(subMatchingSchemas)

      if (!subValidationResult.hasProblems()) {
        if (thenSchema) {
          testBranch(thenSchema)
        }
      } else if (elseSchema) {
        testBranch(elseSchema)
      }
    }

    const ifSchema = asSchema(schema.if)
    if (ifSchema) {
      testCondition(ifSchema, asSchema(schema.then), asSchema(schema.else))
    }

    if (Array.isArray(schema.enum)) {
      const val = getNodeValue(node)
      let enumValueMatch = false
      for (const e of schema.enum) {
        if (equals(val, e)) {
          enumValueMatch = true
          break
        }
      }
      validationResult.enumValues = schema.enum
      validationResult.enumValueMatch = enumValueMatch
      if (!enumValueMatch) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          code: ErrorCode.EnumValueMismatch,
          message: schema.errorMessage || localize('enumWarning', 'Value is not accepted. Valid values: {0}.', schema.enum.map(v => JSON.stringify(v)).join(', ')),
        })
      }
    }

    if (isDefined(schema.const)) {
      const val = getNodeValue(node)
      if (!equals(val, schema.const)) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          code: ErrorCode.EnumValueMismatch,
          message: schema.errorMessage || localize('constWarning', 'Value must be {0}.', JSON.stringify(schema.const)),
        })
        validationResult.enumValueMatch = false
      } else {
        validationResult.enumValueMatch = true
      }
      validationResult.enumValues = [schema.const]
    }

    if (schema.deprecationMessage && node.parent) {
      validationResult.problems.push({
        location: { offset: node.parent.offset, length: node.parent.length },
        severity: DiagnosticSeverity.Warning,
        message: schema.deprecationMessage,
        code: ErrorCode.Deprecated,
      })
    }
  }


  function _validateNumberNode(openApiService: OpenApiService, node: NumberASTNode, schema: JSONSchema, validationResult: ValidationResult, matchingSchemas: ISchemaCollector): void {
    const val = node.value

    function normalizeFloats(float: number): { value: number, multiplier: number } | null {
      const parts = /^(-?\d+)(?:\.(\d+))?(?:e([-+]\d+))?$/.exec(float.toString())
      return parts && {
        value: Number(parts[1] + (parts[2] || '')),
        multiplier: (parts[2]?.length || 0) - (parseInt(parts[3]) || 0),
      }
    }

    if (isNumber(schema.multipleOf)) {
      let remainder: number = -1
      if (Number.isInteger(schema.multipleOf)) {
        remainder = val % schema.multipleOf
      } else {
        let normMultipleOf = normalizeFloats(schema.multipleOf)
        let normValue = normalizeFloats(val)
        if (normMultipleOf && normValue) {
          const multiplier = 10 ** Math.abs(normValue.multiplier - normMultipleOf.multiplier)
          if (normValue.multiplier < normMultipleOf.multiplier) {
            normValue.value *= multiplier
          } else {
            normMultipleOf.value *= multiplier
          }
          remainder = normValue.value % normMultipleOf.value
        }
      }
      if (remainder !== 0) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: localize('multipleOfWarning', 'Value is not divisible by {0}.', schema.multipleOf),
        })
      }
    }

    function getExclusiveLimit(limit: number | undefined, exclusive: boolean | number | undefined): number | undefined {
      if (isNumber(exclusive)) {
        return exclusive
      }
      if (isBoolean(exclusive) && exclusive) {
        return limit
      }
      return undefined
    }

    function getLimit(limit: number | undefined, exclusive: boolean | number | undefined): number | undefined {
      if (!isBoolean(exclusive) || !exclusive) {
        return limit
      }
      return undefined
    }

    const exclusiveMinimum = getExclusiveLimit(schema.minimum, schema.exclusiveMinimum)
    if (isNumber(exclusiveMinimum) && val <= exclusiveMinimum) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('exclusiveMinimumWarning', 'Value is below the exclusive minimum of {0}.', exclusiveMinimum),
      })
    }
    const exclusiveMaximum = getExclusiveLimit(schema.maximum, schema.exclusiveMaximum)
    if (isNumber(exclusiveMaximum) && val >= exclusiveMaximum) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('exclusiveMaximumWarning', 'Value is above the exclusive maximum of {0}.', exclusiveMaximum),
      })
    }
    const minimum = getLimit(schema.minimum, schema.exclusiveMinimum)
    if (isNumber(minimum) && val < minimum) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('minimumWarning', 'Value is below the minimum of {0}.', minimum),
      })
    }
    const maximum = getLimit(schema.maximum, schema.exclusiveMaximum)
    if (isNumber(maximum) && val > maximum) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('maximumWarning', 'Value is above the maximum of {0}.', maximum),
      })
    }
  }

  function _validateStringNode(openApiService: OpenApiService, node: StringASTNode, schema: JSONSchema, validationResult: ValidationResult, matchingSchemas: ISchemaCollector): void {
    if (isNumber(schema.minLength) && node.value.length < schema.minLength) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('minLengthWarning', 'String is shorter than the minimum length of {0}.', schema.minLength),
      })
    }

    if (isNumber(schema.maxLength) && node.value.length > schema.maxLength) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('maxLengthWarning', 'String is longer than the maximum length of {0}.', schema.maxLength),
      })
    }

    if (isString(schema.pattern)) {
      const regex = extendedRegExp(schema.pattern)
      if (!regex.test(node.value)) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: schema.patternErrorMessage || schema.errorMessage || localize('patternWarning', 'String does not match the pattern of "{0}".', schema.pattern),
        })
      }
    }

    if (schema.format) {
      switch (schema.format) {
        case 'uri':
        case 'uri-reference': {
          let errorMessage
          if (!node.value) {
            errorMessage = localize('uriEmpty', 'URI expected.')
          } else {
            const match = /^(([^:/?#]+?):)?(\/\/([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/.exec(node.value)
            if (!match) {
              errorMessage = localize('uriMissing', 'URI is expected.')
            } else if (!match[2] && schema.format === 'uri') {
              errorMessage = localize('uriSchemeMissing', 'URI with a scheme is expected.')
            }
          }
          if (errorMessage) {
            validationResult.problems.push({
              location: { offset: node.offset, length: node.length },
              message: schema.patternErrorMessage || schema.errorMessage || localize('uriFormatWarning', 'String is not a URI: {0}', errorMessage),
            })
          }
        }
          break
        case 'color-hex':
        case 'date-time':
        case 'date':
        case 'time':
        case 'email':
          const format = formats[schema.format]
          if (!node.value || !format.pattern.exec(node.value)) {
            validationResult.problems.push({
              location: { offset: node.offset, length: node.length },
              message: schema.patternErrorMessage || schema.errorMessage || format.errorMessage,
            })
          }
      }
    }

  }

  function _validateArrayNode(openApiService: OpenApiService, node: ArrayASTNode, schema: JSONSchema, validationResult: ValidationResult, matchingSchemas: ISchemaCollector): void {
    if (Array.isArray(schema.items)) {
      const subSchemas = schema.items
      for (let index = 0; index < subSchemas.length; index++) {
        const subSchemaRef = subSchemas[index]
        const subSchema = asSchema(subSchemaRef)
        const itemValidationResult = new ValidationResult()
        const item = node.items[index]
        if (item) {
          validate(openApiService, item, subSchema, itemValidationResult, matchingSchemas)
          validationResult.mergePropertyMatch(itemValidationResult)
        } else if (node.items.length >= subSchemas.length) {
          validationResult.propertiesValueMatches++
        }
      }
      if (node.items.length > subSchemas.length) {
        if (typeof schema.additionalItems === 'object') {
          for (let i = subSchemas.length; i < node.items.length; i++) {
            const itemValidationResult = new ValidationResult()
            validate(openApiService, node.items[i], <any>schema.additionalItems, itemValidationResult, matchingSchemas)
            validationResult.mergePropertyMatch(itemValidationResult)
          }
        } else if (schema.additionalItems === false) {
          validationResult.problems.push({
            location: { offset: node.offset, length: node.length },
            message: localize('additionalItemsWarning', 'Array has too many items according to schema. Expected {0} or fewer.', subSchemas.length),
          })
        }
      }
    } else {
      const itemSchema = asSchema(schema.items)
      if (itemSchema) {
        for (const item of node.items) {
          const itemValidationResult = new ValidationResult()
          validate(openApiService, item, itemSchema, itemValidationResult, matchingSchemas)
          validationResult.mergePropertyMatch(itemValidationResult)
        }
      }
    }

    const containsSchema = asSchema(schema.contains)
    if (containsSchema) {
      const doesContain = node.items.some(item => {
        const itemValidationResult = new ValidationResult()
        validate(openApiService, item, containsSchema, itemValidationResult, NoOpSchemaCollector.instance)
        return !itemValidationResult.hasProblems()
      })

      if (!doesContain) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: schema.errorMessage || localize('requiredItemMissingWarning', 'Array does not contain required item.'),
        })
      }
    }

    if (isNumber(schema.minItems) && node.items.length < schema.minItems) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('minItemsWarning', 'Array has too few items. Expected {0} or more.', schema.minItems),
      })
    }

    if (isNumber(schema.maxItems) && node.items.length > schema.maxItems) {
      validationResult.problems.push({
        location: { offset: node.offset, length: node.length },
        message: localize('maxItemsWarning', 'Array has too many items. Expected {0} or fewer.', schema.maxItems),
      })
    }

    if (schema.uniqueItems === true) {
      const values = getNodeValue(node)
      const duplicates = values.some((value: any, index: number) => {
        return index !== values.lastIndexOf(value)
      })
      if (duplicates) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: localize('uniqueItemsWarning', 'Array has duplicate items.'),
        })
      }
    }

  }

  function _validateObjectNode(openApiService: OpenApiService, node: ObjectASTNode, schema: JSONSchema, validationResult: ValidationResult, matchingSchemas: ISchemaCollector): void {
    const seenKeys: { [key: string]: ASTNode | undefined } = Object.create(null)
    const unprocessedProperties: string[] = []
    for (const propertyNode of node.properties) {
      if (propertyNode.keyNode instanceof StringASTNode) {
        const key = propertyNode.keyNode.value
        seenKeys[key] = propertyNode.valueNode
        unprocessedProperties.push(key)
      }
    }

    if (Array.isArray(schema.required)) {
      for (const propertyName of schema.required) {
        if (!seenKeys[propertyName]) {
          const keyNode = node.parent && node.parent.type === 'property' && (node.parent as PropertyASTNode).keyNode
          const location = keyNode ? { offset: keyNode.offset, length: keyNode.length } : {
            offset: node.offset,
            length: 1,
          }
          validationResult.problems.push({
            location: location,
            message: localize('MissingRequiredPropWarning', 'Missing property "{0}".', propertyName),
          })
        }
      }
    }

    const propertyProcessed = (prop: string) => {
      let index = unprocessedProperties.indexOf(prop)
      while (index >= 0) {
        unprocessedProperties.splice(index, 1)
        index = unprocessedProperties.indexOf(prop)
      }
    }

    if (schema.properties) {
      for (const propertyName of Object.keys(schema.properties)) {
        propertyProcessed(propertyName)
        const propertySchema = schema.properties[propertyName]
        const child = seenKeys[propertyName]
        if (child) {
          if (isBoolean(propertySchema)) {
            if (!propertySchema) {
              const propertyNode = <PropertyASTNode>child.parent
              validationResult.problems.push({
                location: { offset: propertyNode.keyNode.offset, length: propertyNode.keyNode.length },
                message: schema.errorMessage || localize('DisallowedExtraPropWarning', 'Property {0} is not allowed.', propertyName),
              })
            } else {
              validationResult.propertiesMatches++
              validationResult.propertiesValueMatches++
            }
          } else {
            const propertyValidationResult = new ValidationResult()
            validate(openApiService, child, propertySchema, propertyValidationResult, matchingSchemas)
            validationResult.mergePropertyMatch(propertyValidationResult)
          }
        }
      }
    }

    if (schema.patternProperties) {
      for (const propertyPattern of Object.keys(schema.patternProperties)) {
        const regex = extendedRegExp(propertyPattern)
        for (const propertyName of unprocessedProperties.slice(0)) {
          if (regex.test(propertyName)) {
            propertyProcessed(propertyName)
            const child = seenKeys[propertyName]
            if (child) {
              const propertySchema = schema.patternProperties[propertyPattern]
              if (isBoolean(propertySchema)) {
                if (!propertySchema) {
                  const propertyNode = <PropertyASTNode>child.parent
                  validationResult.problems.push({
                    location: { offset: propertyNode.keyNode.offset, length: propertyNode.keyNode.length },
                    message: schema.errorMessage || localize('DisallowedExtraPropWarning', 'Property {0} is not allowed.', propertyName),
                  })
                } else {
                  validationResult.propertiesMatches++
                  validationResult.propertiesValueMatches++
                }
              } else {
                const propertyValidationResult = new ValidationResult()
                validate(openApiService, child, propertySchema, propertyValidationResult, matchingSchemas)
                validationResult.mergePropertyMatch(propertyValidationResult)
              }
            }
          }
        }
      }
    }

    if (typeof schema.additionalProperties === 'object') {
      for (const propertyName of unprocessedProperties) {
        const child = seenKeys[propertyName]
        if (child) {
          const propertyValidationResult = new ValidationResult()
          validate(openApiService, child, <any>schema.additionalProperties, propertyValidationResult, matchingSchemas)
          validationResult.mergePropertyMatch(propertyValidationResult)
        }
      }
    } else if (schema.additionalProperties === false) {
      if (unprocessedProperties.length > 0) {
        for (const propertyName of unprocessedProperties) {
          const child = seenKeys[propertyName]
          if (child) {
            const propertyNode = <PropertyASTNode>child.parent

            validationResult.problems.push({
              location: { offset: propertyNode.keyNode.offset, length: propertyNode.keyNode.length },
              message: schema.errorMessage || localize('DisallowedExtraPropWarning', 'Property {0} is not allowed.', propertyName),
            })
          }
        }
      }
    }

    if (isNumber(schema.maxProperties)) {
      if (node.properties.length > schema.maxProperties) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: localize('MaxPropWarning', 'Object has more properties than limit of {0}.', schema.maxProperties),
        })
      }
    }

    if (isNumber(schema.minProperties)) {
      if (node.properties.length < schema.minProperties) {
        validationResult.problems.push({
          location: { offset: node.offset, length: node.length },
          message: localize('MinPropWarning', 'Object has fewer properties than the required number of {0}', schema.minProperties),
        })
      }
    }

    if (schema.dependencies) {
      for (const key of Object.keys(schema.dependencies)) {
        const prop = seenKeys[key]
        if (prop) {
          const propertyDep = schema.dependencies[key]
          if (Array.isArray(propertyDep)) {
            for (const requiredProp of propertyDep) {
              if (!seenKeys[requiredProp]) {
                validationResult.problems.push({
                  location: { offset: node.offset, length: node.length },
                  message: localize('RequiredDependentPropWarning', 'Object is missing property {0} required by property {1}.', requiredProp, key),
                })
              } else {
                validationResult.propertiesValueMatches++
              }
            }
          } else {
            const propertySchema = asSchema(propertyDep)
            if (propertySchema) {
              const propertyValidationResult = new ValidationResult()
              validate(openApiService, node, propertySchema, propertyValidationResult, matchingSchemas)
              validationResult.mergePropertyMatch(propertyValidationResult)
            }
          }
        }
      }
    }

    const propertyNames = asSchema(schema.propertyNames)
    if (propertyNames) {
      for (const f of node.properties) {
        const key = f.keyNode
        if (key) {
          validate(openApiService, key, propertyNames, validationResult, NoOpSchemaCollector.instance)
        }
      }
    }
  }
}
