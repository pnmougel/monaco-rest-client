import { localize } from '../../utils/localize'
import { ErrorCode } from '../../models/enums/error-codes'
import { Range } from '../../models/range'
import { DiagnosticSeverity } from 'vscode-languageserver-types'

interface Problem {
  location: Range;
  severity?: DiagnosticSeverity;
  code?: ErrorCode;
  message: string;
}

export class ValidationResult {
  public problems: Problem[]

  public propertiesMatches: number
  public propertiesValueMatches: number
  public primaryValueMatches: number
  public enumValueMatch: boolean
  public enumValues: any[] | undefined

  constructor() {
    this.problems = []
    this.propertiesMatches = 0
    this.propertiesValueMatches = 0
    this.primaryValueMatches = 0
    this.enumValueMatch = false
    this.enumValues = undefined
  }

  public hasProblems(): boolean {
    return !!this.problems.length
  }

  public mergeAll(validationResults: ValidationResult[]): void {
    for (const validationResult of validationResults) {
      this.merge(validationResult)
    }
  }

  public merge(validationResult: ValidationResult): void {
    this.problems = this.problems.concat(validationResult.problems)
  }

  public mergeEnumValues(validationResult: ValidationResult): void {
    if (!this.enumValueMatch && !validationResult.enumValueMatch && this.enumValues && validationResult.enumValues) {
      this.enumValues = this.enumValues.concat(validationResult.enumValues)
      for (const error of this.problems) {
        if (error.code === ErrorCode.EnumValueMismatch) {
          error.message = localize('enumWarning', 'Value is not accepted. Valid values: {0}.', this.enumValues.map(v => JSON.stringify(v)).join(', '))
        }
      }
    }
  }

  public mergePropertyMatch(propertyValidationResult: ValidationResult): void {
    this.merge(propertyValidationResult)
    this.propertiesMatches++
    if (propertyValidationResult.enumValueMatch || !propertyValidationResult.hasProblems() && propertyValidationResult.propertiesMatches) {
      this.propertiesValueMatches++
    }
    if (propertyValidationResult.enumValueMatch && propertyValidationResult.enumValues && propertyValidationResult.enumValues.length === 1) {
      this.primaryValueMatches++
    }
  }

  public compare(other: ValidationResult): number {
    const hasProblems = this.hasProblems()
    if (hasProblems !== other.hasProblems()) {
      return hasProblems ? -1 : 1
    }
    if (this.enumValueMatch !== other.enumValueMatch) {
      return other.enumValueMatch ? -1 : 1
    }
    if (this.primaryValueMatches !== other.primaryValueMatches) {
      return this.primaryValueMatches - other.primaryValueMatches
    }
    if (this.propertiesValueMatches !== other.propertiesValueMatches) {
      return this.propertiesValueMatches - other.propertiesValueMatches
    }
    return this.propertiesMatches - other.propertiesMatches
  }

}
