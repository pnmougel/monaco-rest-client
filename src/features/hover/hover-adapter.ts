import type * as monaco from 'monaco-editor-core'
import { WorkerAccessor } from '../../monaco-rest-client'
import { MonacoToProtocolConverter, ProtocolToMonacoConverter } from '../../utils/monaco-converter'

export class HoverAdapter implements monaco.languages.HoverProvider {
  m2p: MonacoToProtocolConverter
  p2m: ProtocolToMonacoConverter

  constructor(private readonly _monaco: typeof monaco, private worker: WorkerAccessor) {
    this.m2p = new MonacoToProtocolConverter(_monaco)
    this.p2m = new ProtocolToMonacoConverter(_monaco)
  }

  provideHover(model: monaco.editor.ITextModel, position: monaco.Position, token: monaco.CancellationToken): monaco.languages.ProviderResult<monaco.languages.Hover> {
    return this.worker(model.uri)
      .then((worker) => {
        return worker.doHover(model.uri.toString(), this.m2p.asPosition(position.lineNumber, position.column))
      })
      .then((hover) => {
        return this.p2m.asHover(hover)
      })
  }
}
