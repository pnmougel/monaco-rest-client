import { OpenApiService } from '../../services/open-api-service'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { MarkedString, Position, Range } from 'vscode-languageserver-types'
import { RequestsDocument } from '../../models/requests-document'
import { Hover } from 'vscode-languageserver-protocol'
import { PropertyASTNode } from '../../models/ast/json/property-node'
import { getNodeValue } from '../../utils/node-utils'
import { JSONDocument } from '../../models/json-document'
import { RestClientOptions } from '../../configuration/rest-client-options'

export class HoverService {

  constructor(private readonly openApiService: OpenApiService) {
  }

  public doHover(document: TextDocument, position: Position, doc: RequestsDocument, options: RestClientOptions): Thenable<Hover | null> {
    const offset = document.offsetAt(position)
    const request = doc.getRequestFromOffset(offset)
    if (!request || !request.jsonDoc) {
      return Promise.resolve(null)
    }
    const schema = this.openApiService.getSchemaForRequest(request, options.schemaDef)

    return this.doHoverWithSchema(document, request.jsonDoc, offset, schema)
  }

  private doHoverWithSchema(document: TextDocument, jsonDoc: JSONDocument, offset: number, schema: any): Thenable<Hover | null> {
    let node = jsonDoc.getNodeFromOffset(offset)
    if (!node || (node.type === 'object' || node.type === 'array') && offset > node.offset + 1 && offset < node.offset + node.length - 1) {
      return Promise.resolve(null)
    }
    const hoverRangeNode = node

    // use the property description when hovering over an object key
    if (node.type === 'string') {
      const parent = node.parent
      if (parent && parent.type === 'property' && (parent as PropertyASTNode).keyNode === node) {
        node = (parent as PropertyASTNode).valueNode
        if (!node) {
          return Promise.resolve(null)
        }
      }
    }

    const hoverRange = Range.create(document.positionAt(hoverRangeNode.offset), document.positionAt(hoverRangeNode.offset + hoverRangeNode.length))

    const createHover = (contents: MarkedString[]) => {
      const result: Hover = {
        contents: contents,
        range: hoverRange,
      }
      return result
    }

    if (schema && node) {
      const matchingSchemas = jsonDoc?.getMatchingSchemas(this.openApiService, schema, node.offset)
      let title: string | undefined = undefined
      let markdownDescription: string | undefined = undefined
      let markdownEnumValueDescription: string | undefined = undefined, enumValue: string | undefined = undefined
      matchingSchemas.every((s) => {
        if (s.node === node && !s.inverted && s.schema) {
          title = title || s.schema.title
          markdownDescription = markdownDescription || s.schema.markdownDescription || toMarkdown(s.schema.description)
          if (s.schema.enum) {
            const idx = s.schema.enum.indexOf(getNodeValue(node!))
            if (s.schema.markdownEnumDescriptions) {
              markdownEnumValueDescription = s.schema.markdownEnumDescriptions[idx]
            } else if (s.schema.enumDescriptions) {
              markdownEnumValueDescription = toMarkdown(s.schema.enumDescriptions[idx])
            }
            if (markdownEnumValueDescription) {
              enumValue = s.schema.enum[idx]
              if (typeof enumValue !== 'string') {
                enumValue = JSON.stringify(enumValue)
              }
            }
          }
        }
        return true
      })
      let result = ''
      if (title) {
        result = toMarkdown(title)
      }
      if (markdownDescription) {
        if (result.length > 0) {
          result += '\n\n'
        }
        result += markdownDescription
      }
      if (markdownEnumValueDescription) {
        if (result.length > 0) {
          result += '\n\n'
        }
        result += `\`${toMarkdownCodeBlock(enumValue!)}\`: ${markdownEnumValueDescription}`
      }
      return Promise.resolve(createHover([result]))
    }
    return Promise.resolve(null)
  }
}

function toMarkdown(plain: string | undefined | { value: string }): string | undefined {
  if (plain) {
    return typeof plain === 'object' ? plain.value : plain
  }
  return undefined
}

function toMarkdownCodeBlock(content: string) {
  // see https://daringfireball.net/projects/markdown/syntax#precode
  if (content.indexOf('`') !== -1) {
    return '`` ' + content + ' ``'
  }
  return content
}
