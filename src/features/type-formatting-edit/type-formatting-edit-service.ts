import type { languages } from 'monaco-editor-core'
import { Position } from 'monaco-editor-core'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { parse } from '../../parser/parse'
import { RestClientOptions } from '../../configuration/rest-client-options'
import { OpenApiService } from '../../services/open-api-service'

export class TypeFormattingEditService {
  constructor(private openApiService: OpenApiService) {}

  getTypeFormattingEdits(document: TextDocument, position: Position, appendNewLine: boolean, isLastLine: boolean, languageOptions: RestClientOptions): languages.TextEdit[] {
    const requests = parse(document, languageOptions)
    if ((requests.syntaxErrors ?? []).length > 0 || (requests.requests ?? []).length !== 1) {
      return []
    }
    const schema = this.openApiService.getSchemaForRequest(requests.requests[0], languageOptions.schemaDef)
    if (schema?.type === undefined) {
      return []
    }
    let startElem = schema?.type === 'array' ? '[' : '{'
    let closeElem = schema?.type === 'array' ? ']' : '}'

    if (isLastLine) {
      return [{
        range: {
          startLineNumber: position.lineNumber,
          endLineNumber: position.lineNumber,
          endColumn: 0,
          startColumn: 0,
        },
        text: `${startElem}\n  \n${closeElem}`,
      }]
    } else {
      return [{
        range: {
          startLineNumber: position.lineNumber + 1,
          endLineNumber: position.lineNumber + 1,
          endColumn: 0,
          startColumn: 0,
        },
        text: `${closeElem}\n${appendNewLine ? '\n' : ''}`,
      }, {
        range: {
          startLineNumber: position.lineNumber,
          endLineNumber: position.lineNumber,
          endColumn: 0,
          startColumn: 0,
        },
        text: `${startElem}\n  `,
      }]
    }
  }
}
