import type { languages } from 'monaco-editor-core'
import * as monaco from 'monaco-editor-core'
import { WorkerAccessor } from '../../monaco-rest-client'

export class TypeFormattingEditAdapter implements languages.OnTypeFormattingEditProvider {
  autoFormatTriggerCharacters = ['\n']
  constructor(private readonly _monaco: typeof monaco, private worker: WorkerAccessor) {}
  provideOnTypeFormattingEdits(model: monaco.editor.IReadOnlyModel,
                               position: monaco.Position,
                               ch: string,
                               options: monaco.languages.FormattingOptions,
                               token: monaco.CancellationToken): Promise<monaco.languages.TextEdit[]> {
    // Press enter in the middle of text
    if (model.getLineContent(position.lineNumber) !== '') {
      return null
    }
    const isLastLine = model.getLineCount() === position.lineNumber
    const nextLine = isLastLine ? '' : model.getLineContent(position.lineNumber + 1).trimLeft()
    // If next line starts with { of [ we do not insert
    if (nextLine.startsWith('{') || nextLine.startsWith('[')) {
      return
    }

    // If next line is not empty, append additional new line in text edit
    const appendNewLine = nextLine !== ''
    const line = model.getLineContent(position.lineNumber - 1)
    return this.worker(model.uri)
      .then((worker) => worker.getTypeFormattingEdits(model.uri.toString(), line, appendNewLine, isLastLine, position))

  }
}
