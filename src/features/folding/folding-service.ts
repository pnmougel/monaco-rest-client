import { JSONScanner } from '../../parser/scanner'
import { FoldingRange } from 'vscode-languageserver-protocol'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { FoldingRangesContext } from './folding-range-context'
import { SyntaxKind } from '../../models/enums/syntax-kind'
import { ScanError } from '../../models/enums/scan-error'
import { FoldingRangeKind, Position } from 'vscode-languageserver-types'

export class FoldingService {
  getKind(kind: SyntaxKind) {
    switch (kind) {
      case SyntaxKind.MustacheTemplateStartConditionTrivia:
      case SyntaxKind.MustacheTemplateEndConditionTrivia:
        return 'mustache-condition'
      case SyntaxKind.OpenBraceToken:
      case SyntaxKind.CloseBraceToken:
        return 'object'
      case SyntaxKind.OpenBracketToken:
      case SyntaxKind.CloseBracketToken:
        return 'array'
    }
  }

  getFoldingRanges(document: TextDocument, context?: FoldingRangesContext): FoldingRange[] {
    const ranges: FoldingRange[] = []
    const nestingLevels: number[] = []
    const stack: FoldingRange[] = []
    let prevStart = -1
    const scanner = new JSONScanner(document.getText(), false)
    let token = scanner.scan()

    let requestRange: FoldingRange | undefined = undefined

    function addRange(range: FoldingRange) {
      ranges.push(range)
      nestingLevels.push(stack.length)
    }

    while (token !== SyntaxKind.EOF) {
      switch (token) {
        case SyntaxKind.MethodToken:
          const startLine = document.positionAt(scanner.getTokenOffset()).line
          requestRange = { startLine, endLine: startLine, kind: 'request' }
          break
        case SyntaxKind.OpenBraceToken:
        case SyntaxKind.MustacheTemplateStartConditionTrivia:
        case SyntaxKind.OpenBracketToken: {
          const startLine = document.positionAt(scanner.getTokenOffset()).line
          const range = { startLine, endLine: startLine, kind: this.getKind(token) }
          stack.push(range)
          break
        }
        case SyntaxKind.CloseBraceToken:
        case SyntaxKind.MustacheTemplateEndConditionTrivia:
        case SyntaxKind.CloseBracketToken: {
          const kind = this.getKind(token)
          if (stack.length > 0 && stack[stack.length - 1].kind === kind) {
            const range = stack.pop()
            const line = document.positionAt(scanner.getTokenOffset()).line
            if (range && line > range.startLine && prevStart !== range.startLine) {
              range.endLine = line
              addRange(range)
              prevStart = range.startLine
            }
          }
          if (stack.length === 0 && !!requestRange) {
            const line = document.positionAt(scanner.getTokenOffset()).line
            if (line > requestRange.startLine && prevStart !== requestRange.startLine) {
              requestRange.endLine = document.positionAt(scanner.getTokenOffset()).line
              addRange(requestRange)
              prevStart = requestRange.startLine
            }
          }
          break
        }

        case SyntaxKind.BlockCommentTrivia: {
          const startLine = document.positionAt(scanner.getTokenOffset()).line
          const endLine = document.positionAt(scanner.getTokenOffset() + scanner.getTokenLength()).line
          if (scanner.getTokenError() === ScanError.UnexpectedEndOfComment && startLine + 1 < document.lineCount) {
            scanner.setPosition(document.offsetAt(Position.create(startLine + 1, 0)))
          } else {
            if (startLine < endLine) {
              addRange({ startLine, endLine, kind: FoldingRangeKind.Comment })
              prevStart = startLine
            }
          }
          break
        }

        case SyntaxKind.LineCommentTrivia: {
          const text = document.getText().substr(scanner.getTokenOffset(), scanner.getTokenLength())
          const m = text.match(/^\/\/\s*#(region\b)|(endregion\b)/)
          if (m) {
            const line = document.positionAt(scanner.getTokenOffset()).line
            if (m[1]) { // start pattern match
              const range = { startLine: line, endLine: line, kind: FoldingRangeKind.Region }
              stack.push(range)
            } else {
              let i = stack.length - 1
              while (i >= 0 && stack[i].kind !== FoldingRangeKind.Region) {
                i--
              }
              if (i >= 0) {
                const range = stack[i]
                stack.length = i
                if (line > range.startLine && prevStart !== range.startLine) {
                  range.endLine = line
                  addRange(range)
                  prevStart = range.startLine
                }
              }
            }
          }
          break
        }

      }
      token = scanner.scan()
    }
    const rangeLimit = context && context.rangeLimit
    if (typeof rangeLimit !== 'number' || ranges.length <= rangeLimit) {
      return ranges
    }
    if (context && context.onRangeLimitExceeded) {
      context.onRangeLimitExceeded(document.uri)
    }

    const counts: number[] = []
    for (let level of nestingLevels) {
      if (level < 30) {
        counts[level] = (counts[level] || 0) + 1
      }
    }
    let entries = 0
    let maxLevel = 0
    for (let i = 0; i < counts.length; i++) {
      const n = counts[i]
      if (n) {
        if (n + entries > rangeLimit) {
          maxLevel = i
          break
        }
        entries += n
      }
    }
    const result = []
    for (let i = 0; i < ranges.length; i++) {
      const level = nestingLevels[i]
      if (typeof level === 'number') {
        if (level < maxLevel || (level === maxLevel && entries++ < rangeLimit)) {
          result.push(ranges[i])
        }
      }
    }
    return result
  }
}

