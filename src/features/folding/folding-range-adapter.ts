import type * as monaco from 'monaco-editor-core'
import { ProtocolToMonacoConverter } from '../../utils/monaco-converter'
import { WorkerAccessor } from '../../monaco-rest-client'
import { FoldingRangesContext } from './folding-range-context'

export class FoldingRangeAdapter implements monaco.languages.FoldingRangeProvider {
  p2m: ProtocolToMonacoConverter

  constructor(private readonly _monaco: typeof monaco, private worker: WorkerAccessor) {
    this.p2m = new ProtocolToMonacoConverter(_monaco)
  }
  provideFoldingRanges(model: monaco.editor.ITextModel, context: FoldingRangesContext, token: monaco.CancellationToken): monaco.languages.ProviderResult<monaco.languages.FoldingRange[]> {
    return this.worker(model.uri)
      .then((worker) => worker.getFoldingRange(model.uri.toString(), context))
      .then((res) => res.map(e => this.p2m.asFoldingRange(e)))
  }
}
