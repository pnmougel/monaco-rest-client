import type { languages } from 'monaco-editor-core'
import { ScanError } from '../../models/enums/scan-error'
import { JSONScanner } from '../../parser/scanner'
import { SyntaxKind } from '../../models/enums/syntax-kind'
import { JSONState } from './json-state'
import { ParentsStack } from './parents-stack'
import { JSONParent } from './json-parent'
import { CharacterCodes } from '../../models/enums/character-codes'

export function createTokenizationSupport(): languages.TokensProvider {
  return {
    getInitialState: () => new JSONState(undefined, undefined, false, false),
    tokenize,
  }
}

export const TOKEN_DELIM_OBJECT = 'delimiter.bracket.json'
export const TOKEN_DELIM_ARRAY = 'delimiter.array.json'
export const TOKEN_DELIM_COLON = 'delimiter.colon.json'
export const TOKEN_DELIM_COMMA = 'delimiter.comma.json'

export const TOKEN_DELIM_SLASH = 'delimiter.slash.url'
export const TOKEN_DELIM_QUESTION = 'delimiter.question.url'
export const TOKEN_DELIM_EQUAL = 'delimiter.equal.url'
export const TOKEN_DELIM_AMPERSAND = 'delimiter.ampersand.url'
export const TOKEN_VALUE_METHOD = 'string.method.url'
export const TOKEN_VALUE_PATH = 'string.path.url'
export const TOKEN_QUERY_KEY = 'string.key.query.url'
export const TOKEN_QUERY_VALUE = 'string.value.query.url'

export const TOKEN_VALUE_BOOLEAN = 'keyword.json'
export const TOKEN_VALUE_NULL = 'keyword.json'
export const TOKEN_VALUE_STRING = 'string.value.json'
export const TOKEN_VALUE_NUMBER = 'number.json'
export const TOKEN_PROPERTY_NAME = 'string.key.json'
export const TOKEN_COMMENT_BLOCK = 'comment.block.json'
export const TOKEN_COMMENT_LINE = 'comment.line.json'
export const TOKEN_MUSTACHE_TEMPLATE_BLOCK = 'template.mustache.block.json'

function tokenize(
  line: string,
  state: JSONState,
  offsetDelta: number = 0,
  stopAtOffset?: number,
): languages.ILineTokens {
  // handle multiline strings and block comments
  let numberOfInsertedCharacters = 0
  let adjustOffset = false

  switch (state.scanError) {
    case ScanError.UnexpectedEndOfString:
      line = '"' + line
      numberOfInsertedCharacters = 1
      break
    case ScanError.UnexpectedEndOfComment:
      line = '/*' + line
      numberOfInsertedCharacters = 2
      break
  }

  const scanner = new JSONScanner(line, false, state.mustacheTemplateDelimiters)
  let lastWasColon = state.lastWasColon
  // let slashFound = state.slashFound;
  let parents = state.parents

  const ret: languages.ILineTokens = {
    tokens: <languages.IToken[]>[],
    endState: state.clone(),
  }

  while (true) {
    let offset = offsetDelta + scanner.getPosition()
    let type = ''

    const kind = scanner.scan()
    if (kind === SyntaxKind.EOF) {
      break
    }
    // Check that the scanner has advanced
    if (offset === offsetDelta + scanner.getPosition()) {
      throw new Error(
        'Scanner did not advance, next 3 characters are: ' + line.substr(scanner.getPosition(), 3),
      )
    }

    // In case we inserted /* or " character, we need to
    // adjust the offset of all tokens (except the first)
    if (adjustOffset) {
      offset -= numberOfInsertedCharacters
    }
    adjustOffset = numberOfInsertedCharacters > 0

    // brackets and type
    switch (kind) {
      case SyntaxKind.OpenBraceToken:
        parents = ParentsStack.push(parents, JSONParent.Object)
        type = TOKEN_DELIM_OBJECT
        // slashFound = false;
        lastWasColon = false
        break
      case SyntaxKind.CloseBraceToken:
        parents = ParentsStack.pop(parents)
        type = TOKEN_DELIM_OBJECT
        // slashFound = false;
        lastWasColon = false
        break
      case SyntaxKind.OpenBracketToken:
        parents = ParentsStack.push(parents, JSONParent.Array)
        type = TOKEN_DELIM_ARRAY
        // slashFound = false;
        lastWasColon = false
        break
      case SyntaxKind.CloseBracketToken:
        parents = ParentsStack.pop(parents)
        type = TOKEN_DELIM_ARRAY
        // slashFound = false;
        lastWasColon = false
        break
      case SyntaxKind.SlashToken:
        type = TOKEN_DELIM_SLASH
        lastWasColon = false
        // slashFound = true;
        break
      case SyntaxKind.ColonToken:
        type = TOKEN_DELIM_COLON
        lastWasColon = true
        break
      case SyntaxKind.CommaToken:
        type = TOKEN_DELIM_COMMA
        lastWasColon = false
        break
      case SyntaxKind.TrueKeyword:
      case SyntaxKind.FalseKeyword:
        type = TOKEN_VALUE_BOOLEAN
        lastWasColon = false
        break
      case SyntaxKind.NullKeyword:
        type = TOKEN_VALUE_NULL
        lastWasColon = false
        break
      case SyntaxKind.StringLiteral:
        type = lastWasColon || parents?.type === JSONParent.Array ? TOKEN_VALUE_STRING : TOKEN_PROPERTY_NAME
        lastWasColon = true
        break
      case SyntaxKind.MultilineStringLiteral:
        type = TOKEN_VALUE_STRING
        lastWasColon = true
        break
      case SyntaxKind.NumericLiteral:
        type = TOKEN_VALUE_NUMBER
        lastWasColon = false
        break
      case SyntaxKind.MethodToken:
        type = TOKEN_VALUE_METHOD
        lastWasColon = false
        break
      case SyntaxKind.MustacheTemplateComment:
        type = TOKEN_COMMENT_LINE
        lastWasColon = false
        break
      case SyntaxKind.MustacheTemplateParam:
      case SyntaxKind.MustacheTemplateStartConditionTrivia:
      case SyntaxKind.MustacheTemplateEndConditionTrivia:
        type = TOKEN_MUSTACHE_TEMPLATE_BLOCK
        lastWasColon = false
        break
      case SyntaxKind.MustacheTemplateSetDelimiter:
        state.mustacheTemplateDelimiters = [scanner.mustacheStart0, scanner.mustacheStart1, scanner.mustacheEnd0, scanner.mustacheEnd1]
        type = TOKEN_MUSTACHE_TEMPLATE_BLOCK
        lastWasColon = false
        break
    }

    // comments, iff enabled
    switch (kind) {
      case SyntaxKind.LineCommentTrivia:
        type = TOKEN_COMMENT_LINE
        break
      case SyntaxKind.BlockCommentTrivia:
        type = TOKEN_COMMENT_BLOCK
        break
    }

    ret.endState = new JSONState(
      state.getStateData(),
      scanner.getTokenError(),
      lastWasColon,
      false,
      parents,
      state.mustacheTemplateDelimiters,
    )
    if (kind === SyntaxKind.UrlToken) {
      const url = scanner.getTokenValue()
      let start = 0
      let inPath = true
      let inQueryParamKey = true
      for (let i = 0; i !== url.length; i++) {
        const c = url.charCodeAt(i)
        if (c === CharacterCodes.slash && inPath) {
          ret.tokens.push({
            startIndex: offset + start + 1,
            scopes: TOKEN_VALUE_PATH,
          })
          ret.tokens.push({
            startIndex: offset + i,
            scopes: TOKEN_DELIM_SLASH,
          })
          start = i
        } else if (c === CharacterCodes.question && inPath) {
          inPath = false
          ret.tokens.push({
            startIndex: offset + start + 1,
            scopes: TOKEN_VALUE_PATH,
          })
          ret.tokens.push({
            startIndex: offset + i,
            scopes: TOKEN_DELIM_QUESTION,
          })
          start = i
        } else if (c === CharacterCodes.ampersand && !inPath) {
          ret.tokens.push({
            startIndex: offset + start + 1,
            scopes: inQueryParamKey ? TOKEN_QUERY_KEY : TOKEN_QUERY_VALUE,
          })
          ret.tokens.push({
            startIndex: offset + i,
            scopes: TOKEN_DELIM_AMPERSAND,
          })
          inQueryParamKey = true
          start = i
        } else if (c === CharacterCodes.equals && !inPath) {
          inQueryParamKey = false
          ret.tokens.push({
            startIndex: offset + start + 1,
            scopes: TOKEN_QUERY_KEY,
          })
          ret.tokens.push({
            startIndex: offset + i,
            scopes: TOKEN_DELIM_AMPERSAND,
          })
          start = i
        }
      }
      ret.tokens.push({
        startIndex: offset + start + 1,
        scopes: inPath ? TOKEN_VALUE_PATH : (inQueryParamKey ? TOKEN_QUERY_KEY : TOKEN_QUERY_VALUE),
      })
    } else {
      ret.tokens.push({
        startIndex: offset,
        scopes: type,
      })
    }

  }

  return ret
}
