import { JSONParent } from './json-parent'

export class ParentsStack {
  constructor(
    public readonly parent?: ParentsStack,
    public readonly type?: JSONParent,
  ) {}

  public static pop(parents?: ParentsStack) {
    if (parents) {
      return parents.parent
    }
    return undefined
  }

  public static push(
    parents?: ParentsStack,
    type?: JSONParent,
  ): ParentsStack {
    return new ParentsStack(parents, type)
  }

  public static equals(
    a?: ParentsStack,
    b?: ParentsStack,
  ): boolean {
    if (!a && !b) {
      return true
    }
    if (!a || !b) {
      return false
    }
    while (a && b) {
      if (a === b) {
        return true
      }
      if (a.type !== b.type) {
        return false
      }
      a = a.parent
      b = b.parent
    }
    return true
  }
}
