import type { languages } from 'monaco-editor-core'
import { ScanError } from '../../models/enums/scan-error'
import { ParentsStack } from './parents-stack'
import { CharacterCodes } from '../../models/enums/character-codes'

export class JSONState implements languages.IState {
  public scanError?: ScanError
  public lastWasColon?: boolean
  public slashFound?: boolean
  public parents?: ParentsStack
  public mustacheTemplateDelimiters = [CharacterCodes.openBrace, CharacterCodes.openBrace, CharacterCodes.closeBrace, CharacterCodes.closeBrace]
  private _state?: languages.IState
  constructor(
    state?: languages.IState,
    scanError?: ScanError,
    lastWasColon?: boolean,
    slashFound?: boolean,
    parents?: ParentsStack,
    mustacheTemplateDelimiters?: number[],
  ) {
    this._state = state
    this.scanError = scanError
    this.lastWasColon = lastWasColon
    this.slashFound = slashFound
    this.parents = parents
    this.mustacheTemplateDelimiters = mustacheTemplateDelimiters
  }

  public clone(): JSONState {
    return new JSONState(
      this._state,
      this.scanError,
      this.lastWasColon,
      this.slashFound,
      this.parents,
      this.mustacheTemplateDelimiters,
    )
  }

  public equals(other: languages.IState): boolean {
    if (other === this) {
      return true
    }
    if (!other || !(other instanceof JSONState)) {
      return false
    }
    return (
      this.scanError === other.scanError &&
      this.lastWasColon === other.lastWasColon &&
      this.slashFound === other.slashFound &&
      ParentsStack.equals(this.parents, other.parents)
    )
  }

  public getStateData(): languages.IState | undefined {
    return this._state
  }

  public setStateData(state: languages.IState): void {
    this._state = state
  }
}
