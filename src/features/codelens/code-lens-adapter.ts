import type { CancellationToken, editor, languages } from 'monaco-editor-core'
import { WorkerAccessor } from '../../monaco-rest-client'

export class CodeLensAdapter implements languages.CodeLensProvider {
  constructor(private worker: WorkerAccessor) {}

  provideCodeLenses(model: editor.ITextModel, token: CancellationToken): languages.ProviderResult<languages.CodeLensList> {
    return this.worker(model.uri)
      .then((worker) => worker.getCodeLens(model.uri.toString()))
      .then(lenses => ({ lenses, dispose: () => {} }))
  }
}
