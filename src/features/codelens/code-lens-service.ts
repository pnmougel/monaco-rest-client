import type { languages } from 'monaco-editor-core'
import { JSONScanner } from '../../parser/scanner'
import { SyntaxKind } from '../../models/enums/syntax-kind'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { parse } from '../../parser/parse'
import { runQueryCommand } from '../../monaco-rest-client'
import { buildRequest } from '../../utils/build-request'
import { RestClientOptions } from '../../configuration/rest-client-options'

export class CodeLensService {

  getCodeLens(document: TextDocument, modelId: string, languageOptions: RestClientOptions): languages.CodeLens[] {
    const text = document.getText()
    const scanner = new JSONScanner(text, true)
    const lenses: languages.CodeLens[] = []
    const lensPositions = []

    function addLens(startLine: number, endLine: number) {
      const start = { line: startLine, character: 0 }
      const end = { line: endLine, character: 0 }
      const textDoc = TextDocument.create(document.uri, document.languageId, document.version, document.getText({
        start,
        end,
      }))
      const requestsDoc = parse(textDoc, languageOptions)

      if (!requestsDoc || requestsDoc.requests.length !== 1 || !requestsDoc.requests[0].methodValue || !requestsDoc.requests[0].urlValue) {
        return
      }
      const request = requestsDoc.requests[0]

      lenses.push({
        range: {
          startLineNumber: startLine + 1,
          startColumn: 1,
          endLineNumber: startLine + 2,
          endColumn: 1,
        },
        id: runQueryCommand,
        command: {
          id: runQueryCommand,
          title: 'Run request',
          arguments: [modelId, buildRequest(request, textDoc)],
        },
      })
    }

    while (scanner.getToken() !== SyntaxKind.EOF) {
      if (scanner.getToken() === SyntaxKind.MethodToken) {
        lensPositions.push(scanner.getTokenStartLine())
      }
      scanner.scanNextNonTrivia()
    }

    if (lensPositions.length > 0) {
      for (let i = 0; i < lensPositions.length - 1; i++) {
        const startLine = lensPositions[i]
        const endLine = lensPositions[i + 1]
        if (lenses.length === 0 || !languageOptions.singleRequest) {
          addLens(startLine, endLine)
        }
      }
      if (lenses.length === 0 || !languageOptions.singleRequest) {
        addLens(lensPositions[lensPositions.length - 1], scanner.getTokenStartLine() + 1)
      }
    }

    return lenses
  }

}
