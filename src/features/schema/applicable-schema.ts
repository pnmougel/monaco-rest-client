import { JSONSchema } from './json-schema'
import { ASTNode } from '../../models/ast/ast-node'

export interface IApplicableSchema {
  node: ASTNode;
  inverted?: boolean;
  schema: JSONSchema;
}
