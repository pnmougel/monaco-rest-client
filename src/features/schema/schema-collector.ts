import { ASTNode } from '../../models/ast/ast-node'
import { IApplicableSchema } from './applicable-schema'

export interface ISchemaCollector {
  schemas: IApplicableSchema[];

  add(schema: IApplicableSchema): void;

  merge(other: ISchemaCollector): void;

  include(node: ASTNode): boolean;

  newSub(): ISchemaCollector;
}

export class SchemaCollector implements ISchemaCollector {
  schemas: IApplicableSchema[] = []
  constructor(private focusOffset = -1, private exclude?: ASTNode) {
  }
  add(schema: IApplicableSchema) {
    this.schemas.push(schema)
  }
  merge(other: ISchemaCollector) {
    Array.prototype.push.apply(this.schemas, other.schemas)
  }
  include(node: ASTNode) {
    return (this.focusOffset === -1 || node.isContained(this.focusOffset)) && (node !== this.exclude)
  }
  newSub(): ISchemaCollector {
    return new SchemaCollector(-1, this.exclude)
  }
}

export class NoOpSchemaCollector implements ISchemaCollector {
  static instance = new NoOpSchemaCollector()
  private constructor() { }
  get schemas() { return [] }
  add(schema: IApplicableSchema) { }
  merge(other: ISchemaCollector) { }
  include(node: ASTNode) { return true }
  newSub(): ISchemaCollector { return this }
}
