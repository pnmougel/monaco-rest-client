import { OpenApiService } from '../../services/open-api-service'
import { CompletionItem, CompletionList } from 'vscode-languageserver-protocol'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { Position } from 'vscode-languageserver-types'
import { RequestsDocument } from '../../models/requests-document'
import { Completer } from './completers/completer'
import { EmptyCompleter } from './completers/empty-completer'
import { MethodCompleter } from './completers/method-completer'
import { PathCompleter } from './completers/path-completer'
import { QueryKeyCompleter } from './completers/query-key-completer'
import { QueryValueCompleter } from './completers/query-value-completer'
import { BodyCompleter } from './completers/body-completer'
import { CompletionContext } from './completion-context'
import { RestClientOptions } from '../../configuration/rest-client-options'
import { fnParse } from '../../utils/fn-stringify'
import { SnippetCompleter } from './completers/snippet-completer'

export class CompletionService {
  constructor(private openApiService: OpenApiService) {}

  public doResolve(item: CompletionItem): Thenable<CompletionItem> {
    return Promise.resolve(item)
  }

  public async doComplete(document: TextDocument, position: Position, requestsDocument: RequestsDocument, options: RestClientOptions): Promise<CompletionList> {
    const customBodyCompletion = !!options.customBodyCompletion ? fnParse(options.customBodyCompletion) : async () => []
    const customPathCompletion = !!options.customPathCompletion ? fnParse(options.customPathCompletion) : async () => []
    let completers: Completer[]

    if (options.schemaDef === undefined) {
      completers = [
        new EmptyCompleter(),
        new MethodCompleter(),
        new PathCompleter(customPathCompletion, options.customOptions ?? {}),
        new QueryKeyCompleter(),
        new QueryValueCompleter(),
        new BodyCompleter(customBodyCompletion, options.customOptions ?? {}),
      ]
    } else {
      completers = [
        new EmptyCompleter(),
        new BodyCompleter(customBodyCompletion, options.customOptions ?? {}),
      ]
    }

    const completerContext = new CompletionContext(document, position, requestsDocument, this.openApiService, options)
    let completerMatch = false
    const results: Promise<any>[] = []

    completers.forEach(completer => {
      if (!completerMatch) {
        completerMatch = completer.isApplicable(completerContext)
        if (completerMatch) {
          results.push(completer.doComplete(completerContext))
        }
      }
    })

    const snippets = options.snippets ?? []
    if(snippets.length > 0) {
      const snippetsCompleter = new SnippetCompleter(snippets)
      if (snippetsCompleter.isApplicable(completerContext)) {
        results.push(snippetsCompleter.doComplete(completerContext))
      }
    }

    await Promise.all(results)
    return completerContext.collector.result
  }
}
