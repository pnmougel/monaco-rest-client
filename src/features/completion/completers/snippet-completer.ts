import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { CompletionItemKind } from 'vscode-languageserver-types'
import { EmptyCompleter } from './empty-completer'

export class SnippetCompleter implements Completer {
  constructor(private snippets: {name: string, content: string, description?: string}[]) {}

  isApplicable(ctx: CompletionContext): boolean {
    return !EmptyCompleter.isInComment(ctx.document, ctx.node?.offset ?? 0, ctx.offset)
  }

  async doComplete(ctx: CompletionContext) {
    ctx.addEntries(CompletionItemKind.Snippet, false, this.snippets.map(snippet => ({
      label: snippet.name,
      documentation: snippet.description,
      insertText: snippet.content,
      filterText: snippet.name,
      type: 'Snippet',
      commitCharacters: ['\t'],
    })))
  }
}
