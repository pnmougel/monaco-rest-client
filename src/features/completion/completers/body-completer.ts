import { ASTNode } from '../../../models/ast/ast-node'
import { CompletionsCollector } from '../completions-collector'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { JSONDocument } from '../../../models/json-document'
import { CompletionItemKind, InsertTextFormat, MarkupContent, MarkupKind } from 'vscode-languageserver-types'
import { CompletionItem } from 'vscode-languageserver-protocol'
import { PropertyASTNode } from '../../../models/ast/json/property-node'
import { ObjectASTNode } from '../../../models/ast/json/object-node'
import { endsWith, extendedRegExp } from '../../../utils/strings'
import { ArrayASTNode } from '../../../models/ast/json/array-node'
import { JSONSchema, JSONSchemaRef } from '../../schema/json-schema'
import { isDefined } from '../../../utils/objects'
import { stringifyObject } from '../../../utils/json'
import { JSONScanner } from '../../../parser/scanner'
import { SyntaxKind } from '../../../models/enums/syntax-kind'
import * as nls from 'vscode-nls'
import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { OpenApiService } from '../../../services/open-api-service'
import { CustomBodyCompletionContext, CustomBodyCompletionResolver } from '../../../monaco-rest-client'
import { getCompletionItemKind } from '../utils'

const localize = nls.loadMessageBundle()

export class BodyCompleter implements Completer {
  constructor(private customBodyCompletion: CustomBodyCompletionResolver, private customOptions: any) {}

  isApplicable(ctx: CompletionContext): boolean {
    return ctx.isInBody && !!ctx.schemaDefinition
  }

  async doComplete(ctx: CompletionContext) {
    let node = ctx.node
    const document = ctx.document
    const collector = ctx.collector

    const offset = ctx.offset
    const text = ctx.text
    const jsonDoc = ctx.jsonDoc

    if (node && (offset === node.offset + node.length) && offset > 0) {
      const ch = text[offset - 1]
      if (node.type === 'object' && ch === '}' || node.type === 'array' && ch === ']') {
        // after ] or }
        node = node.parent
      }
    }

    const schema = ctx.schemaDefinition
    let addValue = true
    let currentKey = ''

    let currentProperty: PropertyASTNode | undefined = undefined
    if (node) {
      if (node.type === 'string') {
        const parent = node.parent
        if (parent && parent.type === 'property' && (parent as PropertyASTNode).keyNode === node) {
          addValue = !(parent as PropertyASTNode).valueNode
          currentProperty = (parent as PropertyASTNode)
          // currentKey = text.substr(node.offset + 1, node.length - 2);
          if (parent) {
            node = parent.parent
          }
        }
      }
    }

    const customCompletionContext = this.buildCustomBodyCompletionContext(ctx)

    // proposals for properties
    if (node && node.type === 'object') {
      // don't suggest keys when the cursor is just before the opening curly brace
      if (node.offset === offset) {
        return
      }
      // don't suggest properties that are already present
      const properties = (node as ObjectASTNode).properties
      properties.forEach(p => {
        if (!currentProperty || currentProperty !== p) {
          collector.proposed[p.keyNode.value] = CompletionItem.create('__')
        }
      })
      let separatorAfter = ''
      if (addValue) {
        separatorAfter = BodyCompleter.evaluateSeparatorAfter(document, document.offsetAt(collector.overwriteRange.end))
      }

      await this.getPropertyCompletions(ctx.openApiService, schema, jsonDoc, node, addValue, separatorAfter, collector, customCompletionContext)
    }

    // proposals for values
    const types: { [type: string]: boolean } = {}
    await this.getValueCompletions(ctx.openApiService, schema, jsonDoc, node, offset, document, collector, types)

    if (collector.getNumberOfProposals() === 0) {
      let offsetForSeparator = offset
      if (node && (node.type === 'string' || node.type === 'number' || node.type === 'boolean' || node.type === 'null')) {
        offsetForSeparator = node.offset + node.length
      }
      const separatorAfter = BodyCompleter.evaluateSeparatorAfter(document, offsetForSeparator)
      BodyCompleter.addFillerValueCompletions(types, separatorAfter, collector)
    }
    return collector.result
  }

  private async getPropertyCompletions(openApiService: OpenApiService, schema: JSONSchema, doc: JSONDocument, node: ASTNode,
                                       addValue: boolean, separatorAfter: string, collector: CompletionsCollector,
                                       customCompletionContext: CustomBodyCompletionContext) {
    const matchingSchemas = doc.getMatchingSchemas(openApiService, schema, node.offset)
    for await(const s of matchingSchemas) {
      if (s.node === node && !s.inverted) {
        const customCompletions = (await this.customBodyCompletion('key', s.schema, customCompletionContext, this.customOptions)) ?? []
        customCompletions.forEach(completion => {
          collector.add({
            kind: getCompletionItemKind(completion.kind),
            label: completion.label,
            detail: completion.type,
            documentation: completion.description ?? '',
            insertText: `"${completion.label}": {$1}`,
            insertTextFormat: InsertTextFormat.Snippet,
            command: {
              title: 'Suggest',
              command: 'editor.action.triggerSuggest',
            },
          })
        })
        const schemaProperties = s.schema.properties
        if (schemaProperties) {
          Object.keys(schemaProperties).forEach((key: string) => {
            let propertySchema: any = schemaProperties[key]
            if (typeof propertySchema === 'object' && !propertySchema.deprecationMessage && !propertySchema.doNotSuggest) {
              if ('$ref' in propertySchema) {
                propertySchema = { ...openApiService.resolveRef(propertySchema.$ref), ...propertySchema }
              }

              const proposal: CompletionItem = {
                kind: CompletionItemKind.Property,
                label: key,
                insertText: this.getInsertTextForProperty(key, propertySchema, addValue, separatorAfter),
                insertTextFormat: InsertTextFormat.Snippet,
                filterText: BodyCompleter.getFilterTextForValue(key),
                documentation: BodyCompleter.fromMarkup(propertySchema.description) || '',
                detail: Array.isArray(propertySchema.type) ? propertySchema.type.join(' | ') : propertySchema.type,
              }
              if (typeof propertySchema.type === 'string') {
                proposal.detail = propertySchema.type
              }

              if (propertySchema.suggestSortText !== undefined) {
                proposal.sortText = propertySchema.suggestSortText
              }
              if (proposal.insertText && endsWith(proposal.insertText, `$1${separatorAfter}`)) {
                proposal.command = {
                  title: 'Suggest',
                  command: 'editor.action.triggerSuggest',
                }
              }
              collector.add(proposal)
            }
          })
        }
        let schemaPropertyNames: any = s.schema.propertyNames
        if (typeof schemaPropertyNames === 'object' && !schemaPropertyNames.deprecationMessage && !schemaPropertyNames.doNotSuggest) {
          if ('$ref' in schemaPropertyNames) {
            schemaPropertyNames = { ...openApiService.resolveRef(schemaPropertyNames.$ref), ...schemaPropertyNames }
          }
          const propertyNameCompletionItem = (name: string, enumDescription: string | MarkupContent | undefined = undefined) => {
            const proposal: CompletionItem = {
              kind: CompletionItemKind.Property,
              label: name,
              insertText: this.getInsertTextForProperty(name, undefined, addValue, separatorAfter),
              insertTextFormat: InsertTextFormat.Snippet,
              filterText: BodyCompleter.getFilterTextForValue(name),
              documentation: enumDescription || BodyCompleter.fromMarkup(schemaPropertyNames.description) || '',
            }
            if (schemaPropertyNames.suggestSortText !== undefined) {
              proposal.sortText = schemaPropertyNames.suggestSortText
            }
            if (proposal.insertText && endsWith(proposal.insertText, `$1${separatorAfter}`)) {
              proposal.command = {
                title: 'Suggest',
                command: 'editor.action.triggerSuggest',
              }
            }
            collector.add(proposal)
          }
          if (schemaPropertyNames.enum) {
            for (let i = 0; i < schemaPropertyNames.enum.length; i++) {
              let enumDescription = undefined
              if (schemaPropertyNames.markdownEnumDescriptions && i < schemaPropertyNames.markdownEnumDescriptions.length) {
                enumDescription = BodyCompleter.fromMarkup(schemaPropertyNames.markdownEnumDescriptions[i])
              } else if (schemaPropertyNames.enumDescriptions && i < schemaPropertyNames.enumDescriptions.length) {
                enumDescription = schemaPropertyNames.enumDescriptions[i]
              }
              propertyNameCompletionItem(schemaPropertyNames.enum[i], enumDescription)
            }
          }
          if (schemaPropertyNames.const) {
            propertyNameCompletionItem(schemaPropertyNames.const)
          }
        }
      }
    }
  }

  private async getValueCompletions(openApiService: OpenApiService, schema: JSONSchema, doc: JSONDocument, node: ASTNode | undefined, offset: number, document: TextDocument, collector: CompletionsCollector, types: { [type: string]: boolean }) {
    let offsetForSeparator = offset
    let parentKey: string | undefined = undefined
    let valueNode: ASTNode | undefined = undefined

    if (node && (node.type === 'string' || node.type === 'number' || node.type === 'boolean' || node.type === 'null')) {
      offsetForSeparator = node.offset + node.length
      valueNode = node
      node = node.parent
    }

    if (!node) {
      this.addSchemaValueCompletions(schema, '', collector, types)
      return
    }

    if ((node.type === 'property') && offset > ((node as PropertyASTNode).colonOffset || 0)) {
      const valueNode = (node as PropertyASTNode).valueNode
      if (valueNode && offset > (valueNode.offset + valueNode.length)) {
        return // we are past the value node
      }
      parentKey = (node as PropertyASTNode).keyNode.value
      node = node.parent
    }

    if (node && (parentKey !== undefined || node.type === 'array')) {
      const separatorAfter = BodyCompleter.evaluateSeparatorAfter(document, offsetForSeparator)

      const matchingSchemas = doc.getMatchingSchemas(openApiService, schema, node.offset, valueNode)
      for (const s of matchingSchemas) {
        if (s.node === node && !s.inverted && s.schema) {
          if (node.type === 'array' && s.schema.items) {
            if (Array.isArray(s.schema.items)) {
              const index = BodyCompleter.findItemAtOffset(node as ArrayASTNode, document, offset)
              if (index < s.schema.items.length) {
                this.addSchemaValueCompletions(s.schema.items[index], separatorAfter, collector, types)
              }
            } else {
              this.addSchemaValueCompletions(s.schema.items, separatorAfter, collector, types)
            }
          }
          if (parentKey !== undefined) {
            let propertyMatched = false
            if (s.schema.properties) {
              let propertySchema: any = s.schema.properties[parentKey]
              if (propertySchema) {
                propertyMatched = true
                if ('$ref' in propertySchema) {
                  propertySchema = { ...openApiService.resolveRef(propertySchema.$ref), ...propertySchema }
                }
                this.addSchemaValueCompletions(propertySchema, separatorAfter, collector, types)
              }
            }
            if (s.schema.patternProperties && !propertyMatched) {
              for (const pattern of Object.keys(s.schema.patternProperties)) {
                const regex = extendedRegExp(pattern)
                if (regex.test(parentKey)) {
                  propertyMatched = true
                  let propertySchema: any = s.schema.patternProperties[pattern]
                  if ('$ref' in propertySchema) {
                    propertySchema = { ...openApiService.resolveRef(propertySchema.$ref), ...propertySchema }
                  }
                  this.addSchemaValueCompletions(propertySchema, separatorAfter, collector, types)
                }
              }
            }
            if (s.schema.additionalProperties && !propertyMatched) {
              let propertySchema: any = s.schema.additionalProperties
              if ('$ref' in propertySchema) {
                propertySchema = { ...openApiService.resolveRef(propertySchema.$ref), ...propertySchema }
              }
              this.addSchemaValueCompletions(propertySchema, separatorAfter, collector, types)
            }
          }
        }
      }
      // if (parentKey === '$schema' && !node.parent) {
      // 	this.addDollarSchemaCompletions(separatorAfter, collector);
      // }
      if (types['boolean']) {
        BodyCompleter.addBooleanValueCompletion(true, separatorAfter, collector)
        BodyCompleter.addBooleanValueCompletion(false, separatorAfter, collector)
      }
      if (types['null']) {
        BodyCompleter.addNullValueCompletion(separatorAfter, collector)
      }
    }

  }

  private addSchemaValueCompletions(schema: JSONSchemaRef, separatorAfter: string, collector: CompletionsCollector, types: { [type: string]: boolean }): void {
    if (typeof schema === 'object') {
      BodyCompleter.addEnumValueCompletions(schema, separatorAfter, collector)
      this.addDefaultValueCompletions(schema, separatorAfter, collector)
      this.collectTypes(schema, types)
      if (Array.isArray(schema.allOf)) {
        schema.allOf.forEach(s => this.addSchemaValueCompletions(s, separatorAfter, collector, types))
      }
      if (Array.isArray(schema.anyOf)) {
        schema.anyOf.forEach(s => this.addSchemaValueCompletions(s, separatorAfter, collector, types))
      }
      if (Array.isArray(schema.oneOf)) {
        schema.oneOf.forEach(s => this.addSchemaValueCompletions(s, separatorAfter, collector, types))
      }
    }
  }

  private addDefaultValueCompletions(schema: JSONSchema, separatorAfter: string, collector: CompletionsCollector, arrayDepth = 0): void {
    let hasProposals = false
    if (isDefined(schema.default)) {
      let type = schema.type
      let value = schema.default
      for (let i = arrayDepth; i > 0; i--) {
        value = [value]
        type = 'array'
      }
      collector.add({
        kind: BodyCompleter.getSuggestionKind(type),
        label: BodyCompleter.getLabelForValue(value),
        insertText: BodyCompleter.getInsertTextForValue(value, separatorAfter),
        insertTextFormat: InsertTextFormat.Snippet,
        detail: localize('json.suggest.default', 'Default value'),
      })
      hasProposals = true
    }
    if (Array.isArray(schema.examples)) {
      schema.examples.forEach(example => {
        let type = schema.type
        let value = example
        for (let i = arrayDepth; i > 0; i--) {
          value = [value]
          type = 'array'
        }
        collector.add({
          kind: BodyCompleter.getSuggestionKind(type),
          label: BodyCompleter.getLabelForValue(value),
          insertText: BodyCompleter.getInsertTextForValue(value, separatorAfter),
          insertTextFormat: InsertTextFormat.Snippet,
        })
        hasProposals = true
      })
    }
    if (Array.isArray(schema.defaultSnippets)) {
      schema.defaultSnippets.forEach(s => {
        let type = schema.type
        let value = s.body
        let label = s.label
        let insertText: string
        let filterText: string
        if (isDefined(value)) {
          // @ts-ignore
          let type = schema.type
          for (let i = arrayDepth; i > 0; i--) {
            value = [value]
            type = 'array'
          }
          insertText = this.getInsertTextForSnippetValue(value, separatorAfter)
          filterText = BodyCompleter.getFilterTextForSnippetValue(value)
          label = label || BodyCompleter.getLabelForSnippetValue(value)
        } else if (typeof s.bodyText === 'string') {
          let prefix = '', suffix = '', indent = ''
          for (let i = arrayDepth; i > 0; i--) {
            prefix = prefix + indent + '[\n'
            suffix = suffix + '\n' + indent + ']'
            indent += '\t'
            type = 'array'
          }
          insertText = prefix + indent + s.bodyText.split('\n').join('\n' + indent) + suffix + separatorAfter
          label = label || insertText
          filterText = insertText.replace(/[\n]/g, '')   // remove new lines
        } else {
          return
        }
        collector.add({
          kind: BodyCompleter.getSuggestionKind(type),
          label,
          documentation: BodyCompleter.fromMarkup(s.description),
          insertText,
          insertTextFormat: InsertTextFormat.Snippet,
          filterText,
        })
        hasProposals = true
      })
    }
    if (!hasProposals && typeof schema.items === 'object' && !Array.isArray(schema.items) && arrayDepth < 5 /* beware of recursion */) {
      this.addDefaultValueCompletions(schema.items, separatorAfter, collector, arrayDepth + 1)
    }
  }


  private static addEnumValueCompletions(schema: JSONSchema, separatorAfter: string, collector: CompletionsCollector): void {
    if (isDefined(schema.const)) {
      collector.add({
        kind: BodyCompleter.getSuggestionKind(schema.type),
        label: BodyCompleter.getLabelForValue(schema.const),
        insertText: BodyCompleter.getInsertTextForValue(schema.const, separatorAfter),
        insertTextFormat: InsertTextFormat.Snippet,
        documentation: BodyCompleter.fromMarkup(schema.description),
      })
    }

    if (Array.isArray(schema.enum)) {
      for (let i = 0, length = schema.enum.length; i < length; i++) {
        const enm = schema.enum[i]
        let documentation: string | MarkupContent | undefined = BodyCompleter.fromMarkup(schema.description)
        if (schema.markdownEnumDescriptions && i < schema.markdownEnumDescriptions.length) {
          documentation = BodyCompleter.fromMarkup(schema.markdownEnumDescriptions[i])
        } else if (schema.enumDescriptions && i < schema.enumDescriptions.length) {
          documentation = schema.enumDescriptions[i]
        }
        collector.add({
          kind: BodyCompleter.getSuggestionKind(schema.type),
          label: BodyCompleter.getLabelForValue(enm),
          insertText: BodyCompleter.getInsertTextForValue(enm, separatorAfter),
          insertTextFormat: InsertTextFormat.Snippet,
          documentation,
        })
      }
    }
  }

  private collectTypes(schema: JSONSchema, types: { [type: string]: boolean }) {
    if (Array.isArray(schema.enum) || isDefined(schema.const)) {
      return
    }
    const type = schema.type
    if (Array.isArray(type)) {
      type.forEach(t => types[t] = true)
    } else if (type) {
      types[type] = true
    }
  }

  private static addFillerValueCompletions(types: { [type: string]: boolean }, separatorAfter: string, collector: CompletionsCollector): void {
    if (types['object']) {
      collector.add({
        kind: BodyCompleter.getSuggestionKind('object'),
        label: '{}',
        insertText: BodyCompleter.getInsertTextForGuessedValue({}, separatorAfter),
        insertTextFormat: InsertTextFormat.Snippet,
        detail: localize('defaults.object', 'New object'),
        documentation: '',
      })
    }
    if (types['array']) {
      collector.add({
        kind: BodyCompleter.getSuggestionKind('array'),
        label: '[]',
        insertText: BodyCompleter.getInsertTextForGuessedValue([], separatorAfter),
        insertTextFormat: InsertTextFormat.Snippet,
        detail: localize('defaults.array', 'New array'),
        documentation: '',
      })
    }
  }

  private static addBooleanValueCompletion(value: boolean, separatorAfter: string, collector: CompletionsCollector): void {
    collector.add({
      kind: BodyCompleter.getSuggestionKind('boolean'),
      label: value ? 'true' : 'false',
      insertText: BodyCompleter.getInsertTextForValue(value, separatorAfter),
      insertTextFormat: InsertTextFormat.Snippet,
      documentation: '',
    })
  }

  private static addNullValueCompletion(separatorAfter: string, collector: CompletionsCollector): void {
    collector.add({
      kind: BodyCompleter.getSuggestionKind('null'),
      label: 'null',
      insertText: 'null' + separatorAfter,
      insertTextFormat: InsertTextFormat.Snippet,
      documentation: '',
    })
  }

  // private addDollarSchemaCompletions(separatorAfter: string, collector: CompletionsCollector): void {
  // 	const schemaIds = this.schemaService.getRegisteredSchemaIds(schema => schema === 'http' || schema === 'https');
  // 	schemaIds.forEach(schemaId => collector.add({
  // 		kind: CompletionItemKind.Module,
  // 		label: this.getLabelForValue(schemaId),
  // 		filterText: this.getFilterTextForValue(schemaId),
  // 		insertText: this.getInsertTextForValue(schemaId, separatorAfter),
  // 		insertTextFormat: InsertTextFormat.Snippet, documentation: ''
  // 	}));
  // }

  private static getLabelForValue(value: any): string {
    return JSON.stringify(value)
  }

  private static getFilterTextForValue(value: any): string {
    return JSON.stringify(value)
  }

  private static getFilterTextForSnippetValue(value: any): string {
    return JSON.stringify(value).replace(/\$\{\d+:([^}]+)\}|\$\d+/g, '$1')
  }

  private static getLabelForSnippetValue(value: any): string {
    const label = JSON.stringify(value)
    return label.replace(/\$\{\d+:([^}]+)\}|\$\d+/g, '$1')
  }

  private static getInsertTextForPlainText(text: string): string {
    return text.replace(/[\\\$\}]/g, '\\$&')   // escape $, \ and }
  }

  private static getInsertTextForValue(value: any, separatorAfter: string): string {
    const text = JSON.stringify(value, null, '\t')
    if (text === '{}') {
      return '{$1}' + separatorAfter
    } else if (text === '[]') {
      return '[$1]' + separatorAfter
    } else if (text === '[\n\t{}\n]') {
      return '[{$1}]' + separatorAfter
    }
    return BodyCompleter.getInsertTextForPlainText(text + separatorAfter)
  }

  private getInsertTextForSnippetValue(value: any, separatorAfter: string): string {
    const replacer = (value: any) => {
      if (typeof value === 'string') {
        if (value[0] === '^') {
          return value.substr(1)
        }
      }
      return JSON.stringify(value)
    }
    return stringifyObject(value, '', replacer) + separatorAfter
  }

  private static getInsertTextForGuessedValue(value: any, separatorAfter: string): string {
    switch (typeof value) {
      case 'object':
        if (value === null) {
          return '${1:null}' + separatorAfter
        }
        return BodyCompleter.getInsertTextForValue(value, separatorAfter)
      case 'string':
        if (value.indexOf('${1') !== 1) {
          return value + separatorAfter
        }
        let snippetValue = JSON.stringify(value)
        snippetValue = snippetValue.substr(1, snippetValue.length - 2) // remove quotes
        snippetValue = BodyCompleter.getInsertTextForPlainText(snippetValue) // escape \ and }
        return '"${1:' + snippetValue + '}"' + separatorAfter
      case 'number':
      case 'boolean':
        return '${1:' + JSON.stringify(value) + '}' + separatorAfter
    }
    return BodyCompleter.getInsertTextForValue(value, separatorAfter)
  }

  private static getSuggestionKind(type: any): CompletionItemKind {
    if (Array.isArray(type)) {
      const array = <any[]>type
      type = array.length > 0 ? array[0] : undefined
    }
    if (!type) {
      return CompletionItemKind.Value
    }
    switch (type) {
      case 'string':
        return CompletionItemKind.Value
      case 'object':
        return CompletionItemKind.Module
      case 'property':
        return CompletionItemKind.Property
      default:
        return CompletionItemKind.Value
    }
  }

  private getInsertTextForProperty(key: string, propertySchema: JSONSchema | undefined, addValue: boolean, separatorAfter: string): string {
    const propertyText = BodyCompleter.getInsertTextForValue(key, '')
    if (!addValue) {
      return propertyText
    }
    const resultText = propertyText + ': '

    let value
    let nValueProposals = 0
    if (propertySchema) {
      if (Array.isArray(propertySchema.defaultSnippets)) {
        if (propertySchema.defaultSnippets.length === 1) {
          const body = propertySchema.defaultSnippets[0].body
          if (isDefined(body)) {
            value = this.getInsertTextForSnippetValue(body, '')
          }
        }
        nValueProposals += propertySchema.defaultSnippets.length
      }
      if (propertySchema.enum) {
        if (!value && propertySchema.enum.length === 1) {
          value = BodyCompleter.getInsertTextForGuessedValue(propertySchema.enum[0], '')
        }
        nValueProposals += propertySchema.enum.length
      }
      if (isDefined(propertySchema.default)) {
        if (!value) {
          value = BodyCompleter.getInsertTextForGuessedValue(propertySchema.default, '')
        }
        nValueProposals++
      }
      if (Array.isArray(propertySchema.examples) && propertySchema.examples.length) {
        if (!value) {
          value = BodyCompleter.getInsertTextForGuessedValue(propertySchema.examples[0], '')
        }
        nValueProposals += propertySchema.examples.length
      }
      if (nValueProposals === 0) {
        let type = Array.isArray(propertySchema.type) ? propertySchema.type[0] : propertySchema.type
        if (!type) {
          if (propertySchema.properties) {
            type = 'object'
          } else if (propertySchema.items) {
            type = 'array'
          }
        }
        switch (type) {
          case 'boolean':
            value = '$1'
            break
          case 'string':
            value = '"$1"'
            break
          case 'object':
            value = '{$1}'
            break
          case 'array':
            value = '[$1]'
            break
          case 'number':
          case 'integer':
            value = '${1:0}'
            break
          case 'null':
            value = '${1:null}'
            break
          default:
            return propertyText
        }
      }
    }
    if (!value || nValueProposals > 1) {
      value = '$1'
    }
    return resultText + value + separatorAfter
  }

  private static evaluateSeparatorAfter(document: TextDocument, offset: number) {
    const scanner = new JSONScanner(document.getText(), true)
    scanner.setPosition(offset)
    const token = scanner.scan()
    switch (token) {
      case SyntaxKind.CommaToken:
      case SyntaxKind.CloseBraceToken:
      case SyntaxKind.CloseBracketToken:
      case SyntaxKind.EOF:
        return ''
      default:
        return ','
    }
  }

  private static findItemAtOffset(node: ArrayASTNode, document: TextDocument, offset: number) {
    const scanner = new JSONScanner(document.getText(), true)
    const children = node.items
    for (let i = children.length - 1; i >= 0; i--) {
      const child = children[i]
      if (offset > child.offset + child.length) {
        scanner.setPosition(child.offset + child.length)
        const token = scanner.scan()
        if (token === SyntaxKind.CommaToken && offset >= scanner.getTokenOffset() + scanner.getTokenLength()) {
          return i + 1
        }
        return i
      } else if (offset >= child.offset) {
        return i
      }
    }
    return 0
  }

  private static fromMarkup(markupString: string | undefined): MarkupContent | string | undefined {
    if (markupString) {
      return {
        kind: MarkupKind.Markdown,
        value: markupString,
      }
    }
    return undefined
  }

  private buildCustomBodyCompletionContext(ctx: CompletionContext) {
    const queryParams: any = {}
    const pathParams: any = {}
    const url = ctx.request?.url
    if (url === undefined || ctx.pathDefinition === undefined) {
      return undefined
    }
    (url.queryParams ?? []).forEach(param => {
      if (param.keyNode?.value) {
        queryParams[param.keyNode?.value] = param.valueNode?.value
      }
    });
    (url.pathSegments ?? []).forEach((pathSegment, idx) => {
      const segment = ctx.pathDefinition.getPathSegment(idx) ?? ''
      if (segment.startsWith('{') && segment.endsWith('}')) {
        pathParams[segment.slice(1, segment.length - 1)] = (pathSegment.value ?? '/').slice(1)
      }
    })
    return {
      method: ctx.request.methodValue,
      pathParams,
      queryParams,
      url: url.value,
    }

  }
}
