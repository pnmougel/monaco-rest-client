import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { RequestAstNode } from '../../../models/ast/request-node'
import { PathSegmentAstNode } from '../../../models/ast/url/path-segment-node'
import { CompletionItemKind, InsertTextFormat, MarkupKind } from 'vscode-languageserver-types'
import { CustomPathCompletionResolver } from '../../../monaco-rest-client'
import { getCompletionItemKind } from '../utils'

export class PathCompleter implements Completer {
  constructor(private customPathCompletion: CustomPathCompletionResolver, private customOptions: any) {}

  isApplicable(ctx: CompletionContext): boolean {
    return !ctx.isInBody && ctx.nodeType === 'pathSegment' || (ctx.nodeType === 'request' && !!(ctx.node as RequestAstNode).method)
  }

  async doComplete(ctx: CompletionContext) {
    const pathSegments = (ctx.request.url?.pathSegments ?? []).map(pathSegment => pathSegment.value.slice(1))
    const pathSegmentIdx = ctx.nodeType === 'pathSegment' ? (ctx.node as PathSegmentAstNode).pathIndex : 0
    const pathSegmentVisited = {}
    let addedCompletions = []
    for await(const path of ctx.openApiService.paths) {

      const candidatePathSegment = path.getPathSegment(pathSegmentIdx)
      if (!!candidatePathSegment && !(candidatePathSegment in pathSegmentVisited) && path.isPartialMatch(ctx.request.methodValue, pathSegments, pathSegmentIdx)) {
        if (candidatePathSegment.startsWith('{')) {
          const pathParam = path.getPathParam(candidatePathSegment.slice(1, candidatePathSegment.length - 1))
          addedCompletions = addedCompletions.concat(await this.customPathCompletion(pathParam.name, pathParam.schema, this.customOptions) ?? [])
          const schema = pathParam.schema

          if (schema?.type === 'boolean') {
            ctx.addEntries(CompletionItemKind.Value, false, [{
              label: 'true',
              insertText: `/true`,
              documentation: pathParam.description,
              filterText: `/${ctx.nodeValue}`,
            }, {
              label: 'false',
              insertText: `/false`,
              documentation: pathParam.description,
              filterText: `/${ctx.nodeValue}`,
            }])
          } else if (schema?.type === 'string' && Array.isArray(schema?.enum)) {
            ctx.addEntries(CompletionItemKind.Value, false, schema.enum.map(option => ({
              label: option,
              insertText: `/${option}`,
              documentation: pathParam.description,
              filterText: `/${ctx.nodeValue}`,
            })))
          }

        } else {
          const pathDescriptions = ctx.openApiService.getPathDescription(candidatePathSegment, pathSegmentIdx, pathSegments)
          const sortedPaths = pathDescriptions.slice(0, 4)
          let desc = sortedPaths.map(path => `\`${path.path}\` [doc](${path.docUrl})\n\n${path.description}`).join(`\n\n---\n\n`)
          if (pathDescriptions.length > 4) {
            desc += `\n\n***+ ${pathDescriptions.length - 4} other endpoint${pathDescriptions.length === 5 ? '' : 's'}***`
          }
          ctx.collector.add({
            label: `${candidatePathSegment}`,
            kind: CompletionItemKind.Value,
            insertText: `/${candidatePathSegment}`,
            insertTextFormat: InsertTextFormat.Snippet,
            filterText: `/${candidatePathSegment}`,
            documentation: {
              kind: MarkupKind.Markdown,
              value: desc,
            },
          })
        }
        pathSegmentVisited[candidatePathSegment] = true
      }
    }
    addedCompletions.forEach(completion => {
      ctx.addEntries(getCompletionItemKind(completion.kind), true, [{
        label: completion.label,
        insertText: `/${completion.label}`,
        filterText: `/${completion.label}`,
        documentation: completion.description,
      }])
    })
  }
}
