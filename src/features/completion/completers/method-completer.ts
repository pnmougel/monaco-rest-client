import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { RequestAstNode } from '../../../models/ast/request-node'
import { CompletionItemKind } from 'vscode-languageserver-types'

export class MethodCompleter implements Completer {
  httpMethods = ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'PATCH', 'OPTIONS', 'TRACE']

  isApplicable(ctx: CompletionContext): boolean {
    return !ctx.request || (!ctx.isInBody && (ctx.nodeType === 'method' || (ctx.nodeType === 'request' && !(ctx.node as RequestAstNode).method)))
  }

  async doComplete(ctx: CompletionContext) {
    ctx.addEntries(CompletionItemKind.Constant, true, this.httpMethods.map(method => ({
        label: method,
        type: 'Http method',
        filterText: method,
        insertText: ctx.nextChar === ' ' ? method : `${method} /`,
        command: {
          title: 'Suggest',
          command: 'editor.action.triggerSuggest',
        },
      }),
    ))
  }
}
