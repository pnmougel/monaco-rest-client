import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { JSONScanner } from '../../../parser/scanner'
import { SyntaxKind } from '../../../models/enums/syntax-kind'

export class EmptyCompleter implements Completer {
  isApplicable(ctx: CompletionContext): boolean {
    return ctx.isInBody && EmptyCompleter.isInComment(ctx.document, ctx.node?.offset ?? 0, ctx.offset)
  }

  async doComplete(ctx: CompletionContext) {
  }

  static isInComment(document: TextDocument, start: number, offset: number) {
    const scanner = new JSONScanner(document.getText(), false)
    scanner.setPosition(start)
    let token = scanner.scan()
    while (token !== SyntaxKind.EOF && (scanner.getTokenOffset() + scanner.getTokenLength() < offset)) {
      token = scanner.scan()
    }
    return (token === SyntaxKind.LineCommentTrivia || token === SyntaxKind.BlockCommentTrivia) && scanner.getTokenOffset() <= offset
  }
}
