import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { QueryParamAstNode } from '../../../models/ast/url/query-param-node'
import { CompletionItemKind, Range } from 'vscode-languageserver-types'

export class QueryValueCompleter implements Completer {
  isApplicable(ctx: CompletionContext): boolean {
    return !ctx.isInBody && !!ctx.pathDefinition && (ctx.nodeType === 'queryValue' || ctx.nodeType === 'queryParam') && (ctx.prevChar === '=' || ctx.nodeType === 'queryValue')
  }

  async doComplete(ctx: CompletionContext) {
    const isStart = ctx.nodeType !== 'queryValue'
    const parent = (ctx.nodeType === 'queryParam' ? ctx.node : ctx.node.parent) as QueryParamAstNode
    const queryParam = ctx.pathDefinition.getQueryParam(parent.keyNode?.value)
    let prevChar = ctx.prevChar

    if (!queryParam) {
      return
    }
    if (isStart) {
      ctx.collector.overwriteRange = Range.create(ctx.document.positionAt(ctx.offset - 1), ctx.document.positionAt(ctx.offset))
    } else {
      prevChar = ''
    }
    const schema = queryParam.schema
    if (schema?.type === 'boolean') {
      ctx.addEntries(CompletionItemKind.Value, true, [{
        label: 'true',
        insertText: `${prevChar}true`,
        filterText: `${prevChar}${ctx.nodeValue}`,
        documentation: queryParam.description,
      }, {
        label: 'false',
        insertText: `${prevChar}false`,
        filterText: `${prevChar}${ctx.nodeValue}`,
        documentation: queryParam.description,
      }])
    } else if (schema?.type === 'string' && Array.isArray(schema?.enum)) {
      ctx.addEntries(CompletionItemKind.Value, true, schema.enum.map((option, idx) => ({
        label: option,
        insertText: `${prevChar}${option}`,
        documentation: (schema.enumDescriptions ?? [])[idx],
        filterText: `${prevChar}${ctx.nodeValue}`,
      })))
    }
  }
}
