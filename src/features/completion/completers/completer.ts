import { CompletionContext } from '../completion-context'

export interface Completer {
  isApplicable(ctx: CompletionContext): boolean

  doComplete(ctx: CompletionContext)
}
