import { Completer } from './completer'
import { CompletionContext } from '../completion-context'
import { QueryParamAstNode } from '../../../models/ast/url/query-param-node'
import { CompletionItemKind, Range } from 'vscode-languageserver-types'
import { CompleterEntry } from '../completer-entry'

export class QueryKeyCompleter implements Completer {
  isApplicable(ctx: CompletionContext): boolean {
    return !ctx.isInBody && !!ctx.pathDefinition && (ctx.prevChar === '?' || ctx.prevChar === '&' || ctx.nodeType === 'queryKey')
  }

  async doComplete(ctx: CompletionContext) {
    const parent = (ctx.nodeType === 'queryParam' ? ctx.node : ctx.node.parent) as QueryParamAstNode
    const hasEqualSign = !!parent?.hasEqualSign
    const isStart = ctx.nodeType !== 'queryKey'
    let prevChar = ctx.prevChar
    const existingParams: any = {}
    const curWord = ctx.nodeType === 'queryKey' ? ctx.nodeValue.toString() : '';
    (ctx.request.url.queryParams ?? []).forEach(param => {
      if (!!param.keyNode) {
        existingParams[param.keyNode.value] = true
      }
    })
    if (isStart) {
      ctx.collector.overwriteRange = Range.create(ctx.document.positionAt(ctx.offset - 1), ctx.document.positionAt(ctx.offset))
    } else {
      prevChar = ''
      if (hasEqualSign) {
        ctx.collector.overwriteRange.end.character += 1
      }
    }

    const entries: CompleterEntry[] = []
    ctx.pathDefinition.queryParams.forEach(queryParam => {
      if (!(queryParam.name in existingParams) || queryParam.name === curWord) {
        let insertText = `${prevChar}${queryParam.name}=`
        const schema = queryParam.schema

        if (!!schema?.default && !hasEqualSign) {
          insertText += `\${1:${schema?.default}}`
        }
        entries.push({
          label: queryParam.name,
          type: schema?.type,
          insertText,
          deprecated: queryParam.deprecated,
          documentation: queryParam.description,
          filterText: `${prevChar}${queryParam.name}`,
          command: {
            title: 'Suggest',
            command: 'editor.action.triggerSuggest',
          },
        })
      }
    })
    ctx.addEntries(CompletionItemKind.Value, false, entries)
  }
}
