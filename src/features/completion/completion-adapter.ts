import type * as monaco from 'monaco-editor-core'
import { MonacoToProtocolConverter, ProtocolToMonacoConverter } from '../../utils/monaco-converter'
import { WorkerAccessor } from '../../monaco-rest-client'

export class CompletionAdapter implements monaco.languages.CompletionItemProvider {
  m2p: MonacoToProtocolConverter
  p2m: ProtocolToMonacoConverter

  constructor(private readonly _monaco: typeof monaco, private worker: WorkerAccessor) {
    this.m2p = new MonacoToProtocolConverter(_monaco)
    this.p2m = new ProtocolToMonacoConverter(_monaco)
  }

  provideCompletionItems(
    model: monaco.editor.IReadOnlyModel,
    position: monaco.Position,
    context: monaco.languages.CompletionContext,
    token: monaco.CancellationToken,
  ): Promise<monaco.languages.CompletionList> {
    return this.worker(model.uri)
      .then((worker) => {
        return worker.doComplete(model.uri.toString(), this.m2p.asPosition(position.lineNumber, position.column))
      })
      .then((completions) => {
        const wordUntil = model.getWordUntilPosition(position)
        const defaultRange = new this._monaco.Range(position.lineNumber, wordUntil.startColumn, position.lineNumber, wordUntil.endColumn)

        return this.p2m.asCompletionResult(completions, defaultRange)
      })
  }
}
