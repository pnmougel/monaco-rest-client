import { CompletionItem, CompletionList, TextEdit } from 'vscode-languageserver-protocol'
import { Range } from 'vscode-languageserver-types'

export interface PathDescription {
  path: string
  description: string
  docUrl: string,
  parts: string[]
}

export class CompletionsCollector {
  proposed: { [key: string]: CompletionItem } = {}
  result: CompletionList
  constructor(public overwriteRange: Range) {
    this.result = {
      items: [],
      isIncomplete: false,
    }
  }

  add(suggestion: CompletionItem) {
    let label = suggestion.label
    if (typeof suggestion.label === 'object') {
      // @ts-ignore
      label = suggestion.label.name
    }

    if (Array.isArray(suggestion.detail)) {
      suggestion.detail = suggestion.detail.join(' | ')
    }

    const existing = this.proposed[label]
    if (!existing) {
      label = label.replace(/[\n]/g, '↵')
      if (label.length > 60) {
        const shortenedLabel = label.substr(0, 57).trim() + '...'
        if (!this.proposed[shortenedLabel]) {
          label = shortenedLabel
        }
      }
      if (this.overwriteRange && suggestion.insertText !== undefined) {
        suggestion.textEdit = TextEdit.replace(this.overwriteRange, suggestion.insertText)
      }
      if (typeof suggestion.label === 'object') {
        // @ts-ignore
        suggestion.label.name = label
      } else {
        suggestion.label = label
      }
      this.proposed[label] = suggestion
      this.result.items.push(suggestion)
    } else {
      if (!existing.documentation) {
        existing.documentation = suggestion.documentation
      }
      if (!existing.detail) {
        existing.detail = suggestion.detail
      }
    }
  }
  setAsIncomplete() {
    this.result.isIncomplete = true
  }
  getNumberOfProposals() {
    return this.result.items.length
  }
}
