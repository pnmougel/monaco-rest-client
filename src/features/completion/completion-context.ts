import { TextDocument } from 'vscode-languageserver-textdocument'
import { CompletionItemKind, InsertTextFormat, MarkupKind, Position, Range } from 'vscode-languageserver-types'
import { CompletionsCollector } from './completions-collector'
import { RequestAstNode } from '../../models/ast/request-node'
import { RequestsDocument } from '../../models/requests-document'
import { ASTNode } from '../../models/ast/ast-node'
import { OpenApiService } from '../../services/open-api-service'
import { PathDefinition } from '../../services/path-definition'
import { JSONDocument } from '../../models/json-document'
import { CompleterEntry } from './completer-entry'
import { RestClientOptions } from '../../configuration/rest-client-options'

export class CompletionContext {
  public readonly offset: number
  public readonly text: string
  public readonly currentWord: string

  public readonly request?: RequestAstNode
  public readonly pathDefinition?: PathDefinition
  public readonly schemaDefinition?: any
  public readonly jsonDoc?: JSONDocument

  public readonly node?: ASTNode
  public readonly nodeType?: string
  public readonly nodeValue?: string

  public readonly collector: CompletionsCollector
  public readonly isInBody: boolean
  public readonly prevChar: string
  public readonly nextChar: string
  private readonly overwriteRange: Range
  constructor(
    public readonly document: TextDocument,
    public readonly position: Position,
    public readonly requestsDocument: RequestsDocument,
    public readonly openApiService: OpenApiService,
    public readonly options: RestClientOptions,
  ) {
    this.offset = document.offsetAt(position)
    this.text = document.getText()
    this.currentWord = this.getCurrentWord()

    this.request = requestsDocument.getRequestAt(this.offset)
    this.pathDefinition = this.openApiService.getPathDefinition(this.request, options.schemaDef)
    this.schemaDefinition = this.openApiService.getSchemaForRequest(this.request, options.schemaDef)

    this.node = this.request?.getNodeFromOffset(this.offset, true)
    this.nodeType = this.node?.type

    this.nodeValue = this.node?.value
    this.prevChar = this.text.charAt(this.offset - 1)
    this.nextChar = this.text.charAt((this.node?.offset ?? 0) + (this.node?.length ?? 0))

    this.jsonDoc = this.request?.jsonDoc

    if (!!this.nodeType && (this.nodeType === 'string' || this.nodeType === 'number' || this.nodeType === 'boolean' || this.nodeType === 'null'
      || this.nodeType === 'method' || this.nodeType === 'pathSegment' || this.nodeType === 'queryKey' || this.nodeType === 'queryValue')) {
      this.overwriteRange = Range.create(document.positionAt(this.node.offset), document.positionAt(this.node.offset + this.node.length))
    } else {
      let overwriteStart = this.offset - this.currentWord.length
      if (overwriteStart > 0 && this.text[overwriteStart - 1] === '"') {
        overwriteStart--
      }
      this.overwriteRange = Range.create(document.positionAt(overwriteStart), position)
    }
    this.isInBody = this.jsonDoc !== undefined && this.offset > this.jsonDoc.root.offset
    this.collector = new CompletionsCollector(this.overwriteRange)
  }

  public addEntries(kind: CompletionItemKind, sorted: boolean, entries: CompleterEntry[]) {
    entries.forEach((entry, idx) => {
      const type = Array.isArray(entry.type) ? entry.type.join(', ') : `${entry.type}`
      const label: any = !!entry.type ? { name: entry.label, type } : entry.label
      const item: any = {
        label: label,
        kind,
        insertText: entry.insertText,
        insertTextFormat: InsertTextFormat.Snippet,
        filterText: entry.filterText,
        deprecated: entry.deprecated,
        commitCharacters: entry.commitCharacters,
      }
      if (entry.documentation !== undefined) {
        if (typeof entry.documentation === 'string') {
          item.documentation = { kind: MarkupKind.Markdown, value: entry.documentation }
        } else {
          item.documentation = entry.documentation
        }
      }
      if (!!entry.command) {
        item.command = entry.command
      }
      if (sorted) {
        item.sortText = `${this.collector.getNumberOfProposals()}`
      }
      this.collector.add(item)
    })

  }

  private getCurrentWord() {
    let i = this.offset - 1
    while (i >= 0 && ' \t\n\r\v":{[,]}'.indexOf(this.text.charAt(i)) === -1) {
      i--
    }
    return this.text.substring(i + 1, this.offset)
  }
}
