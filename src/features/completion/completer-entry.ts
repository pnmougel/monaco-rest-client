export interface CompleterEntry {
  label: string
  type?: string
  insertText: string
  filterText: string
  deprecated?: boolean
  documentation?: string,
  commitCharacters?: string[],
  command?: any
}
