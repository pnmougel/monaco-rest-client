import { CompletionItemKind } from 'vscode-languageserver-types'

export function getCompletionItemKind(v: number): CompletionItemKind {
  const itemKinds = [
    CompletionItemKind.Constant,
    CompletionItemKind.Text,
    CompletionItemKind.Method,
    CompletionItemKind.Function,
    CompletionItemKind.Constructor,
    CompletionItemKind.Field,
    CompletionItemKind.Variable,
    CompletionItemKind.Class,
    CompletionItemKind.Interface,
    CompletionItemKind.Module,
    CompletionItemKind.Property,
    CompletionItemKind.Unit,
    CompletionItemKind.Value,
    CompletionItemKind.Enum,
    CompletionItemKind.Keyword,
    CompletionItemKind.Snippet,
    CompletionItemKind.Color,
    CompletionItemKind.File,
    CompletionItemKind.Reference,
    CompletionItemKind.Folder,
    CompletionItemKind.EnumMember,
    CompletionItemKind.Constant,
    CompletionItemKind.Struct,
    CompletionItemKind.Event,
    CompletionItemKind.Operator,
    CompletionItemKind.TypeParameter,
  ]
  return itemKinds[v] ?? CompletionItemKind.Constant
}
