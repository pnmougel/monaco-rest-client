export interface FormattingOptions {
  /**
   * If indentation is based on spaces (`insertSpaces` = true), the number of spaces that make an indent.
   */
  tabSize?: number;
  /**
   * Is indentation based on spaces?
   */
  insertSpaces?: boolean;
  /**
   * The default 'end of line' character. If not set, '\n' is used as default.
   */
  eol?: string;
  /**
   * If set, will add a new line at the end of the document.
   */
  insertFinalNewline?: boolean;
}
