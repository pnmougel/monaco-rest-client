import type * as monaco from 'monaco-editor-core'
import { MonacoToProtocolConverter, ProtocolToMonacoConverter } from '../../utils/monaco-converter'
import { WorkerAccessor } from '../../monaco-rest-client'


export class FormatAdapter implements monaco.languages.DocumentRangeFormattingEditProvider {
  m2p: MonacoToProtocolConverter
  p2m: ProtocolToMonacoConverter

  constructor(private readonly _monaco: typeof monaco, private worker: WorkerAccessor) {
    this.m2p = new MonacoToProtocolConverter(_monaco)
    this.p2m = new ProtocolToMonacoConverter(_monaco)
  }

  provideDocumentRangeFormattingEdits(model: monaco.editor.ITextModel, range: monaco.Range,
                                      formattingOptions: monaco.languages.FormattingOptions, token: monaco.CancellationToken): monaco.languages.ProviderResult<monaco.languages.TextEdit[]> {
    return this.worker(model.uri)
      .then((worker) => {
        const o = this.m2p.asFormattingOptions(formattingOptions)
        const options = {
          tabSize: o ? o.tabSize : 4,
          insertSpaces: o?.insertSpaces === true,
          insertFinalNewline: true,
          eol: '\n',
        }
        return worker.doFormat(model.uri.toString(), this.m2p.asRange(range), options)
      })
      .then((res) => {
        return this.p2m.asTextEdits(res)
      })
  }
}
