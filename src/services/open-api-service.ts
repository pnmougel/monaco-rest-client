import { RequestAstNode } from '../models/ast/request-node'
import { PathDefinition } from './path-definition'
import { OpenAPIObject, PathItemObject } from 'openapi3-ts'
import { SchemaDef, SchemaDefPath } from '../monaco-rest-client'

import { PathDescription } from '../features/completion/completions-collector'

export class OpenApiService {
  spec: any = undefined
  paths: PathDefinition[] = []
  pathDescriptions: Map<string, PathDescription[]>[]

  constructor() {
    this.pathDescriptions = []
  }
  get openApiSpec() {
    return this.spec
  }
  set openApiSpec(spec: OpenAPIObject) {
    this.spec = spec
    this.paths = Object.entries(this.spec.paths).map(([path, pathDef]) => {
      pathDef = this.resolveEntry(pathDef)
      return Object.entries(pathDef).map(([method, def]) => {
        def = this.resolveEntry(def)
        const pathDefinition = new PathDefinition(this, path, method, def)
        pathDefinition.pathParts.forEach((pathPart, idx) => {
          if (this.pathDescriptions[idx] === undefined) {
            this.pathDescriptions[idx] = new Map()
          }
          let pathDescription = this.pathDescriptions[idx].get(pathPart) ?? []
          if (!pathDescription.some(p => p.path === `/${pathDefinition.path}`)) {
            pathDescription.push({
              path: `/${pathDefinition.path}`,
              description: pathDefinition.description,
              docUrl: pathDefinition.descriptionUrl,
              parts: pathDefinition.pathParts,
            })
            pathDescription = pathDescription.sort((a, b) => a.path.length - b.path.length)
            this.pathDescriptions[idx].set(pathPart, pathDescription)
          }
        })
        return pathDefinition
      })
    }).flat()
  }
  resolveEntry<T>(entry: PathItemObject): T {
    let res = { ...entry }
    while (!!res.$ref) {
      const resolvedRef = this.resolveRef(res.$ref)
      delete res.$ref
      res = { ...res, ...resolvedRef }
    }
    return res as T
  }
  getSchemaForRequest(request?: RequestAstNode, schemaDef?: SchemaDef) {
    if (schemaDef !== undefined) {
      // A schema definition is provided, we always retrieve the schema for this path regardless of the request
      if (schemaDef.type === 'ref') {
        return this.resolveRef(schemaDef.$ref)
      } else if (schemaDef.type === 'path') {
        return this.paths.find(path => path.isMatchSchemaPath(schemaDef as SchemaDefPath))?.schema
      } else if (schemaDef.type === 'schema') {
        return schemaDef.schema
      }
    } else {
      if (!request || !request.urlValue || !request.methodValue) {
        return undefined
      }
      const matchingPath = this.paths.find(path => path.isRequestMatch(request))
      return matchingPath?.schema
    }
  }

  getPathDefinition(request?: RequestAstNode, schemaDef?: SchemaDef) {
    if (schemaDef !== undefined) {
      if (schemaDef.type === 'path') {
        return this.paths.find(path => path.isMatchSchemaPath(schemaDef as SchemaDefPath))
      } else if (schemaDef.type === 'schema') {
        return undefined
      }
    } else {
      if (!request || !request.urlValue || !request.methodValue) {
        return undefined
      }
      return this.paths.find(path => path.isRequestMatch(request))
    }
  }

  getPathDescription(candidatePathSegment: string, pathSegmentIdx: number, pathSegments: string[]) {
    const pathDescriptions = this.pathDescriptions[pathSegmentIdx].get(candidatePathSegment)
    return pathDescriptions.filter(pathDescription => {
      let match = true
      for (let i = 0; i !== pathSegmentIdx && match; i++) {
        if (!pathDescription.parts[i].startsWith('{') && pathDescription.parts[i] !== pathSegments[i]) {
          match = false
        }
      }
      return match
    })
  }

  resolveRef(ref: string) {
    let res = this.spec
    ref.split('/').forEach(part => {
      if (part !== '#') {
        if (!(part in res)) {
          console.error(`Unable to resolve ref ${ref}`, part)
        } else {
          res = res[part]
        }
      }
    })
    return res
  }
}
