import { RequestAstNode } from '../models/ast/request-node'
import { OperationObject, ParameterObject, RequestBodyObject, SchemaObject } from 'openapi3-ts/src/model/OpenApi'
import { OpenApiService } from './open-api-service'
import { SchemaDefPath } from '../monaco-rest-client'

export interface ResolvedParameterObject extends ParameterObject {
  schema?: SchemaObject
}

export class PathDefinition {
  public readonly pathParts: string[]
  private _pathParams: ResolvedParameterObject[] = []
  private readonly _schema: SchemaObject
  private readonly _description: string | undefined
  private readonly _descriptionUrl: string | undefined
  constructor(private openApiService: OpenApiService,
              public readonly path: string,
              public readonly method: string,
              public readonly pathDefinition: OperationObject) {
    if (this.path.startsWith('/')) {
      this.path = this.path.slice(1)
    }
    this.pathParts = this.path.split('/').map(p => p.toLowerCase())
    this.method = method.toLowerCase();

    (this.pathDefinition.parameters ?? []).forEach(parameter => {
      parameter = openApiService.resolveEntry<ParameterObject>(parameter)
      if (!!parameter.schema && typeof parameter === 'object' && '$ref' in parameter.schema) {
        parameter.schema = this.openApiService.resolveEntry<SchemaObject>(parameter.schema)
      }

      if (parameter.in === 'query') {
        this._queryParams.push(parameter)
      } else if (parameter.in === 'path') {
        this._pathParams.push(parameter)
      }
    })

    if (!!this.pathDefinition.requestBody) {
      const requestBody = openApiService.resolveEntry<RequestBodyObject>(this.pathDefinition.requestBody)
      if ('application/json' in requestBody.content && 'schema' in requestBody.content['application/json']) {
        this._schema = openApiService.resolveEntry<SchemaObject>(requestBody.content['application/json'].schema)
      }
    }
    this._description = this.pathDefinition.description
    this._descriptionUrl = this.pathDefinition.externalDocs?.url
  }
  private _queryParams: ResolvedParameterObject[] = []
  get queryParams(): ResolvedParameterObject[] {
    return this._queryParams
  }
  get schema() { return this._schema }
  get description() { return this._description }
  get descriptionUrl() { return this._descriptionUrl }
  getPathSegment(idx: number) {
    return this.pathParts[idx]
  }
  getQueryParam(name: string): ResolvedParameterObject | undefined {
    return this._queryParams.find(param => param.name === name)
  }

  getPathParam(name: string): ResolvedParameterObject | undefined {
    return this._pathParams.find(param => param.name === name)
  }

  isMatch(httpMethod: string, pathSegments: string[]) {
    if (this.pathParts.length !== pathSegments.length || this.method !== httpMethod.toLowerCase()) {
      return false
    }
    let isMatch = true
    for (let i = 0; i !== pathSegments.length && isMatch; i++) {
      isMatch = (pathSegments[i] === this.pathParts[i]) || (this.pathParts[i].startsWith('{') && !pathSegments[i].startsWith('_'))
    }
    return isMatch
  }

  isRequestMatch(request?: RequestAstNode) {
    if (!request || !request.urlValue || !request.methodValue) {
      return undefined
    }
    return this.isMatch(request.methodValue.toLowerCase(), PathDefinition.getPathSegments(request.urlValue))
  }

  isPartialMatch(method: string, pathSegments: string[], pathSegmentIdx: number) {

    if (pathSegmentIdx >= this.pathParts.length || (!!method && method.toLowerCase() !== this.method)) {
      return false
    }
    for (let i = 0; i < pathSegmentIdx; i++) {
      if (this.pathParts[i] !== pathSegments[i] && !(this.pathParts[i].startsWith('{') && !pathSegments[i].startsWith('_'))) {
        return false
      }
    }
    return true
  }

  isMatchSchemaPath(schemaPath: SchemaDefPath) {
    const path = schemaPath.path.startsWith('/') ? schemaPath.path.slice(1) : schemaPath.path
    return schemaPath.method.toLowerCase() === this.method && this.path === path
  }

  private static getPathSegments(url: string) {
    if (!url) {
      return []
    }
    if (url.startsWith('/')) {
      url = url.slice(1)
    }
    return url.split('?')[0].toLowerCase().split('/')
  }
}
