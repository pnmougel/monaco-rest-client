import * as worker from 'monaco-editor-core/esm/vs/editor/editor.worker'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { OpenApiService } from '../services/open-api-service'
import { parse } from '../parser/parse'
import { Position, Range } from 'vscode-languageserver-types'
import { CompletionService } from '../features/completion/completion-service'
import { HoverService } from '../features/hover/hover-service'
import { FormatService } from '../features/format/format-service'
import type { languages } from 'monaco-editor-core'
import { FoldingService } from '../features/folding/folding-service'
import { FoldingRangesContext } from '../features/folding/folding-range-context'
import { ValidationService } from '../features/validation/validation-service'
import { CodeLensService } from '../features/codelens/code-lens-service'
import { RestClientOptions } from '../configuration/rest-client-options'
import { TypeFormattingEditService } from '../features/type-formatting-edit/type-formatting-edit-service'
import IWorkerContext = worker.IWorkerContext

export class RestClientWorker {
  private readonly languageOptions: RestClientOptions
  private readonly modelOptions: { [modelUri: string]: RestClientOptions }
  private readonly languageId: string

  private readonly openApiService: OpenApiService
  private completionService: CompletionService
  private hoverService: HoverService
  private formatService: FormatService
  private foldingService: FoldingService
  private validationService: ValidationService
  private codeLensService: CodeLensService
  private typeFormattingEditsService: TypeFormattingEditService

  constructor(private readonly ctx: IWorkerContext, private readonly createData: ICreateData) {
    this.languageOptions = createData.languageOptions
    this.modelOptions = createData.modelOptions

    this.openApiService = new OpenApiService()

    if (!!createData.languageOptions.openApiSpec) {
      this.openApiService.openApiSpec = this.languageOptions.openApiSpec
    }

    this.languageId = createData.languageId
    this.completionService = new CompletionService(this.openApiService)

    this.hoverService = new HoverService(this.openApiService)
    this.formatService = new FormatService()
    this.foldingService = new FoldingService()
    this.validationService = new ValidationService(this.openApiService)
    this.codeLensService = new CodeLensService()
    this.typeFormattingEditsService = new TypeFormattingEditService(this.openApiService)
  }

  doValidation(uri: string) {
    const { document, options } = this.getTextDocument(uri)
    const requestsDocument = parse(document, options)
    return this.validationService.doValidation(document, requestsDocument, options)
  }

  doComplete(uri: string, position: Position) {
    const { document, options } = this.getTextDocument(uri)
    const requestsDocument = parse(document, options)
    return this.completionService.doComplete(document, position, requestsDocument, options)
  }

  doHover(uri: string, position: Position) {
    const { document, options } = this.getTextDocument(uri)
    const requestsDocument = parse(document, options)
    return this.hoverService.doHover(document, position, requestsDocument, options)
  }

  doFormat(uri: string, range?: Range, formatOptions?: languages.FormattingOptions) {
    const { document, options } = this.getTextDocument(uri)
    let jsonRange
    if (range) {
      const offset = document.offsetAt(range.start)
      const length = document.offsetAt(range.end) - offset
      jsonRange = { offset, length }
    }
    return this.formatService.doFormat(document, jsonRange, formatOptions)
  }

  getCodeLens(uri: string) {
    const { document, options } = this.getTextDocument(uri)
    return this.codeLensService.getCodeLens(document, uri, options)
  }

  getFoldingRange(uri: string, context: FoldingRangesContext) {
    const { document, options } = this.getTextDocument(uri)
    return this.foldingService.getFoldingRanges(document, context)
  }

  getTypeFormattingEdits(uri: string, line: string, appendNewLine: boolean, isLastLine: boolean, position: any) {
    const { document, options } = this.getTextDocumentWithContent(uri, line)
    return this.typeFormattingEditsService.getTypeFormattingEdits(document, position, appendNewLine, isLastLine, options)
  }

  private getTextDocument(uri: string): { document: TextDocument, options: RestClientOptions } {
    let options: RestClientOptions = this.languageOptions
    if (uri in this.modelOptions) {
      options = { ...options, ...this.modelOptions[uri] }
    }
    let models = this.ctx.getMirrorModels()
    for (let model of models) {
      if (model.uri.toString() === uri) {
        return { document: TextDocument.create(uri, this.languageId, model.version, model.getValue()), options }
      }
    }
    return null
  }

  private getTextDocumentWithContent(uri: string, content: string): { document: TextDocument, options: RestClientOptions } {
    let options: RestClientOptions = this.languageOptions
    if (uri in this.modelOptions) {
      options = { ...options, ...this.modelOptions[uri] }
    }
    let models = this.ctx.getMirrorModels()
    for (let model of models) {
      if (model.uri.toString() === uri) {
        return { document: TextDocument.create(uri, this.languageId, model.version, content), options }
      }
    }
    return null
  }
}

export interface ICreateData {
  languageId: string;
  languageOptions: RestClientOptions;
  modelOptions: { [key: string]: RestClientOptions }
}
