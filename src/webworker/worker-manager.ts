import type { RestClientWorker } from './rest-client-worker'
import type * as monaco from 'monaco-editor-core'
import { LanguageServiceOptions } from '../configuration/language-service-options'
import { fnStringify } from '../utils/fn-stringify'

const STOP_WHEN_IDLE_FOR = 2 * 60 * 1000 // 2min

export class WorkerManager {
  private _languageServiceOptions: LanguageServiceOptions
  private readonly _idleCheckInterval: number
  private _lastUsedTime: number
  private _configChangeListener: monaco.IDisposable

  private _worker: monaco.editor.MonacoWebWorker<RestClientWorker>
  private _client: Promise<RestClientWorker>

  constructor(private readonly _monaco: typeof monaco, languageServiceOptions: LanguageServiceOptions) {
    this._languageServiceOptions = languageServiceOptions
    this._worker = null
    this._idleCheckInterval = window.setInterval(() => this._checkIfIdle(), 30 * 1000)
    this._lastUsedTime = 0
    this._configChangeListener = this._languageServiceOptions.onDidChange(() => this._stopWorker())
  }
  dispose(): void {
    clearInterval(this._idleCheckInterval)
    this._configChangeListener.dispose()
    this._stopWorker()
  }
  getLanguageServiceWorker(...resources: monaco.Uri[]): Promise<RestClientWorker> {
    let _client: RestClientWorker
    return this._getClient()
      .then((client) => {
        _client = client
      })
      .then((_) => {
        return this._worker.withSyncedResources(resources)
      })
      .then((_) => _client)
  }
  private _stopWorker(): void {
    if (this._worker) {
      this._worker.dispose()
      this._worker = null
    }
    this._client = null
  }
  private _checkIfIdle(): void {
    if (!this._worker) {
      return
    }
    let timePassedSinceLastUsed = Date.now() - this._lastUsedTime
    if (timePassedSinceLastUsed > STOP_WHEN_IDLE_FOR) {
      this._stopWorker()
    }
  }
  private _getClient(): Promise<RestClientWorker> {
    this._lastUsedTime = Date.now()

    if (!this._client) {
      const languageOptions = this._languageServiceOptions.options
      if (typeof languageOptions.customPathCompletion !== 'string') {
        languageOptions.customPathCompletion = fnStringify(languageOptions.customPathCompletion)
      }
      if (typeof languageOptions.customQueryCompletion !== 'string') {
        languageOptions.customQueryCompletion = fnStringify(languageOptions.customQueryCompletion)
      }
      if (typeof languageOptions.customBodyCompletion !== 'string') {
        languageOptions.customBodyCompletion = fnStringify(languageOptions.customBodyCompletion)
      }
      this._worker = this._monaco.editor.createWebWorker<RestClientWorker>({
        moduleId: 'RestClientWorker',
        label: this._languageServiceOptions.languageId,
        createData: {
          languageOptions: this._languageServiceOptions.options,
          languageId: this._languageServiceOptions.languageId,
          modelOptions: this._languageServiceOptions.modelOptions,
        },
      })
      this._client = <Promise<RestClientWorker>>(<any>this._worker.getProxy())
    }

    return this._client
  }
}
