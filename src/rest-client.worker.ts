import * as worker from 'monaco-editor-core/esm/vs/editor/editor.worker'
import { ICreateData, RestClientWorker } from './webworker/rest-client-worker'
import IWorkerContext = worker.IWorkerContext

self.onmessage = () => {
  // ignore the first message
  worker.initialize((ctx: IWorkerContext, createData: ICreateData) => {
    return new RestClientWorker(ctx, createData)
  })
}

