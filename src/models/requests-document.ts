import { RequestAstNode } from './ast/request-node'
import { Diagnostic } from 'vscode-languageserver-protocol'

export class RequestsDocument {
  requests: RequestAstNode[]

  constructor(public readonly syntaxErrors: Diagnostic[] = []) {
    this.requests = []
  }

  public getRequestFromOffset(offset: number,
                              // noinspection
                              includeRightBound = false): RequestAstNode | undefined {
    let prevRequest = undefined
    for (let i = 0; i !== this.requests.length; i++) {
      if (this.requests[i].offset >= offset) {
        return prevRequest
      }
      prevRequest = this.requests[i]
    }
    if (this.requests.length > 0) {
      return this.requests[this.requests.length - 1]
    } else {
      return undefined
    }
  }

  public getRequestAt(offset: number): RequestAstNode | undefined {
    for (let i = 0; i !== this.requests.length; i++) {
      const req = this.requests[i]
      if (offset >= req.offset && offset <= req.offset + req.length) {
        return req
      }
    }
    return undefined
  }
}

