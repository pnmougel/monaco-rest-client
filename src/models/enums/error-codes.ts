/**
 * Error codes used by diagnostics
 */
export enum ErrorCode {
  Undefined = 0,
  EnumValueMismatch = 1,
  Deprecated = 2,
  UnexpectedEndOfComment = 0x101,
  UnexpectedEndOfString = 0x102,
  UnexpectedEndOfNumber = 0x103,
  InvalidUnicode = 0x104,
  InvalidEscapeCharacter = 0x105,
  InvalidCharacter = 0x106,
  InvalidHttpMethod = 0x107,
  PropertyExpected = 0x201,
  CommaExpected = 0x202,
  ColonExpected = 0x203,
  ValueExpected = 0x204,
  CommaOrCloseBacketExpected = 0x205,
  CommaOrCloseBraceExpected = 0x206,
  TrailingComma = 0x207,
  DuplicateKey = 0x208,
  CommentNotPermitted = 0x209,
  PathExpected = 0x210,
  QueryParamKeyExpected = 0x211,
  QueryParamValueExpected = 0x212,
  QueryParamExpected = 0x213,
  MethodExpected = 0x214,
  UnexpectedBody = 0x215,
  BodyExpected = 0x216,
  InvalidMustacheTemplate = 0x217,

  // Url semantic
  InvalidPath = 0x250,
  InvalidQueryParamKey = 0x251,
  InvalidQueryParamValue = 0x252,

  SchemaResolveError = 0x300
}
