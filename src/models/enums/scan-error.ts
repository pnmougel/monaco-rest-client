export const enum ScanError {
  None = 0,
  UnexpectedEndOfComment = 1,
  UnexpectedEndOfString = 2,
  UnexpectedEndOfNumber = 3,
  UnexpectedEndOfMustacheTemplate = 4,
  InvalidUnicode = 5,
  InvalidEscapeCharacter = 6,
  InvalidCharacter = 7
}
