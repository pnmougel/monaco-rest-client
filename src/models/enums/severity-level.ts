export type SeverityLevel = 'error' | 'warning' | 'ignore';
