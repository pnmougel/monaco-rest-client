import { ASTNode } from '../ast-node'

export class QueryParamValueAstNode extends ASTNode {
  public type: 'queryValue' = 'queryValue'
  constructor(parent: ASTNode | undefined, offset: number, public value: string) {
    super(parent, offset)
    this.length = value.length
  }
}
