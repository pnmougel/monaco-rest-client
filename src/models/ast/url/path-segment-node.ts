import { ASTNode } from '../ast-node'

export class PathSegmentAstNode extends ASTNode {
  public type: 'pathSegment' = 'pathSegment'
  constructor(parent: ASTNode | undefined, offset: number, public value: string, public readonly pathIndex: number) {
    super(parent, offset)
    this.length = value.length
  }
}
