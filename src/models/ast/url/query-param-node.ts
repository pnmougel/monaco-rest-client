import { ASTNode } from '../ast-node'
import { QueryParamKeyAstNode } from './query-param-key-node'
import { QueryParamValueAstNode } from './query-param-value-node'
import { CharacterCodes } from '../../enums/character-codes'
import { UrlAstNode } from './url-node'
import { ErrorCode } from '../../enums/error-codes'

export class QueryParamAstNode extends ASTNode {
  public type: 'queryParam' = 'queryParam'
  public keyNode: QueryParamKeyAstNode
  public valueNode: QueryParamValueAstNode
  public items: ASTNode[] = []
  public hasEqualSign = false

  constructor(parent: UrlAstNode | undefined, offset: number, public value: string) {
    super(parent, offset)
    this.length = value.length
    this.parseQueryParam()
  }
  public get children(): ASTNode[] {
    return this.items
  }
  public parseQueryParam() {
    let equalPosition = -1
    for (let i = 0; i !== this.value.length; i++) {
      const code = this.value.charCodeAt(i)
      if (code === CharacterCodes.equals) {
        equalPosition = i
        this.hasEqualSign = true
        if (i !== 0) {
          this.keyNode = new QueryParamKeyAstNode(this, this.offset, this.value.slice(0, i))
        } else {
          (this.parent as UrlAstNode).errors.push({
            code: ErrorCode.QueryParamKeyExpected,
            message: 'Query parameter key expected',
            start: this.offset,
            end: this.offset + 1,
          })
        }
      }
    }
    if (equalPosition === -1) {
      this.keyNode = new QueryParamKeyAstNode(this, this.offset, this.value)
    } else if (equalPosition < this.value.length - 1) {
      this.valueNode = new QueryParamValueAstNode(this, this.offset + equalPosition + 1, this.value.slice(equalPosition + 1))
    } else {
      (this.parent as UrlAstNode).errors.push({
        code: ErrorCode.QueryParamValueExpected,
        message: 'Query parameter value expected',
        start: this.offset + equalPosition,
        end: this.offset + equalPosition + 1,
      })
    }

    if (this.keyNode) {
      this.items.push(this.keyNode)
    }
    if (this.valueNode) {
      this.items.push(this.valueNode)
    }
  }
}
