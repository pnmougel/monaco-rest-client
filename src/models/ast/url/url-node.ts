import { ASTNode } from '../ast-node'
import { PathSegmentAstNode } from './path-segment-node'
import { QueryParamAstNode } from './query-param-node'
import { CharacterCodes } from '../../enums/character-codes'
import { ErrorCode } from '../../enums/error-codes'
import { ParseError } from '../../../parser/parse-error'

export class UrlAstNode extends ASTNode {
  public type: 'url' = 'url'

  public pathSegments: PathSegmentAstNode[] = []
  public queryParams: QueryParamAstNode[] = []
  public errors: ParseError[] = []
  private items: ASTNode[] = []
  constructor(parent: ASTNode | undefined, offset: number, public readonly value: string) {
    super(parent, offset)

    this.parseUrl()
  }
  public get children(): ASTNode[] {
    return this.items
  }
  parseUrl() {
    let pathSegmentIndex = 0
    let questionPosition = -1
    let start = 0
    for (let i = 1; i < this.value.length; i++) {
      const code = this.value.charCodeAt(i)
      if (code === CharacterCodes.slash && questionPosition === -1) {
        pathSegmentIndex = this.addPathSegment(start, i, pathSegmentIndex)
        start = i
      } else if (code === CharacterCodes.question && questionPosition === -1) {
        questionPosition = i
        pathSegmentIndex = this.addPathSegment(start, i, pathSegmentIndex)
        start = i + 1
      } else if (code === CharacterCodes.ampersand && questionPosition !== -1) {
        this.addQueryParamSegment(start, i)
        start = i + 1
      }
    }

    if (start < this.value.length) {
      if (questionPosition === -1) {
        this.addPathSegment(start, this.value.length, pathSegmentIndex)
      } else {
        this.addQueryParamSegment(start, this.value.length)
      }
    }
  }
  private addPathSegment(start: number, end: number, pathSegmentIndex: number) {
    if (end - start > 0) {
      const pathSegmentNode = new PathSegmentAstNode(this, this.offset + start, this.value.slice(start, end), pathSegmentIndex)
      this.pathSegments.push(pathSegmentNode)
      this.items.push(pathSegmentNode)
      return pathSegmentIndex + 1
    } else {
      this.errors.push({
        code: ErrorCode.PathExpected,
        message: 'Path expected',
        start: this.offset + start,
        end: this.offset + start + 1,
      })
    }
    return pathSegmentIndex
  }
  private addQueryParamSegment(start: number, end: number) {
    if (start !== end) {
      const queryParamNode = new QueryParamAstNode(this, this.offset + start, this.value.slice(start, end))
      this.queryParams.push(queryParamNode)
      this.items.push(queryParamNode)
    } else {
      this.errors.push({
        code: ErrorCode.QueryParamExpected,
        message: 'Query parameter expected',
        start: this.offset + start,
        end: this.offset + start + 1,
      })
    }
  }
}
