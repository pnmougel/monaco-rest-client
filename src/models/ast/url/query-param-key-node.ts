import { ASTNode } from '../ast-node'

export class QueryParamKeyAstNode extends ASTNode {
  public type: 'queryKey' = 'queryKey'
  constructor(parent: ASTNode | undefined, offset: number, public value: string) {
    super(parent, offset)
    this.length = value.length
  }
}
