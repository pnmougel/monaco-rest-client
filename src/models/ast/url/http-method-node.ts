import { ASTNode } from '../ast-node'

export class HttpMethodAstNode extends ASTNode {
  public type: 'method' = 'method'
  constructor(parent: ASTNode | undefined, offset: number, public value: string) {
    super(parent, offset)
  }
}
