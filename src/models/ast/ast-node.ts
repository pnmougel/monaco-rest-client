type NodeType = 'root' | 'object' | 'property' | 'array' | 'number' | 'boolean' | 'null' | 'string' |
  'template' | 'method' | 'url' | 'request' | 'pathSegment' | 'queryKey' | 'queryValue' | 'queryParam';

export abstract class ASTNode {

  public readonly abstract type: NodeType
  readonly value?: any

  public offset: number
  public length: number
  public readonly parent: ASTNode | undefined

  protected constructor(parent: ASTNode | undefined, offset: number, length: number = 0) {
    this.offset = offset
    this.length = length
    this.parent = parent
  }

  public get children(): ASTNode[] {
    return []
  }

  isContained(offset: number, includeRightBound = false): boolean {
    return (offset >= this.offset && offset < (this.offset + this.length)) || includeRightBound && (offset === (this.offset + this.length))
  }

  public toString(): string {
    return 'type: ' + this.type + ' (' + this.offset + '/' + this.length + ')' + (this.parent ? ' parent: {' + this.parent.toString() + '}' : '')
  }
}
