import { ASTNode } from '../ast-node'
import { StringASTNode } from './string-node'
import { ObjectASTNode } from './object-node'
import { TemplateASTNode } from './template-node'

export class PropertyASTNode extends ASTNode {
  public type: 'property' = 'property'
  public keyNode: StringASTNode | TemplateASTNode
  public valueNode?: ASTNode
  public colonOffset: number

  constructor(parent: ObjectASTNode | undefined, offset: number, keyNode: StringASTNode) {
    super(parent, offset)
    this.colonOffset = -1
    this.keyNode = keyNode
  }

  public get children(): ASTNode[] {
    return this.valueNode ? [this.keyNode, this.valueNode] : [this.keyNode]
  }
}
