import { ASTNode } from '../ast-node'
import { JsonAstNode } from './json-ast-node'

export class ArrayASTNode extends JsonAstNode {

  public type: 'array' = 'array'
  public items: ASTNode[]

  constructor(parent: ASTNode | undefined, offset: number) {
    super(parent, offset)
    this.items = []
  }

  public get children(): ASTNode[] {
    return this.items
  }

  public toJson() {
    const res = []
    this.items.forEach(item => {
      if (item instanceof JsonAstNode) {
        res.push(item.toJson())
      }
    })
    return res
  }
}
