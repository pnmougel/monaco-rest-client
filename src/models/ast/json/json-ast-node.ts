import { ASTNode } from '../ast-node'

export abstract class JsonAstNode extends ASTNode {
  abstract toJson(): any
}
