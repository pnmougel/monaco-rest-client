import { ASTNode } from '../ast-node'
import { PropertyASTNode } from './property-node'
import { JsonAstNode } from './json-ast-node'

export class ObjectASTNode extends JsonAstNode {
  public type: 'object' = 'object'
  public properties: PropertyASTNode[]

  constructor(parent: ASTNode | undefined, offset: number) {
    super(parent, offset)

    this.properties = []
  }

  public get children(): ASTNode[] {
    return this.properties
  }

  public toJson() {
    const res = {}
    this.properties.forEach(prop => {
      if (!!prop.keyNode?.value && !!prop.valueNode) {
        if (prop.valueNode instanceof JsonAstNode) {
          res[prop.keyNode?.value] = prop.valueNode.toJson()
        }
      }
    })
    return res
  }
}
