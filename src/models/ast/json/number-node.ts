import { ASTNode } from '../ast-node'
import { JsonAstNode } from './json-ast-node'

export class NumberASTNode extends JsonAstNode {

  public type: 'number' = 'number'
  public isInteger: boolean
  public value: number

  constructor(parent: ASTNode | undefined, offset: number) {
    super(parent, offset)
    this.isInteger = true
    this.value = Number.NaN
  }

  toJson(): any {
    return this.value
  }
}
