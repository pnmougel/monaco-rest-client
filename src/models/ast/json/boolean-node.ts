import { ASTNode } from '../ast-node'
import { JsonAstNode } from './json-ast-node'

export class BooleanASTNode extends JsonAstNode {

  public readonly type: 'boolean' = 'boolean'
  public value: boolean

  constructor(parent: ASTNode | undefined, boolValue: boolean, offset: number) {
    super(parent, offset)
    this.value = boolValue
  }

  toJson(): any {
    return this.value
  }
}
