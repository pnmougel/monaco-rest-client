import { ASTNode } from '../ast-node'
import { JsonAstNode } from './json-ast-node'

export class StringASTNode extends JsonAstNode {
  public type: 'string' = 'string'
  public value: string

  constructor(parent: ASTNode | undefined, offset: number, length?: number) {
    super(parent, offset, length)
    this.value = ''
  }

  toJson(): any {
    return this.value
  }
}
