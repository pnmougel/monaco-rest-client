import { ASTNode } from '../ast-node'

export class TemplateASTNode extends ASTNode {
  public type: 'template' = 'template'
  public value: string
  constructor(parent: ASTNode | undefined, offset: number, length?: number) {
    super(parent, offset, length)
    this.value = ''
  }
}
