import { ASTNode } from '../ast-node'
import { JsonAstNode } from './json-ast-node'

export class NullASTNode extends JsonAstNode {

  public type: 'null' = 'null'
  public value: null = null
  constructor(parent: ASTNode | undefined, offset: number) {
    super(parent, offset)
  }

  toJson(): any {
    return this.value
  }
}
