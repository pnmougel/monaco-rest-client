import { ASTNode } from './ast-node'
import { UrlAstNode } from './url/url-node'
import { HttpMethodAstNode } from './url/http-method-node'
import { JSONDocument } from '../json-document'
import { findNodeAtOffset } from '../../utils/node-utils'

export class RequestAstNode extends ASTNode {
  public type: 'request' = 'request'
  public method?: HttpMethodAstNode
  public url?: UrlAstNode
  public jsonDoc?: JSONDocument
  constructor(parent: ASTNode | undefined, offset: number) {
    super(parent, offset)
  }
  private _json?: ASTNode
  get json() {
    return this._json
  }
  set json(node: ASTNode | undefined) {
    this._json = node
    this.jsonDoc = new JSONDocument(node)
  }
  public get children(): ASTNode[] {
    let items = []
    if (this.method) {
      items.push(this.method)
    }
    if (this.url) {
      items.push(this.url)
    }
    if (this._json) {
      items.push(this._json)
    }
    return items
  }
  get urlValue() {
    return this.url?.value
  }
  get methodValue() {
    return this.method?.value
  }
  public getNodeFromOffset(offset: number, includeRightBound = false): ASTNode | undefined {
    return <ASTNode>findNodeAtOffset(this, offset, includeRightBound)
  }
}
