import { ASTNode } from './ast/ast-node'
import { findNodeAtOffset } from '../utils/node-utils'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { DiagnosticSeverity, Range } from 'vscode-languageserver-types'
import { Diagnostic } from 'vscode-languageserver-protocol'
import { ValidationResult } from '../features/validation/validation-result'
import { validate } from '../features/validation/validate'
import { JSONSchema } from '../features/schema/json-schema'
import { NoOpSchemaCollector, SchemaCollector } from '../features/schema/schema-collector'
import { IApplicableSchema } from '../features/schema/applicable-schema'
import { OpenApiService } from '../services/open-api-service'

export class JSONDocument {
  root?: ASTNode
  constructor(public readonly request: ASTNode | undefined) {
    this.root = request
  }

  public getNodeFromOffset(offset: number, includeRightBound = false): ASTNode | undefined {
    if (this.root) {
      return <ASTNode>findNodeAtOffset(this.root, offset, includeRightBound)
    }
    return undefined
  }

  public visit(visitor: (node: ASTNode) => boolean): void {
    if (this.root) {
      const doVisit = (node: ASTNode): boolean => {
        let ctn = visitor(node)
        const children = node.children
        if (Array.isArray(children)) {
          for (let i = 0; i < children.length && ctn; i++) {
            ctn = doVisit(children[i])
          }
        }
        return ctn
      }
      doVisit(this.root)
    }
  }

  public validate(openApiService: OpenApiService, textDocument: TextDocument, schema: JSONSchema | undefined, severity: DiagnosticSeverity = DiagnosticSeverity.Warning): Diagnostic[] | undefined {
    if (this.root && schema) {
      const validationResult = new ValidationResult()
      validate(openApiService, this.root, schema, validationResult, NoOpSchemaCollector.instance)
      return validationResult.problems.map(p => {
        const range = Range.create(textDocument.positionAt(p.location.offset), textDocument.positionAt(p.location.offset + p.location.length))
        return Diagnostic.create(range, p.message, p.severity ?? severity, p.code)
      })
    }
    return undefined
  }

  public getMatchingSchemas(openApiService: OpenApiService, schema: JSONSchema, focusOffset: number = -1, exclude?: ASTNode): IApplicableSchema[] {
    const matchingSchemas = new SchemaCollector(focusOffset, exclude)
    if (this.root && schema) {
      validate(openApiService, this.root, schema, new ValidationResult(), matchingSchemas)
    }
    return matchingSchemas.schemas
  }
}

