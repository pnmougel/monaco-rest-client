export interface HttpQuery {
  method: string
  url: string
  rawBody?: string
  jsonBody?: any
}
