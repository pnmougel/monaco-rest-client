import { ErrorCode } from '../models/enums/error-codes'

export interface ParseError {
  code: ErrorCode,
  message: string,
  start: number,
  end: number
}
