import { TextDocument } from 'vscode-languageserver-textdocument'
import { RequestsDocument } from '../models/requests-document'
import { Diagnostic } from 'vscode-languageserver-protocol'
import { JSONScanner } from './scanner'
import { SyntaxKind } from '../models/enums/syntax-kind'
import { ASTNode } from '../models/ast/ast-node'
import { DiagnosticSeverity, Range } from 'vscode-languageserver-types'
import { localize } from '../utils/localize'
import { ScanError } from '../models/enums/scan-error'
import { ArrayASTNode } from '../models/ast/json/array-node'
import { StringASTNode } from '../models/ast/json/string-node'
import { ObjectASTNode } from '../models/ast/json/object-node'
import { PropertyASTNode } from '../models/ast/json/property-node'
import { NumberASTNode } from '../models/ast/json/number-node'
import { NullASTNode } from '../models/ast/json/null-node'
import { BooleanASTNode } from '../models/ast/json/boolean-node'
import { RequestAstNode } from '../models/ast/request-node'
import { TemplateASTNode } from '../models/ast/json/template-node'
import { HttpMethodAstNode } from '../models/ast/url/http-method-node'
import { UrlAstNode } from '../models/ast/url/url-node'
import { ErrorCode } from '../models/enums/error-codes'
import { isNumber } from '../utils/objects'
import { RestClientOptions } from '../configuration/rest-client-options'

const httpMethods = new Set(['get', 'post', 'put', 'delete', 'head', 'patch', 'options', 'trace'])

export function parse(textDocument: TextDocument, languageOptions: RestClientOptions): RequestsDocument {
  const problems: Diagnostic[] = []
  let lastProblemOffset = -1
  const text = textDocument.getText()
  const scanner = new JSONScanner(text, false)

  function _scanNext(ignoreLineComment: boolean = false): SyntaxKind {
    while (true) {
      const token = scanner.scan(ignoreLineComment)
      _checkScanError()
      switch (token) {
        case SyntaxKind.LineCommentTrivia:
        case SyntaxKind.BlockCommentTrivia:
        case SyntaxKind.Trivia:
        case SyntaxKind.LineBreakTrivia:
        case SyntaxKind.MustacheTemplateStartConditionTrivia:
        case SyntaxKind.MustacheTemplateEndConditionTrivia:
        case SyntaxKind.MustacheTemplateSetDelimiter:
        case SyntaxKind.MustacheTemplateComment:
          break
        default:
          return token
      }
    }
  }

  function _errorAtRange<T extends ASTNode>(message: string, code: ErrorCode, startOffset: number, endOffset: number, severity: DiagnosticSeverity = DiagnosticSeverity.Error): void {
    if (problems.length === 0 || startOffset !== lastProblemOffset) {
      const range = Range.create(textDocument.positionAt(startOffset), textDocument.positionAt(endOffset))
      problems.push(Diagnostic.create(range, message, severity, code, textDocument.languageId))
      lastProblemOffset = startOffset
    }
  }

  function _error<T extends ASTNode>(message: string, code: ErrorCode, node: T | undefined = undefined, skipUntilAfter: SyntaxKind[] = [], skipUntil: SyntaxKind[] = []): T | undefined {
    let start = scanner.getTokenOffset()
    let end = scanner.getTokenOffset() + scanner.getTokenLength()
    if (start === end && start > 0) {
      start--
      while (start > 0 && /\s/.test(text.charAt(start))) {
        start--
      }
      end = start + 1
    }
    _errorAtRange(message, code, start, end)

    if (node) {
      _finalize(node, false)
    }
    if (skipUntilAfter.length + skipUntil.length > 0) {
      let token = scanner.getToken()
      while (token !== SyntaxKind.EOF) {
        if (skipUntilAfter.indexOf(token) !== -1) {
          _scanNext()
          break
        } else if (skipUntil.indexOf(token) !== -1) {
          break
        }
        token = _scanNext()
      }
    }
    return node
  }

  function _checkScanError(): boolean {
    switch (scanner.getTokenError()) {
      case ScanError.InvalidUnicode:
        _error(localize('InvalidUnicode', 'Invalid unicode sequence in string.'), ErrorCode.InvalidUnicode)
        return true
      case ScanError.InvalidEscapeCharacter:
        _error(localize('InvalidEscapeCharacter', 'Invalid escape character in string.'), ErrorCode.InvalidEscapeCharacter)
        return true
      case ScanError.UnexpectedEndOfNumber:
        _error(localize('UnexpectedEndOfNumber', 'Unexpected end of number.'), ErrorCode.UnexpectedEndOfNumber)
        return true
      case ScanError.UnexpectedEndOfMustacheTemplate:
        _error(localize('UnexpectedEndOfMustacheTemplate', 'Unexpected end of mustache template.'), ErrorCode.InvalidMustacheTemplate)
        return true
      case ScanError.UnexpectedEndOfComment:
        _error(localize('UnexpectedEndOfComment', 'Unexpected end of comment.'), ErrorCode.UnexpectedEndOfComment)
        return true
      case ScanError.UnexpectedEndOfString:
        _error(localize('UnexpectedEndOfString', 'Unexpected end of string.'), ErrorCode.UnexpectedEndOfString)
        return true
      case ScanError.InvalidCharacter:
        _error(localize('InvalidCharacter', 'Invalid characters in string. Control characters must be escaped.'), ErrorCode.InvalidCharacter)
        return true
    }
    return false
  }

  function _finalize<T extends ASTNode>(node: T, scanNext: boolean, ignoreLineComment: boolean = false): T {
    node.length = scanner.getTokenOffset() + scanner.getTokenLength() - node.offset
    if (scanNext) {
      _scanNext(ignoreLineComment)
    }

    return node
  }

  function _parseArray(parent: ASTNode | undefined): ArrayASTNode | undefined {
    if (scanner.getToken() !== SyntaxKind.OpenBracketToken) {
      return undefined
    }
    const node = new ArrayASTNode(parent, scanner.getTokenOffset())
    _scanNext() // consume OpenBracketToken

    let needsComma = false
    while (scanner.getToken() !== SyntaxKind.CloseBracketToken && scanner.getToken() !== SyntaxKind.EOF) {
      if (scanner.getToken() === SyntaxKind.CommaToken) {
        if (!needsComma) {
          _error(localize('ValueExpected', 'Value expected'), ErrorCode.ValueExpected)
        }
        const commaOffset = scanner.getTokenOffset()
        _scanNext() // consume comma
        if (scanner.getToken() === SyntaxKind.CloseBracketToken) {
          if (needsComma) {
            _errorAtRange(localize('TrailingComma', 'Trailing comma'), ErrorCode.TrailingComma, commaOffset, commaOffset + 1)
          }
          continue
        }
      } else if (needsComma) {
        _error(localize('ExpectedComma', 'Expected comma'), ErrorCode.CommaExpected)
      }
      const item = _parseValue(node)
      if (!item) {
        _error(localize('PropertyExpected', 'Value expected'), ErrorCode.ValueExpected, undefined, [], [SyntaxKind.CloseBracketToken, SyntaxKind.CommaToken])
      } else {
        node.items.push(item)
      }
      needsComma = true
    }

    if (scanner.getToken() !== SyntaxKind.CloseBracketToken) {
      return _error(localize('ExpectedCloseBracket', 'Expected comma or closing bracket'), ErrorCode.CommaOrCloseBacketExpected, node)
    }

    return _finalize(node, true)
  }

  const keyPlaceholder = new StringASTNode(undefined, 0, 0)

  function _parseProperty(parent: ObjectASTNode | undefined, keysSeen: { [key: string]: (PropertyASTNode | boolean) }): PropertyASTNode | undefined {
    const node = new PropertyASTNode(parent, scanner.getTokenOffset(), keyPlaceholder)
    let key = _parseString(node) || _parseTemplate(node)
    if (!key) {
      if (scanner.getToken() === SyntaxKind.Unknown) {
        // give a more helpful error message
        _error(localize('DoubleQuotesExpected', 'Property keys must be doublequoted'), ErrorCode.Undefined)
        const keyNode = new StringASTNode(node, scanner.getTokenOffset(), scanner.getTokenLength())
        keyNode.value = scanner.getTokenValue()
        key = keyNode
        _scanNext() // consume Unknown
      } else {
        return undefined
      }
    }
    node.keyNode = key

    const seen = keysSeen[key.value]
    if (seen) {
      _errorAtRange(localize('DuplicateKeyWarning', 'Duplicate object key'), ErrorCode.DuplicateKey, node.keyNode.offset, node.keyNode.offset + node.keyNode.length, DiagnosticSeverity.Warning)
      if (typeof seen === 'object') {
        _errorAtRange(localize('DuplicateKeyWarning', 'Duplicate object key'), ErrorCode.DuplicateKey, seen.keyNode.offset, seen.keyNode.offset + seen.keyNode.length, DiagnosticSeverity.Warning)
      }
      keysSeen[key.value] = true // if the same key is duplicate again, avoid duplicate error reporting
    } else {
      keysSeen[key.value] = node
    }

    if (scanner.getToken() === SyntaxKind.ColonToken) {
      node.colonOffset = scanner.getTokenOffset()
      _scanNext() // consume ColonToken
    } else {
      _error(localize('ColonExpected', 'Colon expected'), ErrorCode.ColonExpected)
      const token = scanner.getToken()
      if ((token === SyntaxKind.StringLiteral || token === SyntaxKind.MultilineStringLiteral) && textDocument.positionAt(key.offset + key.length).line < textDocument.positionAt(scanner.getTokenOffset()).line) {
        node.length = key.length
        return node
      }
    }
    const value = _parseValue(node)
    if (!value) {
      return _error(localize('ValueExpected', 'Value expected'), ErrorCode.ValueExpected, node, [], [SyntaxKind.CloseBraceToken, SyntaxKind.CommaToken])
    }
    node.valueNode = value
    node.length = value.offset + value.length - node.offset
    return node
  }

  function _parseObject(parent: ASTNode | undefined): ObjectASTNode | undefined {
    if (scanner.getToken() !== SyntaxKind.OpenBraceToken) {
      return undefined
    }
    const node = new ObjectASTNode(parent, scanner.getTokenOffset())
    const keysSeen: any = Object.create(null)
    _scanNext() // consume OpenBraceToken
    let needsComma = false

    while (scanner.getToken() !== SyntaxKind.CloseBraceToken && scanner.getToken() !== SyntaxKind.EOF) {
      if (scanner.getToken() === SyntaxKind.CommaToken) {
        if (!needsComma) {
          _error(localize('PropertyExpected', 'Property expected'), ErrorCode.PropertyExpected)
        }
        const commaOffset = scanner.getTokenOffset()
        _scanNext() // consume comma
        if (scanner.getToken() === SyntaxKind.CloseBraceToken) {
          if (needsComma) {
            _errorAtRange(localize('TrailingComma', 'Trailing comma'), ErrorCode.TrailingComma, commaOffset, commaOffset + 1)
          }
          continue
        }
      } else if (needsComma) {
        _error(localize('ExpectedComma', 'Expected comma'), ErrorCode.CommaExpected)
      }
      const property = _parseProperty(node, keysSeen)
      if (!property) {
        _error(localize('PropertyExpected', 'Property expected'), ErrorCode.PropertyExpected, undefined, [], [SyntaxKind.CloseBraceToken, SyntaxKind.CommaToken])
      } else {
        node.properties.push(property)
      }
      needsComma = true
    }

    if (scanner.getToken() !== SyntaxKind.CloseBraceToken) {
      return _error(localize('ExpectedCloseBrace', 'Expected comma or closing brace'), ErrorCode.CommaOrCloseBraceExpected, node)
    }
    return _finalize(node, true)
  }

  function _parseString(parent: ASTNode | undefined): StringASTNode | undefined {
    if (scanner.getToken() !== SyntaxKind.StringLiteral && scanner.getToken() !== SyntaxKind.MultilineStringLiteral) {
      return undefined
    }

    const node = new StringASTNode(parent, scanner.getTokenOffset())
    node.value = scanner.getTokenValue()

    return _finalize(node, true)
  }

  function _parseNumber(parent: ASTNode | undefined): NumberASTNode | undefined {
    if (scanner.getToken() !== SyntaxKind.NumericLiteral) {
      return undefined
    }

    const node = new NumberASTNode(parent, scanner.getTokenOffset())
    if (scanner.getTokenError() === ScanError.None) {
      const tokenValue = scanner.getTokenValue()
      try {
        const numberValue = JSON.parse(tokenValue)
        if (!isNumber(numberValue)) {
          return _error(localize('InvalidNumberFormat', 'Invalid number format.'), ErrorCode.Undefined, node)
        }
        node.value = numberValue
      } catch (e) {
        return _error(localize('InvalidNumberFormat', 'Invalid number format.'), ErrorCode.Undefined, node)
      }
      node.isInteger = tokenValue.indexOf('.') === -1
    }
    return _finalize(node, true)
  }

  function _parseLiteral(parent: ASTNode | undefined): ASTNode | undefined {
    // @ts-ignore
    let node: ASTNodeImpl
    switch (scanner.getToken()) {
      case SyntaxKind.NullKeyword:
        return _finalize(new NullASTNode(parent, scanner.getTokenOffset()), true)
      case SyntaxKind.TrueKeyword:
        return _finalize(new BooleanASTNode(parent, true, scanner.getTokenOffset()), true)
      case SyntaxKind.FalseKeyword:
        return _finalize(new BooleanASTNode(parent, false, scanner.getTokenOffset()), true)
      default:
        return undefined
    }
  }

  function _parseTemplate(parent: ASTNode | undefined): TemplateASTNode | undefined {
    if (scanner.getToken() !== SyntaxKind.MustacheTemplateParam) {
      return undefined
    }
    const node = new TemplateASTNode(parent, scanner.getTokenOffset())
    node.value = scanner.getTokenValue()
    node.length = node.value.length

    _scanNext()
    return node
  }

  function _parseValue(parent: ASTNode | undefined): ASTNode | undefined {
    return _parseArray(parent) || _parseObject(parent) || _parseString(parent) || _parseNumber(parent) || _parseTemplate(parent) || _parseLiteral(parent)
  }

  function _parseRequest(parent: ASTNode | undefined, languageOptions: RestClientOptions): RequestAstNode | undefined {
    const _requestNode = new RequestAstNode(parent, scanner.getTokenOffset())
    if (languageOptions.schemaDef === undefined) {
      if (scanner.getToken() === SyntaxKind.MethodToken) {
        if (!httpMethods.has(scanner.value.toLowerCase())) {
          _error(localize('InvalidHttpMethod', 'Invalid Http Method'), ErrorCode.InvalidHttpMethod)
        }
        _requestNode.method = _finalize(new HttpMethodAstNode(_requestNode, scanner.getTokenOffset(), scanner.getTokenValue()), true, true)
      } else {
        _error(localize('MethodExpected', 'Method expected'), ErrorCode.MethodExpected)
      }
      if (scanner.getToken() === SyntaxKind.UrlToken) {
        const urlNode = new UrlAstNode(_requestNode, scanner.getTokenOffset(), scanner.getTokenValue())
        urlNode.errors.forEach(error => {
          _errorAtRange(error.message, error.code, error.start, error.end)
        })
        _requestNode.url = _finalize(urlNode, true)
      } else {
        _error(localize('PathExpected', 'Path expected'), ErrorCode.PathExpected)
      }

      while (scanner.getToken() !== SyntaxKind.EOF &&
      scanner.getToken() !== SyntaxKind.MethodToken &&
      scanner.getToken() !== SyntaxKind.UrlToken &&
      scanner.getToken() !== SyntaxKind.OpenBracketToken &&
      scanner.getToken() !== SyntaxKind.OpenBraceToken) {
        _scanNext()
      }
    } else if (scanner.getToken() !== SyntaxKind.OpenBracketToken && scanner.getToken() !== SyntaxKind.OpenBraceToken) {
      _error(localize('BodyExpected', 'Body expected'), ErrorCode.BodyExpected)
    }

    if (scanner.getToken() !== SyntaxKind.OpenBracketToken || scanner.getToken() !== SyntaxKind.OpenBraceToken) {
      let jsonBody = _parseValue(_requestNode)
      if (!!jsonBody) {
        _requestNode.json = jsonBody
      }
    }
    if (_requestNode.children.length > 0) {
      const firstChild = _requestNode.children[0]
      const lastChild = _requestNode.children[_requestNode.children.length - 1]

      _requestNode.offset = firstChild.offset
      if (!!_requestNode.methodValue && !!_requestNode.urlValue) {
        _requestNode.length = (lastChild.offset + lastChild.length) - firstChild.offset
      } else {
        _requestNode.length = scanner.pos - _requestNode.offset
      }
    } else {
      _requestNode.length = 0
    }
    return _requestNode
  }

  const doc = new RequestsDocument(problems)

  _scanNext()
  let i = 0
  do {
    if (languageOptions.singleRequest && i > 0) {
      _error(localize('Unexpected Content', 'A single request is expected'), ErrorCode.Undefined)
    } else {
      const _requestNode = _parseRequest(undefined, languageOptions)
      if (!_requestNode) {
        _error(localize('Invalid symbol', 'Expected a JSON object, array or literal.'), ErrorCode.Undefined)
      } else {
        doc.requests.push(_requestNode)
      }
    }

    i++
  } while (scanner.getToken() !== SyntaxKind.EOF && i < 100)

  return doc
}
