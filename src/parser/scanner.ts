import { SyntaxKind } from '../models/enums/syntax-kind'
import { ScanError } from '../models/enums/scan-error'
import { CharacterCodes } from '../models/enums/character-codes'
import { isDigit, isLineBreak, isWhiteSpace } from '../utils/char-utils'

export class JSONScanner {
  len: number
  pos = 0
  value: string = ''
  tokenOffset = 0
  token: SyntaxKind = SyntaxKind.Unknown
  lineNumber = 0
  lineStartOffset = 0
  tokenLineStartOffset = 0
  prevTokenLineStartOffset = 0
  scanError: ScanError = ScanError.None
  // isCloseToken = true
  isNextLiteralAMethodToken = true

  mustacheStart0 = CharacterCodes.openBrace
  mustacheStart1 = CharacterCodes.openBrace
  mustacheEnd0 = CharacterCodes.closeBrace
  mustacheEnd1 = CharacterCodes.closeBrace


  prevToken: SyntaxKind = SyntaxKind.Unknown

  constructor(public readonly text: string, private readonly ignoreTrivia: boolean = false,
              mustacheTemplateDelimiters = [CharacterCodes.openBrace, CharacterCodes.openBrace, CharacterCodes.closeBrace, CharacterCodes.closeBrace]) {
    this.len = this.text.length
    this.mustacheStart0 = mustacheTemplateDelimiters[0]
    this.mustacheStart1 = mustacheTemplateDelimiters[1]
    this.mustacheEnd0 = mustacheTemplateDelimiters[2]
    this.mustacheEnd1 = mustacheTemplateDelimiters[3]
  }

  scanHexDigits(count: number, exact?: boolean): number {
    let digits = 0
    let value = 0
    while (digits < count || !exact) {
      let ch = this.text.charCodeAt(this.pos)
      if (ch >= CharacterCodes._0 && ch <= CharacterCodes._9) {
        value = value * 16 + ch - CharacterCodes._0
      } else if (ch >= CharacterCodes.A && ch <= CharacterCodes.F) {
        value = value * 16 + ch - CharacterCodes.A + 10
      } else if (ch >= CharacterCodes.a && ch <= CharacterCodes.f) {
        value = value * 16 + ch - CharacterCodes.a + 10
      } else {
        break
      }
      this.pos++
      digits++
    }
    if (digits < count) {
      value = -1
    }
    return value
  }

  setPosition(newPosition: number) {
    this.pos = newPosition
    this.value = ''
    this.tokenOffset = 0
    this.token = SyntaxKind.Unknown
    this.scanError = ScanError.None
  }

  scanNumber(): string {
    let start = this.pos
    if (this.text.charCodeAt(this.pos) === CharacterCodes._0) {
      this.pos++
    } else {
      this.pos++
      while (this.pos < this.text.length && isDigit(this.text.charCodeAt(this.pos))) {
        this.pos++
      }
    }
    if (this.pos < this.text.length && this.text.charCodeAt(this.pos) === CharacterCodes.dot) {
      this.pos++
      if (this.pos < this.text.length && isDigit(this.text.charCodeAt(this.pos))) {
        this.pos++
        while (this.pos < this.text.length && isDigit(this.text.charCodeAt(this.pos))) {
          this.pos++
        }
      } else {
        this.scanError = ScanError.UnexpectedEndOfNumber
        return this.text.substring(start, this.pos)
      }
    }
    let end = this.pos
    if (this.pos < this.text.length && (this.text.charCodeAt(this.pos) === CharacterCodes.E || this.text.charCodeAt(this.pos) === CharacterCodes.e)) {
      this.pos++
      if (this.pos < this.text.length && this.text.charCodeAt(this.pos) === CharacterCodes.plus || this.text.charCodeAt(this.pos) === CharacterCodes.minus) {
        this.pos++
      }
      if (this.pos < this.text.length && isDigit(this.text.charCodeAt(this.pos))) {
        this.pos++
        while (this.pos < this.text.length && isDigit(this.text.charCodeAt(this.pos))) {
          this.pos++
        }
        end = this.pos
      } else {
        this.scanError = ScanError.UnexpectedEndOfNumber
      }
    }
    return this.text.substring(start, end)
  }

  scanString(isMultiline: boolean): string {
    let result = '',
      start = this.pos
    while (true) {
      if (this.pos >= this.len) {
        result += this.text.substring(start, this.pos)
        this.scanError = ScanError.UnexpectedEndOfString
        break
      }
      const ch = this.text.charCodeAt(this.pos)
      if (isMultiline) {
        if (ch === CharacterCodes.doubleQuote && this.text.charCodeAt(this.pos + 1) === CharacterCodes.doubleQuote && this.text.charCodeAt(this.pos + 2) === CharacterCodes.doubleQuote) {
          result += this.text.substring(start, this.pos)
          this.pos += 3
          break
        }
      } else {
        if (ch === CharacterCodes.doubleQuote) {
          result += this.text.substring(start, this.pos)
          this.pos++
          break
        }
      }

      if (ch === CharacterCodes.backslash) {
        result += this.text.substring(start, this.pos)
        this.pos++
        if (this.pos >= this.len) {
          this.scanError = ScanError.UnexpectedEndOfString
          break
        }
        const ch2 = this.text.charCodeAt(this.pos++)
        switch (ch2) {
          case CharacterCodes.doubleQuote:
            result += '\"'
            break
          case CharacterCodes.backslash:
            result += '\\'
            break
          case CharacterCodes.slash:
            result += '/'
            break
          case CharacterCodes.b:
            result += '\b'
            break
          case CharacterCodes.f:
            result += '\f'
            break
          case CharacterCodes.n:
            result += '\n'
            break
          case CharacterCodes.r:
            result += '\r'
            break
          case CharacterCodes.t:
            result += '\t'
            break
          case CharacterCodes.u:
            const ch3 = this.scanHexDigits(4, true)
            if (ch3 >= 0) {
              result += String.fromCharCode(ch3)
            } else {
              this.scanError = ScanError.InvalidUnicode
            }
            break
          default:
            this.scanError = ScanError.InvalidEscapeCharacter
        }
        start = this.pos
        continue
      }
      if (ch >= 0 && ch <= 0x1f) {
        if (isLineBreak(ch)) {
          if (isMultiline) {
            if (ch === CharacterCodes.carriageReturn && this.text.charCodeAt(this.pos) === CharacterCodes.lineFeed) {
              this.pos++
            }
            this.lineNumber++
            this.tokenLineStartOffset = this.pos
          } else {
            // mark as error but continue with string
            result += this.text.substring(start, this.pos)
            this.scanError = ScanError.UnexpectedEndOfString
            break
          }
        } else {
          this.scanError = ScanError.InvalidCharacter
        }
      }
      this.pos++
    }
    return result
  }

  scanMustacheTemplate() {
    const startMustache = this.pos
    this.pos += (this.mustacheStart1 === undefined ? 1 : 2)

    const safeLength = this.len - 1 // For lookahead.
    let mustacheTemplateClosed = false
    while (this.pos < safeLength) {
      const ch = this.text.charCodeAt(this.pos)

      if (ch === this.mustacheEnd0 && (this.mustacheEnd1 === undefined || this.text.charCodeAt(this.pos + 1) === this.mustacheEnd1)) {
        this.pos += (this.mustacheEnd1 === undefined ? 1 : 2)
        mustacheTemplateClosed = true
        const content = this.text.substring(startMustache + (this.mustacheStart1 === undefined ? 1 : 2), this.pos - (this.mustacheEnd1 === undefined ? 1 : 2))
        const contentLength = this.pos - startMustache - 4
        const c = content.charCodeAt(0)

        // Handle mustache set delimiter (i.e., {{=<DELIM_START> <DELIM_END>=}})
        if (contentLength >= 5 && contentLength <= 7 && c === CharacterCodes.equals && content.charCodeAt(contentLength - 1) === CharacterCodes.equals) {
          this.mustacheStart0 = content.charCodeAt(1)
          let spacePosition = content.charCodeAt(2) === CharacterCodes.space ? 2 : 3
          this.mustacheStart1 = spacePosition === 2 ? undefined : content.charCodeAt(2)
          this.mustacheEnd0 = content.charCodeAt(spacePosition + 1)
          this.mustacheEnd1 = content.charCodeAt(spacePosition + 2) === CharacterCodes.equals ? undefined : content.charCodeAt(spacePosition + 2)
          this.value = this.text.substring(startMustache, this.pos)
          return SyntaxKind.MustacheTemplateSetDelimiter
        } else if (c === CharacterCodes.exclamation) {
          // Handle mustache comment
          this.value = this.text.substring(startMustache, this.pos)
          return SyntaxKind.MustacheTemplateComment
        } else if (content === '#toJson') {
          // Handle toJson mustache template
          // We look ahead for /toJson
          let i = 1
          let matchPos = 0
          const toMatch = '/toJson'

          while (this.pos + i < safeLength) {
            const c = this.text.charCodeAt(this.pos + i)
            if (c === toMatch.charCodeAt(matchPos)) {
              matchPos += 1
              if (matchPos === toMatch.length) {
                this.pos += (i + (this.mustacheEnd1 === undefined ? 2 : 3))
                this.value = this.text.substring(startMustache, this.pos)
                return SyntaxKind.MustacheTemplateParam
              }
            } else {
              matchPos = 0
              if (isLineBreak(c)) {
                if (c === CharacterCodes.carriageReturn && this.text.charCodeAt(this.pos) === CharacterCodes.lineFeed) {
                  this.pos++
                }
                this.lineNumber++
                this.tokenLineStartOffset = this.pos
                this.scanError = ScanError.UnexpectedEndOfMustacheTemplate
                this.value = this.text.substring(startMustache, this.pos)
                return SyntaxKind.MustacheTemplateParam
              }
            }
            i += 1
          }
        } else if (c === CharacterCodes.hash || c === CharacterCodes.caret || c === CharacterCodes.slash) {
          this.value = this.text.substring(startMustache, this.pos)
          // Type is start mustache condition
          return c === CharacterCodes.slash ? SyntaxKind.MustacheTemplateEndConditionTrivia : SyntaxKind.MustacheTemplateStartConditionTrivia
        } else {
          // Param template
          // Check if there is a default value just after
          const matchStart = `${String.fromCharCode(this.mustacheStart0)}${this.mustacheStart1 === undefined ? '' : String.fromCharCode(this.mustacheStart1)}^${content}${String.fromCharCode(this.mustacheEnd0)}${this.mustacheEnd1 === undefined ? '' : String.fromCharCode(this.mustacheEnd1)}`
          const posStart = this.matchInLine(matchStart)
          if (posStart > 0) {
            const matchEnd = `${String.fromCharCode(this.mustacheStart0)}${this.mustacheStart1 === undefined ? '' : String.fromCharCode(this.mustacheStart1)}/${content}${String.fromCharCode(this.mustacheEnd0)}${this.mustacheEnd1 === undefined ? '' : String.fromCharCode(this.mustacheEnd1)}`
            const posEnd = this.matchInLine(matchEnd)
            if (posEnd > 0) {
              this.pos += posEnd
              this.value = this.text.substring(startMustache, this.pos)
              return SyntaxKind.MustacheTemplateParam
            } else {
              this.scanError = ScanError.UnexpectedEndOfMustacheTemplate
            }
          }
        }
        break
      }

      this.pos++

      if (isLineBreak(ch)) {
        if (ch === CharacterCodes.carriageReturn && this.text.charCodeAt(this.pos) === CharacterCodes.lineFeed) {
          this.pos++
        }
        this.lineNumber++
        this.tokenLineStartOffset = this.pos
        this.scanError = ScanError.UnexpectedEndOfMustacheTemplate
      }
    }

    if (!mustacheTemplateClosed) {
      this.pos++
      this.scanError = ScanError.UnexpectedEndOfMustacheTemplate
    }

    this.value = this.text.substring(startMustache, this.pos)
    return SyntaxKind.MustacheTemplateParam
  }

  matchInLine(next: string) {
    let i = 0
    let posToCheck = 0
    const matchLength = next.length

    let char = this.text.charCodeAt(this.pos + i)
    while (!isLineBreak(char) && this.pos + i < this.len) {
      if (char === next.charCodeAt(posToCheck)) {
        posToCheck += 1
        if (matchLength === posToCheck) {
          return i + 1
        }
      } else {
        posToCheck = 0
      }
      i += 1
      char = this.text.charCodeAt(this.pos + i)
    }
    return -1
  }

  scanNext(ignoreLineComment: boolean): SyntaxKind {
    if (this.token !== SyntaxKind.Trivia && this.token !== SyntaxKind.Unknown) {
      this.prevToken = this.token
    }
    this.value = ''
    this.scanError = ScanError.None

    this.tokenOffset = this.pos
    this.lineStartOffset = this.lineNumber
    this.prevTokenLineStartOffset = this.tokenLineStartOffset

    if (this.pos >= this.len) {
      // at the end
      this.tokenOffset = this.len
      return this.token = SyntaxKind.EOF
    }

    let code = this.text.charCodeAt(this.pos)
    // trivia: whitespace
    if (isWhiteSpace(code)) {
      do {
        this.pos++
        this.value += String.fromCharCode(code)
        code = this.text.charCodeAt(this.pos)
      } while (isWhiteSpace(code))

      return this.token = SyntaxKind.Trivia
    }

    // trivia: newlines
    if (isLineBreak(code)) {
      this.pos++
      this.value += String.fromCharCode(code)
      if (code === CharacterCodes.carriageReturn && this.text.charCodeAt(this.pos) === CharacterCodes.lineFeed) {
        this.pos++
        this.value += '\n'
      }
      if (this.prevToken === SyntaxKind.CloseBracketToken || this.prevToken === SyntaxKind.CloseBraceToken) {
        this.isNextLiteralAMethodToken = true
      }
      this.lineNumber++
      this.tokenLineStartOffset = this.pos
      return this.token = SyntaxKind.LineBreakTrivia
    }

    if (code === this.mustacheStart0 && (this.mustacheStart1 === undefined || this.text.charCodeAt(this.pos + 1) === this.mustacheStart1)) {
      return this.token = this.scanMustacheTemplate()

    }

    // noinspection FallThroughInSwitchStatementJS
    switch (code) {
      // tokens: []{}:,
      case CharacterCodes.openBrace:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.OpenBraceToken
      case CharacterCodes.closeBrace:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.CloseBraceToken
      case CharacterCodes.openBracket:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.OpenBracketToken
      case CharacterCodes.closeBracket:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.CloseBracketToken
      case CharacterCodes.colon:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.ColonToken
      case CharacterCodes.comma:
        this.pos++
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.CommaToken

      // strings and multi line strings
      case CharacterCodes.doubleQuote:
        this.isNextLiteralAMethodToken = false
        if (this.text.charCodeAt(this.pos + 1) === CharacterCodes.doubleQuote && this.text.charCodeAt(this.pos + 2) === CharacterCodes.doubleQuote) {
          this.pos += 3
          this.value = this.scanString(true)
          return this.token = SyntaxKind.MultilineStringLiteral
        } else {
          this.pos++
          this.value = this.scanString(false)
          return this.token = SyntaxKind.StringLiteral
        }

      case CharacterCodes.slash:
        const start = this.pos - 1
        // Single-line comment
        if (this.text.charCodeAt(this.pos + 1) === CharacterCodes.slash && !ignoreLineComment) {
          this.pos += 2

          while (this.pos < this.len) {
            if (isLineBreak(this.text.charCodeAt(this.pos))) {
              break
            }
            this.pos++

          }
          this.value = this.text.substring(start, this.pos)
          return this.token = SyntaxKind.LineCommentTrivia
        } else if (this.text.charCodeAt(this.pos + 1) === CharacterCodes.asterisk) {
          // Multi-line comment
          this.pos += 2

          const safeLength = this.len - 1 // For lookahead.
          let commentClosed = false
          while (this.pos < safeLength) {
            const ch = this.text.charCodeAt(this.pos)

            if (ch === CharacterCodes.asterisk && this.text.charCodeAt(this.pos + 1) === CharacterCodes.slash) {
              this.pos += 2
              commentClosed = true
              break
            }

            this.pos++

            if (isLineBreak(ch)) {
              if (ch === CharacterCodes.carriageReturn && this.text.charCodeAt(this.pos) === CharacterCodes.lineFeed) {
                this.pos++
              }

              this.lineNumber++
              this.tokenLineStartOffset = this.pos
            }
          }

          if (!commentClosed) {
            this.pos++
            this.scanError = ScanError.UnexpectedEndOfComment
          }

          this.value = this.text.substring(start, this.pos)
          return this.token = SyntaxKind.BlockCommentTrivia
        } else {
          // Url
          while (this.pos < this.len && !isLineBreak(code) && !isWhiteSpace(code)) {
            this.pos++
            code = this.text.charCodeAt(this.pos)
          }
          if (isLineBreak(code)) {
            this.isNextLiteralAMethodToken = true
          }
          if (this.tokenOffset !== this.pos) {
            this.value = this.text.substring(this.tokenOffset, this.pos)
            return this.token = this.prevToken === SyntaxKind.MethodToken ? SyntaxKind.UrlToken : SyntaxKind.Unknown
          }
          return this.token = SyntaxKind.Unknown
        }

      // numbers
      case CharacterCodes.minus:
        this.isNextLiteralAMethodToken = false
        this.value += String.fromCharCode(code)
        this.pos++
        if (this.pos === this.len || !isDigit(this.text.charCodeAt(this.pos))) {
          return this.token = SyntaxKind.Unknown
        }
      // found a minus, followed by a number so
      // we fall through to proceed with scanning
      // numbers
      case CharacterCodes._0:
      case CharacterCodes._1:
      case CharacterCodes._2:
      case CharacterCodes._3:
      case CharacterCodes._4:
      case CharacterCodes._5:
      case CharacterCodes._6:
      case CharacterCodes._7:
      case CharacterCodes._8:
      case CharacterCodes._9:
        this.value += this.scanNumber()
        this.isNextLiteralAMethodToken = false
        return this.token = SyntaxKind.NumericLiteral
      // literals and unknown symbols
      default:
        // is a literal? Read the full word.
        while (this.pos < this.len && this.isUnknownContentCharacter(code)) {
          this.pos++
          code = this.text.charCodeAt(this.pos)
        }
        const isEol = isLineBreak(code)
        if (this.tokenOffset !== this.pos) {
          this.value = this.text.substring(this.tokenOffset, this.pos)
          // keywords: true, false, null
          switch (this.value) {
            case 'true':
              return this.token = SyntaxKind.TrueKeyword
            case 'false':
              return this.token = SyntaxKind.FalseKeyword
            case 'null':
              return this.token = SyntaxKind.NullKeyword
          }
          if (this.isNextLiteralAMethodToken) {
            this.isNextLiteralAMethodToken = false
            return this.token = SyntaxKind.MethodToken
          } else {
            if (isEol) {
              this.isNextLiteralAMethodToken = true
            }
            return this.token = SyntaxKind.Unknown
          }
        }
        // some
        this.value += String.fromCharCode(code)
        this.pos++
        return this.token = SyntaxKind.Unknown
    }
  }

  isUnknownContentCharacter(code: CharacterCodes) {
    if (isLineBreak(code) || isWhiteSpace(code)) {
      return false
    }
    switch (code) {
      case CharacterCodes.closeBrace:
      case CharacterCodes.closeBracket:
      case CharacterCodes.openBrace:
      case CharacterCodes.openBracket:
      case CharacterCodes.doubleQuote:
      case CharacterCodes.colon:
      case CharacterCodes.comma:
      case CharacterCodes.equals:
      case CharacterCodes.question:
      case CharacterCodes.ampersand:
      case CharacterCodes.slash:
        return false
    }
    return true
  }

  scanNextNonTrivia(ignoreLineComment: boolean = false): SyntaxKind {
    let result: SyntaxKind
    do {
      result = this.scanNext(ignoreLineComment)
    } while (result >= SyntaxKind.LineCommentTrivia && result <= SyntaxKind.Trivia)
    return result
  }

  getPosition() {
    return this.pos
  }

  scan(ignoreLineComment: boolean = false) {
    return this.ignoreTrivia ? this.scanNextNonTrivia(ignoreLineComment) : this.scanNext(ignoreLineComment)
  }

  getToken() {
    return this.token
  }

  getTokenValue() {
    return this.value
  }

  getTokenOffset() {
    return this.tokenOffset
  }

  getTokenLength() {
    return this.pos - this.tokenOffset
  }

  getTokenStartLine() {
    return this.lineStartOffset
  }

  getTokenStartCharacter() {
    return this.tokenOffset - this.prevTokenLineStartOffset
  }

  getTokenError() {
    return this.scanError
  }
}
