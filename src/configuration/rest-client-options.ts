import { OpenAPIObject } from 'openapi3-ts'
import { RestClientFeatures } from './rest-client-features'
import {
  CustomBodyCompletionResolver,
  CustomCompletionItem,
  CustomPathCompletionResolver,
  SchemaDef,
} from '../monaco-rest-client'

export interface RestClientOptions {
  readonly validate?: boolean

  readonly allowTrailingComma?: boolean

  /**
   * If defined:
   * - disable endpoint definition
   * - validate only against the provided schema path
   */
  readonly schemaDef?: SchemaDef;

  /**
   * If true, only a single request will be allowed
   */
  readonly singleRequest?: boolean;

  /**
   * Defines whether comments are allowed or not. If set to false, comments will be reported as errors.
   * DocumentLanguageSettings.allowComments will override this setting.
   */
  readonly allowComments?: boolean;

  readonly openApiSpec?: OpenAPIObject

  readonly features?: RestClientFeatures

  readonly customOptions?: object

  snippets?: {name: string, content: string, description?: string}[]
  customPathCompletion?: CustomPathCompletionResolver
  customBodyCompletion?: CustomBodyCompletionResolver
  customQueryCompletion?: (...param: any[]) => CustomCompletionItem[]
}

export const optionDefaults: Required<RestClientOptions> = {
  validate: true,
  allowTrailingComma: true,
  allowComments: true,
  singleRequest: false,
  schemaDef: undefined,
  openApiSpec: undefined,
  customPathCompletion: (paramName: string, paramSchema: any) => { return Promise.resolve([]) },
  customBodyCompletion: (kind: 'key' | 'value', schema: any, ctx, options: any) => { return Promise.resolve([]) },
  customQueryCompletion: () => { return [] },
  customOptions: {},
  snippets: [],

  features: {
    completionItems: true,
    hovers: true,
    documentSymbols: true,
    foldingRanges: true,
    diagnostics: true,
    rangeFormatting: true,
    codeLens: true,
    tokens: true,
  },
}
