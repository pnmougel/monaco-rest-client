import type * as monaco from 'monaco-editor-core'
import { RestClientOptions } from './rest-client-options'

export class LanguageServiceOptions {
  private readonly _languageId: string
  constructor(
    _monaco: typeof monaco,
    languageId: string,
    options: RestClientOptions,
  ) {
    this._onDidChange = new _monaco.Emitter<LanguageServiceOptions>()
    this._languageId = languageId
    this.setOptions(options)
  }
  private _onDidChange: monaco.Emitter<LanguageServiceOptions>
  get onDidChange(): monaco.IEvent<LanguageServiceOptions> {
    return this._onDidChange.event
  }
  private _options: RestClientOptions
  get options(): RestClientOptions {
    return this._options
  }
  private _modelOptions: { [modelUrl: string]: RestClientOptions } = {}
  get modelOptions() {
    return this._modelOptions ?? {}
  }
  get languageId(): string {
    return this._languageId
  }
  setModelOptions(url: string, options: RestClientOptions) {
    this._modelOptions[url] = options || Object.create(null)
    this._onDidChange.fire(this)
  }

  setOptions(options: RestClientOptions): void {
    this._options = options || Object.create(null)
    this._onDidChange.fire(this)
  }
}
