export interface RestClientFeatures {
  readonly tokens?: boolean

  /**
   * Defines whether the built-in completionItemProvider is enabled.
   */
  readonly completionItems?: boolean;

  /**
   * Defines whether the built-in hoverProvider is enabled.
   */
  readonly hovers?: boolean;

  /**
   * Defines whether the built-in documentSymbolProvider is enabled.
   */
  readonly documentSymbols?: boolean;

  /**
   * Defines whether the built-in foldingRange provider is enabled.
   */
  readonly foldingRanges?: boolean;

  /**
   * Defines whether the built-in diagnostic provider is enabled.
   */
  readonly diagnostics?: boolean;

  /**
   * Defines whether the built-in code lend provider is enabled
   */
  readonly codeLens?: boolean;

  /**
   * Defines whether the built-in range formatting provider is enabled
   */
  readonly rangeFormatting?: boolean;
}
