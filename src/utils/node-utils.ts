/**
 * Finds the node at the given path in a JSON DOM.
 */
import { ASTNode } from '../models/ast/ast-node'

/**
 * Gets the JSON path of the given JSON DOM node
 */
export function getNodePath(node: ASTNode): (string | number)[] {
  if (!node.parent || !node.parent.children) {
    return []
  }
  const path = getNodePath(node.parent)
  if (node.parent.type === 'property') {
    const key = node.parent.children[0].value
    path.push(key)
  } else if (node.parent.type === 'array') {
    const index = node.parent.children.indexOf(node)
    if (index !== -1) {
      path.push(index)
    }
  }
  return path
}

/**
 * Evaluates the JavaScript object of the given JSON DOM node
 */
export function getNodeValue(node: ASTNode): any {
  switch (node.type) {
    case 'array':
      return node.children!.map(getNodeValue)
    case 'object':
      const obj = Object.create(null)
      for (let prop of node.children!) {
        const valueNode = prop.children![1]
        if (valueNode) {
          obj[prop.children![0].value] = getNodeValue(valueNode)
        }
      }
      return obj
    case 'null':
    case 'string':
    case 'number':
    case 'boolean':
      return node.value
    default:
      return undefined
  }

}

// export function contains(node: ASTNode, offset: number, includeRightBound = false): boolean {
//   return (offset >= node.offset && offset < (node.offset + node.length)) || includeRightBound && (offset === (node.offset + node.length));
// }

/**
 * Finds the most inner node at the given offset. If includeRightBound is set, also finds nodes that end at the given offset.
 */
export function findNodeAtOffset(node: ASTNode, offset: number, includeRightBound = false): ASTNode | undefined {
  if (node.isContained(offset, includeRightBound)) {
    const children = node.children
    if (Array.isArray(children)) {
      for (let i = 0; i < children.length && children[i].offset <= offset; i++) {
        const item = findNodeAtOffset(children[i], offset, includeRightBound)
        if (item) {
          return item
        }
      }

    }
    return node
  }
  return undefined
}
