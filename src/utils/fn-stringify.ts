export function fnStringify(fn) {
  if (fn instanceof Function || typeof fn == 'function') {
    let fnBody = fn.toString()

    if (fnBody.length < 8 || fnBody.substring(0, 8) !== 'function') { //this is ES6 Arrow Function
      return '_NuFrRa_' + fnBody
    }
    return fnBody
  }
}

export function fnParse(fnAsStr: any) {
  const prefix = fnAsStr.substring(0, 8)
  if (prefix === 'function') {
    return eval('(' + fnAsStr + ')')
  }
  if (prefix === '_NuFrRa_') {
    return eval(fnAsStr.slice(8))
  }
}
