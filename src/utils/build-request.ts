import { HttpQuery } from '../models/http-query'
import { TextDocument } from 'vscode-languageserver-textdocument'
import { RequestAstNode } from '../models/ast/request-node'
import { ObjectASTNode } from '../models/ast/json/object-node'
import { ArrayASTNode } from '../models/ast/json/array-node'

export function buildRequest(request: RequestAstNode, textDoc: TextDocument): HttpQuery {
  let rawBody = undefined
  let jsonBody: any = undefined

  if (!!request.jsonDoc?.root) {
    let parsableBody = textDoc.getText()
    let offset = request.jsonDoc.root.offset
    parsableBody = parsableBody.substr(offset, request.jsonDoc.root.length)
    rawBody = parsableBody
    if (request.jsonDoc.root instanceof ObjectASTNode || request.jsonDoc.root instanceof ArrayASTNode) {
      jsonBody = request.jsonDoc.root.toJson()
    }
  }

  return {
    method: request.methodValue, url: request.urlValue, rawBody, jsonBody,
  }
}
