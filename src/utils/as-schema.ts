import { isBoolean } from './objects'
import { JSONSchema, JSONSchemaRef } from '../features/schema/json-schema'

export function asSchema(schema: JSONSchemaRef): JSONSchema;
export function asSchema(schema: JSONSchemaRef | undefined): JSONSchema | undefined;
export function asSchema(schema: JSONSchemaRef | undefined): JSONSchema | undefined {
  if (isBoolean(schema)) {
    return schema ? {} : { 'not': {} }
  }
  return schema
}
