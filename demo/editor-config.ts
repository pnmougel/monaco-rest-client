import * as monaco from 'monaco-editor-core';

export const editorConfig: monaco.editor.IStandaloneEditorConstructionOptions = {
  contextmenu: false,
  minimap: {
    enabled: false
  },
  autoClosingBrackets: 'always',
  automaticLayout: true,
  autoIndent: 'brackets',
  autoSurround: 'brackets',
  wordBasedSuggestions: false,
  scrollBeyondLastLine: false,
  definitionLinkOpensInPeek: true,
  formatOnType: true,
  inlineSuggest: {
    enabled: true
  },
  parameterHints: {
    enabled: true
  },
  showFoldingControls: 'always',
  suggest: {
    preview: true,
    showInlineDetails: true,
    showTypeParameters: true,
  },
}
