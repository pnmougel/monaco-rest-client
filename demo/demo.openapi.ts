import { OpenAPIObject } from 'openapi3-ts';

export const demoSpec: OpenAPIObject = {
  openapi: '3.0.0',
  info: {
    version: '1.0.0',
    title: 'Demo spec',
  },
  paths: {
    test: {
      '$ref': '#/components/paths/test',
    },
    'test/{id}': {
      '$ref': '#/components/paths/test',
    },
  },
  components: {
    paths: {
      test: {
        get: {
          test: 'Test endpoint',
          parameters: [
            {
              'name': 'enable',
              'in': 'query',
              'description': 'Boolean parameter',
              'schema': {
                'type': 'boolean',
                'default': false,
              },
            },
            {
              'name': 'id',
              'in': 'path',
              'description': 'Path parameter',
              'schema': {
                "type": "string",
                "enum": [
                  "42",
                  "43",
                ]
              },
            },
            {
              'name': 'type',
              'in': 'query',
              'description': 'Enum parameter with completion',
              "schema": {
                "type": "string",
                "enum": [
                  "foo",
                  "bar",
                ],
                'enumDescriptions': [
                  'param foo',
                  'param bar',
                ],
              }
            }],
          requestBody: {
            content: {
              'application/json': {
                schema: {
                  '$ref': '#/components/schemas/Test',
                },
              },
            },
            'required': false,
          },
        },
      },
    },
    schemas: {
      "Test": {
        "type": "object",
        "properties": {
          "booleanProp": {
            "description": "A boolean property !",
            "type": "boolean",
            "default": false,
          },
          "stringProp": {
            "description": "A string property",
            "type": "string",
            "default": 'foobar',
          },
          "enumProp": {
            "description": "An enum property",
            'type': 'string',
            'enum': [
              'foo',
              'bar',
            ],
            'enumDescriptions': [
              'foo description',
              'bar description',
            ],
          },
          "nested": {
            "description": "A nested object",
            '$ref': '#/components/schemas/Nested',
          }
        }
      },
      Nested: {
        type: "object",
        properties: {
          subfield: {
            type: 'string'
          },
          subfield2: {
            type: 'string'
          }
        }
      }
    }
  },

};
