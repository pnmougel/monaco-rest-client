export function customPathCompletion(paramName: string, paramSchema: any, options: any) {
  if (!('xEnv' in options)) {
    return Promise.resolve([]);
  }
  const headers = { 'x-env': `${options['xEnv']}` };

  if (paramName === 'index') {
    return fetch('http://localhost:3000/indices/all', { headers })
      .then(res => res.json())
      .then(res => res.map(indexInfo => ({
        label: indexInfo.index,
        description: indexInfo.index,
        kind: 7
      })))
      .catch(err => {})
  } else {
    return Promise.resolve([]);
  }
}
