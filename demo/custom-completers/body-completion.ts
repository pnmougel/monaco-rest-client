import { CustomBodyCompletionContext, CustomBodyCompletionResolver } from '../../src/monaco-rest-client';

export const customBodyCompletion: CustomBodyCompletionResolver = (kind: 'key' | 'value', schema: any, ctx: CustomBodyCompletionContext, options: any) => {
  if (!('xEnv' in options) || !schema || !ctx ) {
    return Promise.resolve([]);
  }
  const headers = { 'x-env': `${options['xEnv']}` };
  if (kind === 'key') {
    const propertyRef = schema.propertyNames?.$ref ?? schema.additionalPropertiesKey?.$ref;
    if (propertyRef === '#/components/_types/Field') {

      const indices = ctx?.pathParams['index'] ?? '_all';
      return fetch(`http://localhost:3000/indices/fields/${indices}`, { headers })
        .then(res => res.json())
        .then(fields => fields.map(field => {
            return {
              label: field.field,
              type: field.type,
              // TODO: Set correct type
              kind: 3
            };
          }),
        )
        .catch(err => {})
    }
  }
};
