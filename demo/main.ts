import { languageId, setupRestClient } from '../src/monaco-rest-client'
import * as monaco from 'monaco-editor-core'
import { esSpec } from './schema/schema-7.3.0'
import { editorConfig } from './editor-config'
import { editorTheme } from './editor-theme'
import { endpointOnly, exampleUpdateMustacheDelimiter } from './editor-example'
import { customPathCompletion } from './custom-completers/path-completion'
import { customBodyCompletion } from './custom-completers/body-completion'

export const MONACO_URI = monaco.Uri.parse('inmemory://model.json');
export const MONACO_URI2 = monaco.Uri.parse('inmemory://model2.json');

// Setup webworkers
(window as any).MonacoEnvironment = {
    getWorker: (moduleId: string, label: string) => {
        if (label === languageId) {
            return new Worker('./rest-client.worker.js');
        }
        return new Worker('./editor.worker.js');
    },
};

// Custom theme
monaco.editor.defineTheme('myTheme', editorTheme);
monaco.editor.setTheme('myTheme');

const restClient = setupRestClient(monaco, {
    allowTrailingComma: true,
    openApiSpec: esSpec,
    features: {
        codeLens: true,
    },
    snippets: [{
        name: 'sq',
        content: `GET /\${1:index}/_search
{
    "query": {
        "$2"
    }
}`
    }],
    // singleRequest: true,
    // schemaDef: {
    //     type: 'ref',
    //     $ref: '#/components/ccr.follow/Request',
    // },
    // schemaPath: {
    //     method: 'GET',
    //     path: '_search'
    // },
    // schemaPath: {
    //     method: 'PUT',
    //     path: '{index}/_mapping'
    // },
    customOptions: {
        xEnv: 20,
    },
    customPathCompletion,
    customBodyCompletion,
});

restClient.setModelOptions('inmemory://model2.json', {
    // schemaDef: {
    //     type: 'schema',
    //     schema: {
    //         type: 'object',
    //         properties: {
    //             test: {
    //                 type: 'string',
    //                 description: 'Bla bla'
    //             }
    //         }
    //     }
    // },
    schemaDef: {
        type: 'path',
        method: 'GET',
        path: '_search',
    },
});

const modelContainer = document.getElementById('container')!;
const model = monaco.editor.createModel(endpointOnly, languageId, MONACO_URI);
const editor = restClient.createEditor(modelContainer, { model, ...editorConfig }, {
    queryRunner: (query) => {
        console.log('From model 1')
        console.log(query)
        // extractSource(query.rawBody, query.jsonBody);
    }
})

const modelContainer2 = document.getElementById('container2')!;
const model2 = monaco.editor.createModel(exampleUpdateMustacheDelimiter, languageId, MONACO_URI2);
// const editor2 = monaco.editor.create(modelContainer2, { model: model2, ...editorConfig });
const editor2 = restClient.createEditor(modelContainer2, { model: model2, ...editorConfig }, {
    queryRunner: (query) => {
        console.log('From model 2')
        console.log(query)
        // extractSource(query.rawBody, query.jsonBody);
    }
})

//
// // Define query runner
// restClient.registerRunQueryAction(editor, (query) => {
//     console.log('From model 1')
//     console.log(query)
//     // extractSource(query.rawBody, query.jsonBody);
// });
// restClient.registerRunQueryAction(editor2, (query) => {
//     console.log('From model 2')
//     console.log(query)
//     // extractSource(query.rawBody, query.jsonBody);
// });


/**
 * This method allows to rebuild a valid json when the rawBody contains mustache template
 * Since we do not parse the json, there is some edge case, but really they should not occur.
 *
 * If there is a comment, not in the source parameter and this comment
 * contains something like `"source": { {{}} }`, then instead of the real source, this will be the content inserted.
 *
 * It will also fail if there is a param named source of type object and containing {{}}, e.g., a string
 *
 * @param str
 * @param jsonBody
 */
function extractSource(str: string, jsonBody: any) {
    if ('source' in jsonBody) {
        const regexp = /"source"[\s\n]*:[\s\n]*{/g;
        let match: RegExpExecArray;
        while ((match = regexp.exec(str)) !== null) {
            const startIdx = match.index + match[0].length;

            let extractedObj = '';
            let inString = false;
            let inMultilineString = false;
            let lvl = 1;
            let pos = 0;
            while (lvl !== 0 && pos < str.length) {
                const c = str.charAt(startIdx + pos);
                if (c === '"') {
                    if(str.charAt(startIdx + pos + 1) === '"' && str.charAt(startIdx + pos + 2) === '"') {
                        pos += 2
                        inMultilineString = !inMultilineString
                    } else if(!inMultilineString) {
                        inString = !inString;
                    }
                    extractedObj += '\"';
                } else {
                    if (c === '{' && !inString && !inMultilineString) {
                        lvl += 1;
                    } else if (c === '}' && !inString && !inMultilineString) {
                        lvl -= 1;
                    }
                    if (inString || inMultilineString || (c !== ' ')) {
                        extractedObj += c;
                    }
                }
                pos += 1;
            }
            if (extractedObj.indexOf('{{') !== -1) {
                jsonBody.source = `{${extractedObj}`;
            }
        }

    }
    console.log(jsonBody);
}

// const specContainer = document.getElementById('spec')!;
// const specModel = monaco.editor.createModel(JSON.stringify(esSpec, null, 2), 'json', MONACO_URI2);
// const openApiEditor = monaco.editor.create(specContainer, { model: specModel, ...editorConfig });
//
// openApiEditor.onDidChangeModelContent(() => {
//     const text = specModel.getValue();
//     try {
//         const spec = JSON.parse(text);
//         restClient.options = {
//             openApiSpec: spec,
//         };
//     } catch (e) {
//         console.error('Unable to parse spec');
//     }
// });

