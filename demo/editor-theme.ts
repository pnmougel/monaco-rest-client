import { editor } from 'monaco-editor-core';

export const editorTheme: editor.IStandaloneThemeData = {
  base: 'vs',
  inherit: true,
  rules: [
    { token: 'template.mustache.block.json', background: 'EDF9FA', foreground: 'FF0000' },
    { token: 'string.method.url', foreground: 'd07134' },
    { token: 'string.key.json', foreground: '277afe'},
    { token: 'string.path.url', foreground: '3a868d' },
  ],
  colors: {
    'editor.foreground': '#000000',
    'editor.background': '#EDF9FA',
    'editorCursor.foreground': '#8B0000',
    'editor.lineHighlightBackground': '#0000FF20',
    'editorLineNumber.foreground': '#008800',
    'editor.selectionBackground': '#88000030',
    'editor.inactiveSelectionBackground': '#88000015',
  },
}
