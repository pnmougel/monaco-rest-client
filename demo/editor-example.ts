export const endpointOnly = `GET /_search
`

export const example = `
GET /log100k/_count
{
  // This is a test
  "query": {
    "match": {
      "test": {
        "minimum_should_match"
      }
    },
    "terms": {
      
    },
  }
}
`

export const exampleMustache = `
GET /log100k/_search/template
{
  "source": {
    "query": {
      "match": {
        "field": {{from}}{{^from}}0{{/from}}
      }
    }
  }
}

POST /_render/template
{
  "source": {
    "query": {
      "match": {
        "field": {{from}}{{^from}}0{{/from}}
      }
    }
  }
}

GET /log100k/_search/template
{
  "source": {
    "bla": 42
  },
  "param": {
    
  }
}
`

export const exampleSearch = `
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "field": {
              "query": {{from}}{{^from}}0{{/from}}
            }
          }
        },
        {
          "match": {
            "field": {
              "query": {{#toJson}}my_query{{/toJson}}
            }
          }
        },
        {{#param}}{
          "match": {
            "field": {
              "query": {{param}},
            }
          }
        },
        {{/param}}{{^param}}{
          "match_all": {}
        },
        {{/param}}{
          "match": {
            "field": {
              "query": {{param}}
            }
          }
        }
      ]
    }
  }
}`

export const exampleUpdateMustacheDelimiter = `
{{=< >=}}
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "field": {
              "query": <from>
            }
          }
        },
        {
          "match": {
            "field": {
              "query": <#toJson>my_query</toJson>
            }
          }
        },
        <#param>
        {
          "match": {
            "field": {
              "query": <param>,
            }
          }
        },
        </param>
        <^param>
        {
          "match_all": {}
        },
        </param>{
          "match": {
              "field": {
                "query": <param>
              }
            }
          }
        ]
      }
    }
  }`
