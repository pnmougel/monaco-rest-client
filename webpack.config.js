const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    'main': './demo/main.ts',
    'monaco-rest-client': './src/monaco-rest-client.ts',
    'rest-client.worker': './src/rest-client.worker.ts',
    'editor.worker': 'monaco-editor-core/esm/vs/editor/editor.worker.js',
  },
  devtool: "source-map",
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: false,
    port: 9090,
  },
  output: {
    library: 'monaco-rest-client',
    libraryTarget: 'umd',
    globalObject: 'self',
    filename: (chunkData) => {
      switch (chunkData.chunk.name) {
        case 'editor.worker':
          return 'editor.worker.js';
        case 'rest-client.worker':
          return "rest-client.worker.js"
        case 'main':
          return "main.js"
        case 'monaco-rest-client':
          return "monaco-rest-client.js"
      }
    },
    path: path.resolve(__dirname, 'dist'),
  },
  optimization: {
    minimize: false,
    // runtimeChunk: 'single',
    // moduleIds: 'named',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          // chunks: 'all',
          reuseExistingChunk: true,
        },
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.ttf$/,
        use: ['file-loader'],
      }],
  },
  resolve: {
    extensions: ['.ts', '.js', '.ttf'],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: 'demo/index.html'
    })
  ]
};
